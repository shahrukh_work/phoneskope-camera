//
//  DataHandler.swift
//  NAASLapp
//
//  Created by IOS on 11/09/2017.
//  Copyright © 2017 softak. All rights reserved.
//
import CoreData
import Foundation

public class DataHandler
{
    let context:NSManagedObjectContext
    
    init()
    {
        context = CoreDataManager.sharedInstance.managedObjectContext
    }
    
    func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    //MARK: - save & delete video path and file
    
    func saveVideoPath (path: String, duration : String, size: String, rotationAngle: Double, thumbnail: NSData, isSlowMotion: Bool) {
        let rifleScopeVideosCount = getSaveVideoPath().count
        let rifleScopeVideos = RifleScopeVideos(context: context)
        rifleScopeVideos.path = path
        rifleScopeVideos.duration = duration
        rifleScopeVideos.size = size
        rifleScopeVideos.rotationAngle = rotationAngle
        rifleScopeVideos.thumbnail = thumbnail as Data
        rifleScopeVideos.serialno = Int16(rifleScopeVideosCount + 1)
        rifleScopeVideos.isSlowMotion = isSlowMotion
        
        CoreDataVideos.shared.rifleVideos.append(rifleScopeVideos)
        CoreDataManager.sharedInstance.saveContext()
    }
    
 
    
    func updateSaveVideoPathArray(sourceIndex : Int, destinationIndex: Int){
        
         CoreDataVideos.shared.rifleVideos[sourceIndex].serialno = Int16(destinationIndex + 1)
         CoreDataVideos.shared.rifleVideos[destinationIndex].serialno = Int16(sourceIndex + 1)
    //    var sourceValue = CoreDataVideos.shared.rifleVideos[sourceIndex]
     //   CoreDataVideos.shared.rifleVideos[sourceIndex] = CoreDataVideos.shared.rifleVideos[destinationIndex]
     //   CoreDataVideos.shared.rifleVideos[destinationIndex] = sourceValue
    }
    
    func updateSaveVideoPath(sourceIndex : Int, destinationIndex: Int){
        //sourceIndex - > The one that is moving
        //destinationIndex - > Where it is moving
        let fetchRequest:NSFetchRequest<RifleScopeVideos> = RifleScopeVideos.fetchRequest()
        let sectionSortDescriptor = NSSortDescriptor(key: "serialno", ascending: true)
        let sortDescriptors = [sectionSortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors
        
        do {
            let results = try context.fetch(fetchRequest) as! [RifleScopeVideos]
            results[sourceIndex].serialno = Int16(destinationIndex + 1)
            results[destinationIndex].serialno = Int16(sourceIndex + 1)
            CoreDataManager.sharedInstance.saveContext()
    
        } catch {
            print("Fetch Failed: \(error)")
        }
        
    }
    
    func getSaveVideoPath () -> [RifleScopeVideos]{
        var data: [RifleScopeVideos] = []
        do{
            let fetchRequest:NSFetchRequest<RifleScopeVideos> = RifleScopeVideos.fetchRequest()
            let sectionSortDescriptor = NSSortDescriptor(key: "serialno", ascending: true)
            let sortDescriptors = [sectionSortDescriptor]
            fetchRequest.sortDescriptors = sortDescriptors
            data = try context.fetch(fetchRequest)
            
        } catch {
            
        }
        return data
    }
    
    func deleteRow(fileName: String){
        //Delete from  Singleton
        
        //Delete from coredata
        let fetchRequest: NSFetchRequest<RifleScopeVideos> = RifleScopeVideos.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "path = %@", fileName)
        var fetchedItems = [RifleScopeVideos]()
        do{
            fetchedItems=try context.fetch(fetchRequest)
        }catch{
            fatalError("Could not fetch")
        }
        for item in fetchedItems{
            context.delete(item)
        }
        CoreDataManager.sharedInstance.saveContext()
    }
    
    func deleteFile(filePath:String, fileName: String){
        let filenameArray = filePath.components(separatedBy:"/" )
       
        
        let fileManager = FileManager.default
        do {
            try fileManager.removeItem(atPath: filePath)
            for (index,row) in CoreDataVideos.shared.rifleVideos.enumerated() {
                if let path = row.path as? String{
                    if path == "/"+filenameArray.last! {
                        CoreDataVideos.shared.rifleVideos.remove(at: index)
                    }
                }
            }
            
            deleteRow(fileName: "/"+filenameArray.last! )
            
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    }
    
    func deleteAllData(){
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "RifleScopeVideos")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }
    
    func resaveValues() {
        for data in CoreDataVideos.shared.rifleVideos {
            let rifleScopeVideos = RifleScopeVideos(context: context)
            rifleScopeVideos.path = data.path!
            rifleScopeVideos.duration = data.duration!
            rifleScopeVideos.size = data.size!
            rifleScopeVideos.rotationAngle = data.rotationAngle
            rifleScopeVideos.thumbnail = data.thumbnail!
            rifleScopeVideos.serialno = data.serialno
            CoreDataManager.sharedInstance.saveContext()
            
        }
    }
    
    // MARK: - Training mode
    
    func saveTrainingMode(_ value : Bool){
        UserDefaults.standard.set(value, forKey: "trainingMode")
    }
    
    func getTrainingMode() -> Bool{
        var mode = false
        if UserDefaults.standard.object(forKey: "trainingMode") != nil{
            mode = UserDefaults.standard.bool(forKey: "trainingMode")
        }
        return mode
    }
    
    func saveSettingsForRifle (name:String,iso : Float , rotate : Float , focus : Float){
    let newObj = Rifle(context:context)
        newObj.name = name
        newObj.focusValue = focus
        newObj.isoValue = iso
        newObj.rotateValue = rotate
        CoreDataManager.sharedInstance.saveContext()
    }
    
    func getSettingsForRifle () -> [Rifle]{
        var data: [Rifle] = []
        do{
            let fetchRequest:NSFetchRequest<Rifle> = Rifle.fetchRequest()
            data = try context.fetch(fetchRequest)
           
        } catch {
            
        }
        return data
        
    }
    
    func saveRotateInfo(name:String, rotate : Float){
        let newObj = RotateInfo(context:context)
        newObj.name = name
        newObj.rotate = rotate
        CoreDataManager.sharedInstance.saveContext()
    }
    func getRotateInfo (name : String) -> [RotateInfo]{
        var data: [RotateInfo] = []
        do{
            let fetchRequest:NSFetchRequest<RotateInfo> = RotateInfo.fetchRequest()
           // fetchRequest.predicate = NSPredicate(block: "")
            data = try context.fetch(fetchRequest)
            
        } catch {
            
        }
        return data
        
    }
    func saveIntroductionSettings(key: Int,hasShown : Bool){
        let newObj = IntroductionValues(context : context)
        newObj.key_value = Int64(key)
        newObj.hasShown = hasShown
        CoreDataManager.sharedInstance.saveContext()
    }
    func getIntroductionSettings (key : Int64) -> IntroductionValues{
        var data: IntroductionValues!
        let fetchRequest : NSFetchRequest<IntroductionValues> = IntroductionValues.fetchRequest()

        fetchRequest.predicate = NSPredicate(format: "key_value == \(key)")

        do{
            let fetchedResults = try context.fetch(fetchRequest) as! [IntroductionValues]
            if let aContact = fetchedResults.first {
                print(aContact.key_value)
                data = aContact
            }
          
        } catch {
            print(error)
        }
        return data
    }
    
    func deleteAllRecords() {
       
        
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "IntroductionValues")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print ("There was an error")
        }
    }

    
    func getIntroductionSettingsAll () -> [IntroductionValues]{
        var data: [IntroductionValues] = []
        let fetchRequest:NSFetchRequest<IntroductionValues> = IntroductionValues.fetchRequest()
        do{
            data = try context.fetch(fetchRequest)
        } catch {
            
        }
        return data
    }
    func updateIntroductionSettings(key : Int16){
       
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "IntroductionValues")
        fetchRequest.predicate = NSPredicate(format: "key_value == \(key)")
        
        do {
            let results = try context.fetch(fetchRequest) as! [IntroductionValues]
            if results.count != 0 { // Atleast one was returned
                
                for intro in results {
                    intro.hasShown = true
                }
                CoreDataManager.sharedInstance.saveContext()
            }
        } catch {
            print("Fetch Failed: \(error)")
        }
        
    }
    
}
