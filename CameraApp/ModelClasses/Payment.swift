//
//  Payment.swift
//  CameraApp
//
//  Created by iMac on 20/02/2019.
//  Copyright © 2019 softech. All rights reserved.
//

import Foundation
import EVReflection
class Payment: EVObject {
    var expiry_date: Int = 0
    var paid: Bool = false
    var type: String = ""
    var data: String = ""
}
