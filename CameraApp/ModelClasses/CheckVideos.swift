//
//  CheckVideos.swift
//  CameraApp
//
//  Created by iMac on 03/04/2019.
//  Copyright © 2019 softech. All rights reserved.
//

import Foundation
import UIKit

protocol CheckVideosDelegate {
    func didFinishProcessingVideo()
    func sendPerentageToViewController(_ percent: String)
}


class CheckVideos: PercentageCheckVideoDelegate {
    
    static let shared = CheckVideos()
    let dataHandler = DataHandler()
    var rifleVideos: [RifleScopeVideos] = []
    var imagePath = ""
    var delegate: CheckVideosDelegate?
    var isProcessing = false
    
    func getVideo(){
        isProcessing = true
        let fileManager = FileManager.default
        rifleVideos = CoreDataVideos.shared.rifleVideos//dataHandler.getSaveVideoPath()
        //  CoreDataVideos.shared.rifleVideos = rifleVideos
        if rifleVideos.count  != 0 {
            imagePath = (dataHandler.getDirectoryPath() as NSString).appending("\(rifleVideos[0].path!)")
            if fileManager.fileExists(atPath: imagePath){
                ChangeVideoCompostion.percentDelegate = self
                ChangeVideoCompostion.changeComposition( URL(fileURLWithPath: imagePath), angle: CGFloat(rifleVideos[0].rotationAngle), fileName: rifleVideos[0].path!, isSlowMotion: rifleVideos[0].isSlowMotion, success: { (success, percentage) in
                    if success {
                        self.rifleVideos = CoreDataVideos.shared.rifleVideos
                        if self.rifleVideos.count != 0 {
                            let imagePAth = (DataHandler().getDirectoryPath() as NSString).appending("\(self.rifleVideos[0].path!)")
                            DataHandler().deleteFile(filePath: imagePAth,fileName: self.rifleVideos[0].path!)
                        }
                        self.delegate?.didFinishProcessingVideo()
                        self.getVideo()
                        
                    }
                })
            }else{
                isProcessing = false
                print("file not found")
            }
        }else{
            isProcessing = false
            print("no more files")
        }
    }
    
    func sendPercentage(_ percent: String) {
        
        delegate?.sendPerentageToViewController(percent)
    }
}
