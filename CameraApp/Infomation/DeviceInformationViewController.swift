//
//  DeviceInformationViewController.swift
//  CameraApp
//
//  Created by iMac on 05/10/2018.
//  Copyright © 2018 softech. All rights reserved.
//

import UIKit

class DeviceInformationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet weak var featureListTableView: UITableView!
    @IBOutlet weak var modelLabel: UILabel!
    var trainingModeVC = TrainingModeViewController()
    var listArray : Dictionary<String,Bool> = [:]
    
    
    var iPhone5Array : Dictionary<String,Bool> = ["Capture Photo":true,"Record Video":true,"View Photos":true,"Switch Front/Rear Camera":true, "Change Skope Binocular/Rifle": true, "Save Preset Settings" : true,"Get saved presets": true, "Stablization":true,"WaterMark":true,"Photo Filters":true,"Geo Tagging": true,"Save Raw Photo":false,"Timer":true,"Photo quality": true,"Telephoto": false,"Slow Motion": false,"Timelaps":true,"4K- Video":false,"Video quality": true, "Rotate Photo/Video":true,"Change ISO and Focus":true]
    
    var iPhone5sArray : Dictionary<String,Bool> = ["Capture Photo":true,"Record Video":true,"View Photos":true,"Switch Front/Rear Camera":true, "Change Skope Binocular/Rifle": true, "Save Preset Settings" : true,"Get saved presets": true, "Stablization":true,"WaterMark":true,"Photo Filters":true,"Geo Tagging": true,"Save Raw Photo":false,"Timer":true,"Photo quality": true,"Telephoto": false,"Slow Motion": true,"Timelaps":true,"4K-Video":false,"Video quality": true, "Rotate Photo/Video":true,"Change ISO and Focus":true]
    
    var iPhone6Array : Dictionary<String,Bool> = ["Capture Photo":true,"Record Video":true,"View Photos":true,"Switch Front/Rear Camera":true, "Change Skope Binocular/Rifle": true, "Save Preset Settings" : true,"Get saved presets": true, "Stablization":true,"WaterMark":true,"Photo Filters":true,"Geo Tagging": true,"Save Raw Photo":true,"Timer":true,"Photo quality": true,"Telephoto": false,"Slow Motion": true,"Timelaps":true,"4K-Video":true,"Video quality": true, "Rotate Photo/Video":true,"Change ISO and Focus":true]
    

    var iPhone6sArray : Dictionary<String,Bool> = ["Capture Photo":true,"Record Video":true,"View Photos":true,"Switch Front/Rear Camera":true, "Change Skope Binocular/Rifle": true, "Save Preset Settings" : true,"Get saved presets": true, "Stablization":true,"WaterMark":true,"Photo Filters":true,"Geo Tagging": true,"Save Raw Photo":true,"Timer":true,"Photo quality": true,"Telephoto": false,"Slow Motion": true,"Timelaps":true,"4K-Video":true,"Video quality": true, "Rotate Photo/Video":true,"Change ISO and Focus":true]
    var iPhone6PlusArray : Dictionary<String,Bool> = ["Capture Photo":true,"Record Video":true,"View Photos":true,"Switch Front/Rear Camera":true, "Change Skope Binocular/Rifle": true, "Save Preset Settings" : true,"Get saved presets": true, "Stablization":true,"WaterMark":true,"Photo Filters":true,"Geo Tagging": true,"Save Raw Photo":true,"Timer":true,"Photo quality": true,"Telephoto": false,"Slow Motion": true,"Timelaps":true,"4K-Video":true,"Video quality": true, "Rotate Photo/Video":true,"Change ISO and Focus":true]
    
    var iPhone6sPlusArray : Dictionary<String,Bool> = ["Capture Photo":true,"Record Video":true,"View Photos":true,"Switch Front/Rear Camera":true, "Change Skope Binocular/Rifle": true, "Save Preset Settings" : true,"Get saved presets": true, "Stablization":true,"WaterMark":true,"Photo Filters":true,"Geo Tagging": true,"Save Raw Photo":true,"Timer":true,"Photo quality": true,"Telephoto": false,"Slow Motion": true,"Timelaps":true,"4K-Video":true,"Video quality": true, "Rotate Photo/Video":true,"Change ISO and Focus":true]
    
    var iPhone7Array : Dictionary<String,Bool> = ["Capture Photo":true,"Record Video":true,"View Photos":true,"Switch Front/Rear Camera":true, "Change Skope Binocular/Rifle": true, "Save Preset Settings" : true,"Get saved presets": true, "Stablization":true,"WaterMark":true,"Photo Filters":true,"Geo Tagging": true,"Save Raw Photo":true,"Timer":true,"Photo quality": true,"Telephoto": false,"Slow Motion": true,"Timelaps":true,"4K-Video":true,"Video quality": true, "Rotate Photo/Video":true,"Change ISO and Focus":true]
    
    var iPhone7PlusArray : Dictionary<String,Bool> = ["Capture Photo":true,"Record Video":true,"View Photos":true,"Switch Front/Rear Camera":true, "Change Skope Binocular/Rifle": true, "Save Preset Settings" : true,"Get saved presets": true, "Stablization":true,"WaterMark":true,"Photo Filters":true,"Geo Tagging": true,"Save Raw Photo":true,"Timer":true,"Photo quality": true,"Telephoto": true,"Slow Motion": true,"Timelaps":true,"4K-Video":true,"Video quality": true, "Rotate Photo/Video":true,"Change ISO and Focus":true]
    
    var iPhone8Array : Dictionary<String,Bool> = ["Capture Photo":true,"Record Video":true,"View Photos":true,"Switch Front/Rear Camera":true, "Change Skope Binocular/Rifle": true, "Save Preset Settings" : true,"Get saved presets": true, "Stablization":true,"WaterMark":true,"Photo Filters":true,"Geo Tagging": true,"Save Raw Photo":true,"Timer":true,"Photo quality": true,"Telephoto": false,"Slow Motion": true,"Timelaps":false,"4K-Video":true,"Video quality": true, "Rotate Photo/Video":true,"Change ISO and Focus":true]
    
    var iPhone8PlusArray : Dictionary<String,Bool> = ["Capture Photo":true,"Record Video":true,"View Photos":true,"Switch Front/Rear Camera":true, "Change Skope Binocular/Rifle": true, "Save Preset Settings" : true,"Get saved presets": true, "Stablization":true,"WaterMark":true,"Photo Filters":true,"Geo Tagging": true,"Save Raw Photo":true,"Timer":true,"Photo quality": true,"Telephoto": true,"Slow Motion": true,"Timelaps":false,"4K-Video":true,"Video quality": true, "Rotate Photo/Video":true,"Change ISO and Focus":true]
    var iPhoneXArray : Dictionary<String,Bool> = ["Capture Photo":true,"Record Video":true,"View Photos":true,"Switch Front/Rear Camera":true, "Change Skope Binocular/Rifle": true, "Save Preset Settings" : true,"Get saved presets": true, "Stablization":true,"WaterMark":true,"Photo Filters":true,"Geo Tagging": true,"Save Raw Photo":true,"Timer":true,"Photo quality": true,"Telephoto": true,"Slow Motion": true,"Timelaps":false,"4K-Video":true,"Video quality": true, "Rotate Photo/Video":true,"Change ISO and Focus":true]
    
    var iPhoneXSArray : Dictionary<String,Bool> = ["Capture Photo":true,"Record Video":true,"View Photos":true,"Switch Front/Rear Camera":true, "Change Skope Binocular/Rifle": true, "Save Preset Settings" : true,"Get saved presets": true, "Stablization":true,"WaterMark":true,"Photo Filters":true,"Geo Tagging": true,"Save Raw Photo":true,"Timer":true,"Photo quality": true,"Telephoto": true,"Slow Motion": true,"Timelaps":false,"4K-Video":true,"Video quality": true, "Rotate Photo/Video":true,"Change ISO and Focus":true]
    
    var iPhoneXSMaxArray : Dictionary<String,Bool> = ["Capture Photo":true,"Record Video":true,"View Photos":true,"Switch Front/Rear Camera":true, "Change Skope Binocular/Rifle": true, "Save Preset Settings" : true,"Get saved presets": true, "Stablization":true,"WaterMark":true,"Photo Filters":true,"Geo Tagging": true,"Save Raw Photo":true,"Timer":true,"Photo quality": true,"Telephoto": true,"Slow Motion": true,"Timelaps":false,"4K-Video":true,"Video quality": true, "Rotate Photo/Video":true,"Change ISO and Focus":true]
    var iPhoneXRArray : Dictionary<String,Bool> = ["Capture Photo":true,"Record Video":true,"View Photos":true,"Switch Front/Rear Camera":true, "Change Skope Binocular/Rifle": true, "Save Preset Settings" : true,"Get saved presets": true, "Stablization":true,"WaterMark":true,"Photo Filters":true,"Geo Tagging": true,"Save Raw Photo":true,"Timer":true,"Photo quality": true,"Telephoto": true,"Slow Motion": true,"Timelaps":false,"4K-Video":true,"Video quality": true, "Rotate Photo/Video":true,"Change ISO and Focus":true]


    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.alpha = 0.5
        self.view.backgroundColor = .black
        
        // Do any additional setup after loading the view.
    }
 
    override func viewWillAppear(_ animated: Bool) {
      //  let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DeviceInformationViewController.dismissViewController))
      //  view.addGestureRecognizer(tap)
        checkDevice()
        
    }
    
    func checkDevice (){
          print(UIDevice.modelName)
        if UIDevice.modelName == "iPhone 5" {
            modelLabel.text = UIDevice.modelName
            listArray = iPhone5Array
        }else if UIDevice.modelName == "iPhone 5s" {
            listArray = iPhone5sArray
            modelLabel.text = UIDevice.modelName
            
        }else if UIDevice.modelName == "iPhone 6" {
            modelLabel.text = UIDevice.modelName
            listArray = iPhone6Array
        }else if UIDevice.modelName == "iPhone 6 Plus" {
            modelLabel.text = UIDevice.modelName
            listArray = iPhone6PlusArray
        }else if UIDevice.modelName == "iPhone 6s" {
             modelLabel.text = UIDevice.modelName
            listArray = iPhone6sArray
        }else if UIDevice.modelName == "iPhone 6s Plus" {
             modelLabel.text = UIDevice.modelName
            listArray = iPhone6sPlusArray
        }else if UIDevice.modelName == "iPhone 7" {
            modelLabel.text = UIDevice.modelName
            listArray = iPhone7Array
        }else if UIDevice.modelName == "iPhone 7 Plus" {
            modelLabel.text = UIDevice.modelName
            listArray = iPhone7PlusArray
        }else if UIDevice.modelName == "iPhone 8" {
            modelLabel.text = UIDevice.modelName
            listArray = iPhone8Array
        }else if UIDevice.modelName == "iPhone 8 Plus" {
            modelLabel.text = UIDevice.modelName
            listArray = iPhone8PlusArray
        }else if UIDevice.modelName == "iPhone X" {
            modelLabel.text = UIDevice.modelName
            listArray = iPhoneXArray
        }else if UIDevice.modelName == "iPhone XS" {
            modelLabel.text = UIDevice.modelName
            listArray = iPhoneXSArray
        }else if UIDevice.modelName == "iPhone XS Max" {
            modelLabel.text = UIDevice.modelName
            listArray = iPhoneXSMaxArray
        }else if UIDevice.modelName == "iPhone XR" {
            modelLabel.text = UIDevice.modelName
            listArray = iPhoneXRArray
        }
        
    }
    @objc func  dismissViewController (){
        self.dismiss(animated: true, completion: nil)
        trainingModeVC.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        dismissViewController ()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! InformationTableViewCell
        let lazyMapCollection = iPhone5Array.keys
        
        
        let stringArray = Array(lazyMapCollection.map { String($0) })
        
        let lazyMapCollection1 = listArray.values
        let stringArray1 = Array(lazyMapCollection1.map { Bool($0) })
        // let stringArray = Array(lazyMapCollection).map { String($0) } // also works
        print(stringArray)
        cell.informationLabel.text = stringArray[indexPath.row]
        
        if   stringArray1[indexPath.row] == true{
            cell.informationImageView.image = UIImage(named: "checked-checkbox")
        }else{
             cell.informationImageView.image = UIImage(named: "unchecked")
        }
        
        return cell
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
class InformationTableViewCell : UITableViewCell{
    @IBOutlet weak var informationLabel: UILabel!
    
    @IBOutlet weak var informationImageView: UIImageView!
}
