//
//  IntroductionButtonExtension.swift
//  CameraApp
//
//  Created by iMac on 09/10/2018.
//  Copyright © 2018 softech. All rights reserved.
//

import Foundation
import AVFoundation
import Photos
import AssetsLibrary
import CoreLocation
import MediaPlayer ////Only for hidding  Volume view
import GLKit
import CoreImage
import SVProgressHUD
import LLVideoEditor
import NVActivityIndicatorView
import AMTooltip
extension AVCamManualCameraViewController {
   
    func setIntroButtons(){
       
        //9970 select skope
        
        setIntroBottomContainerButton(screenView: nil, screenButton: scopeButtonMenu, screenImageView: nil, screenSlider: nil)
        
      
        //9971 flash
        setIntroBottomContainerButton(screenView: photFlashView, screenButton: nil, screenImageView: nil, screenSlider: nil)
        
        //9972 save rifle
        setIntroBottomContainerButton(screenView: nil, screenButton: saveRifleOutlet, screenImageView: nil, screenSlider: nil)
        
        //9973 telephoto
        setIntroBottomContainerButton(screenView: nil, screenButton: telePhotoButtonOulet, screenImageView: nil, screenSlider: nil)
        
        
        //9974 select rifle
        setIntroBottomContainerButton(screenView: nil, screenButton: selectRifleOulet, screenImageView: nil, screenSlider: nil)
        
        //9975 camera switch top
         setIntroBottomContainerButton(screenView: switchCameraPhotoTop, screenButton: nil, screenImageView: nil, screenSlider: nil)
        
        //9976 left iso button
        setIntroMainViewOnTopButton(screenView: isoLeftView, screenButton: nil, screenImageView: nil, screenSlider: nil, superView: self.view)
        
        //9977 left focus button
        setIntroMainViewOnBottomButton(screenView: focusLeftView, screenButton: nil, screenImageView: nil, screenSlider: nil, superView: self.view)
        
        //9978 rotation image view rifle scope
         setIntroMainViewOnBottomButton(screenView: nil, screenButton: nil, screenImageView: rotationImageView, screenSlider: nil, superView: self.view)
        
        //9979 iso rifle scope image view
         setIntroMainViewOnBottomButton(screenView: nil, screenButton: nil, screenImageView: brightImageView, screenSlider: nil, superView: self.view)
        
        //9980 focus rifle scope image view
         setIntroMainViewOnBottomButton(screenView: nil, screenButton: nil, screenImageView: focusImageView, screenSlider: nil, superView: self.view)
        
       //9981 up slider button outlet
        setIntroMainViewOnTopButton(screenView: nil, screenButton: upButtonOutlet, screenImageView: nil, screenSlider: nil, superView: self.view)
        
       //9982 up slider button outlet
        setIntroMainViewOnBottomButton(screenView: nil, screenButton: downButtonOulet, screenImageView: nil, screenSlider: nil, superView: self.view)
        
        //9983
        setIntroTopContainerButton(screenView: nil, screenButton: menuButton, screenImageView: nil, screenSlider: nil, screenLabel: nil, superView: bottomContainerView)
        
         //9984
        setVideoModeButtonTopConstraints(screenView: nil, screenButton: photoModeButton, screenImageView: nil, screenSlider: nil, screenLabel: nil, superView: bottomContainerView)
        //9985
        setIntroMainViewOnTopButton (screenView: binocularView, screenButton: nil, screenImageView: nil, screenSlider: nil, superView: self.selectScopeView)
        
        //9986
         setIntroMainViewOnTopButton(screenView: rifleView, screenButton: nil, screenImageView: nil, screenSlider: nil, superView:self.selectScopeView)
        //9987
        setIntroTopContainerButton(screenView: nil, screenButton: stablizationButtonOutlet, screenImageView: nil, screenSlider: nil, screenLabel: nil, superView: photoMenu)
        //9988
        setIntroTopContainerButton(screenView: nil, screenButton: filterButton, screenImageView: nil, screenSlider: nil, screenLabel: nil, superView: photoMenu)
        //9989
        setIntroTopContainerButton(screenView: nil, screenButton: nil, screenImageView: nil, screenSlider: nil, screenLabel: timerLabel, superView: photoMenu)
        //9990
        setIntroTopContainerButton(screenView: nil, screenButton: nil, screenImageView: nil, screenSlider: nil, screenLabel: waterMarkLabel, superView: photoMenu)
        //9991
        setIntroTopContainerButton(screenView: nil, screenButton: nil, screenImageView: nil, screenSlider: nil, screenLabel: geoTaggingLabel, superView: photoMenu)
        //9992
        setIntroTopContainerButton(screenView: nil, screenButton: nil, screenImageView: nil, screenSlider: nil, screenLabel: fileTypeLabel, superView: photoMenu)
        //9993
        setIntroTopContainerButton(screenView: nil, screenButton: photoQualityButtonOutlet, screenImageView: nil, screenSlider: nil, screenLabel: nil, superView: photoMenu)
        //9994
        setIntroTopContainerButton(screenView: nil, screenButton: featureListButtonOutlet, screenImageView: nil, screenSlider: nil, screenLabel: nil, superView: photoMenu)
        //9995
        setIntroTopContainerButton(screenView: nil, screenButton: nil, screenImageView: nil, screenSlider: nil, screenLabel: timeLapsLabel, superView: videoMenu)
        //9996
        setIntroTopContainerButton(screenView: nil, screenButton: nil, screenImageView: nil, screenSlider: nil, screenLabel: slowMoLabel, superView: videoMenu)
        //9997
        setIntroTopContainerButton(screenView: nil, screenButton: nil, screenImageView: nil, screenSlider: nil, screenLabel: f4kVideoLabel, superView: videoMenu)
        //9998
        setIntroTopContainerButton(screenView: nil, screenButton: videoQualityButtonOutlet, screenImageView: nil, screenSlider: nil, screenLabel: nil, superView: videoMenu)
        
        

        }
    
    func showIntroButtons(){
        //        //MARK: - display of buttons
                if !dataHandler.getTrainingMode(){
        
                    let button0 = self.view.viewWithTag(9970)
                    button0?.isHidden = true
        
                    let button1 = self.view.viewWithTag(9971)
                    button1?.isHidden = true
        
                    let button2 = self.view.viewWithTag(9972)
                    button2?.isHidden = true
        
                    let button3 = self.view.viewWithTag(9973)
                    button3?.isHidden = true
        
                    let button4 = self.view.viewWithTag(9974)
                    button4?.isHidden = true
        
                    let button5 = self.view.viewWithTag(9975)
                    button5?.isHidden = true
        
                    let button6 = self.view.viewWithTag(9976)
                    button6?.isHidden = true
        
                    let button7 = self.view.viewWithTag(9977)
                    button7?.isHidden = true
        
                    let button8 = self.view.viewWithTag(9978)
                    button8?.isHidden = true
        
                    let button9 = self.view.viewWithTag(9979)
                    button9?.isHidden = true
        
                    let button10 = self.view.viewWithTag(9980)
                    button10?.isHidden = true
        
                    let button11 = self.view.viewWithTag(9981)
                    button11?.isHidden = true
        
                    let button12 = self.view.viewWithTag(9982)
                    button12?.isHidden = true
        
                    let button13 = self.view.viewWithTag(9983)
                    button13?.isHidden = true
        
                    let button14 = self.view.viewWithTag(9984)
                    button14?.isHidden = true
        
                    let button15 = self.view.viewWithTag(9985)
                    button15?.isHidden = true
        
                    let button16 = self.view.viewWithTag(9986)
                    button16?.isHidden = true
        
                    let button17 = self.view.viewWithTag(9987)
                    button17?.isHidden = true
        
                    let button18 = self.view.viewWithTag(9988)
                    button18?.isHidden = true
        
                    let button19 = self.view.viewWithTag(9989)
                    button19?.isHidden = true
        
                    let button20 = self.view.viewWithTag(9990)
                    button20?.isHidden = true
        
                    let button21 = self.view.viewWithTag(9991)
                    button21?.isHidden = true
        
                    let button22 = self.view.viewWithTag(9992)
                    button22?.isHidden = true
        
                    let button23 = self.view.viewWithTag(9993)
                    button23?.isHidden = true
        
                    let button24 = self.view.viewWithTag(9994)
                    button24?.isHidden = true
        
                    let button25 = self.view.viewWithTag(9995)
                    button25?.isHidden = true
        
                    let button26 = self.view.viewWithTag(9996)
                    button26?.isHidden = true
        
                    let button27 = self.view.viewWithTag(9997)
                    button27?.isHidden = true
        
                    let button28 = self.view.viewWithTag(9998)
                    button28?.isHidden = true
                    
                    turnOffTrainingModeView.isHidden = true
        
                }else{
        if dataHandlerButton.getIntroductionSettings(key: 9970).hasShown == false{
            let button = self.view.viewWithTag(9970)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9970)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9971).hasShown == false{
            let button = self.view.viewWithTag(9971)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9971)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9972).hasShown == false{
            let button = self.view.viewWithTag(9972)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9972)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9973).hasShown == false{
            let button = self.view.viewWithTag(9973)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9973)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9974).hasShown == false{
            let button = self.view.viewWithTag(9974)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9974)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9975).hasShown == false{
            let button = self.view.viewWithTag(9975)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9975)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9976).hasShown == false{
            let button = self.view.viewWithTag(9976)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9976)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9977).hasShown == false{
            let button = self.view.viewWithTag(9977)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9977)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9978).hasShown == false{
            let button = self.view.viewWithTag(9978)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9978)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9979).hasShown == false{
            let button = self.view.viewWithTag(9979)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9979)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9980).hasShown == false{
            let button = self.view.viewWithTag(9980)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9980)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9981).hasShown == false{
            let button = self.view.viewWithTag(9981)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9981)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9982).hasShown == false{
            let button = self.view.viewWithTag(9982)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9982)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9983).hasShown == false{
            let button = self.view.viewWithTag(9983)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9983)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9984).hasShown == false{
            let button = self.view.viewWithTag(9984)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9984)
            button?.isHidden = true
        }
        
        if dataHandlerButton.getIntroductionSettings(key: 9985).hasShown == false{
            let button = self.view.viewWithTag(9985)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9985)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9986).hasShown == false{
            let button = self.view.viewWithTag(9986)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9986)
            button?.isHidden = true
        }
        
        ///////////////////
        if dataHandlerButton.getIntroductionSettings(key: 9987).hasShown == false{
            let button = self.view.viewWithTag(9987)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9987)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9988).hasShown == false{
            let button = self.view.viewWithTag(9988)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9988)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9989).hasShown == false{
            let button = self.view.viewWithTag(9989)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9989)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9990).hasShown == false{
            let button = self.view.viewWithTag(9990)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9990)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9991).hasShown == false{
            let button = self.view.viewWithTag(9991)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9991)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9992).hasShown == false{
            let button = self.view.viewWithTag(9992)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9992)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9993).hasShown == false{
            let button = self.view.viewWithTag(9993)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9993)
            button?.isHidden = true
        }
        
        if dataHandlerButton.getIntroductionSettings(key: 9994).hasShown == false{
            let button = self.view.viewWithTag(9994)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9994)
            button?.isHidden = true
        }
        
        if dataHandlerButton.getIntroductionSettings(key: 9995).hasShown == false{
            let button = self.view.viewWithTag(9995)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9995)
            button?.isHidden = true
        }
        
        if dataHandlerButton.getIntroductionSettings(key: 9996).hasShown == false{
            let button = self.view.viewWithTag(9996)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9996)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9997).hasShown == false{
            let button = self.view.viewWithTag(9997)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9997)
            button?.isHidden = true
        }
        if dataHandlerButton.getIntroductionSettings(key: 9998).hasShown == false{
            let button = self.view.viewWithTag(9998)
            button?.isHidden = false
        }else{
            let button = self.view.viewWithTag(9998)
            button?.isHidden = true
            }
        turnOffTrainingModeView.isHidden = false
        }
        
        
    }
     //MARK: - SmallViews On Bottom Side
    func setIntroBottomContainerButton(screenView : UIView?, screenButton : UIButton? , screenImageView : UIImageView?,screenSlider : UISlider?) {
        var button1 = UIButton()
        button1.setImage(UIImage(named: "help 4"), for: .normal)
        self.topContainerView.addSubview(button1)
        
        
        button1.tag = Int(buttonTag + String(describing:buttonTagIndex))!
        button1.addTarget(self, action: #selector(AVCamManualCameraViewController.infoButtonAction(_:)), for: .touchUpInside)
        button1.translatesAutoresizingMaskIntoConstraints = false
        button1.widthAnchor.constraint(equalToConstant: 15).isActive = true
        button1.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        
        if screenView != nil{
            button1.topAnchor.constraint(equalTo: (screenView?.bottomAnchor)!, constant: 0).isActive = true
            
            button1.centerXAnchor.constraint(equalTo: (screenView?.centerXAnchor)!, constant: 0).isActive = true
            print(button1.tag)
        }
        if screenButton != nil{
           
            button1.topAnchor.constraint(equalTo: (screenButton?.bottomAnchor)!, constant: 0).isActive = true
           
            button1.centerXAnchor.constraint(equalTo: (screenButton?.centerXAnchor)!, constant: 0).isActive = true
            print(button1.tag)
          
        }
        if screenImageView != nil{
            button1.topAnchor.constraint(equalTo: (screenImageView?.bottomAnchor)!, constant: 0).isActive = true
            
            button1.centerXAnchor.constraint(equalTo: (screenImageView?.centerXAnchor)!, constant: 0).isActive = true
            print(button1.tag)
        }
        
        if screenSlider != nil{
            button1.topAnchor.constraint(equalTo: (screenSlider?.bottomAnchor)!, constant: 0).isActive = true
            
            button1.centerXAnchor.constraint(equalTo: (screenSlider?.centerXAnchor)!, constant: 0).isActive = true
            print(button1.tag)
        }
        buttonTagIndex += 1
    }
    
    
    func setVideoModeButtonTopConstraints(screenView : UIView?, screenButton : UIButton? , screenImageView : UIImageView?,screenSlider : UISlider?,screenLabel : UILabel?,superView : UIView?) {
        var button1 = UIButton()
        button1.setImage(UIImage(named: "help 4"), for: .normal)
        button1.tag = Int(buttonTag + String(describing:buttonTagIndex))!
        button1.addTarget(self, action: #selector(AVCamManualCameraViewController.infoButtonAction(_:)), for: .touchUpInside)
        superView?.addSubview(button1)
        
        
        button1.translatesAutoresizingMaskIntoConstraints = false
        button1.widthAnchor.constraint(equalToConstant: 15).isActive = true
        button1.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        if screenButton != nil{
            
            button1.topAnchor.constraint(equalTo: (superView?.topAnchor)!, constant: 1).isActive = true
            button1.centerXAnchor.constraint(equalTo: (screenButton?.centerXAnchor)!, constant: 0).isActive = true
            print(button1.tag)
            
        }
         buttonTagIndex += 1
    }
    
    //MARK: - SmallViews On Top Side
    func setIntroTopContainerButton(screenView : UIView?, screenButton : UIButton? , screenImageView : UIImageView?,screenSlider : UISlider?,screenLabel : UILabel?,superView : UIView?) {
        var button1 = UIButton()
        button1.setImage(UIImage(named: "help 4"), for: .normal)
        button1.tag = Int(buttonTag + String(describing:buttonTagIndex))!
        button1.addTarget(self, action: #selector(AVCamManualCameraViewController.infoButtonAction(_:)), for: .touchUpInside)
        superView?.addSubview(button1)
        
       
        button1.translatesAutoresizingMaskIntoConstraints = false
        button1.widthAnchor.constraint(equalToConstant: 15).isActive = true
        button1.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        if screenView != nil{
            button1.bottomAnchor.constraint(equalTo: (screenView?.topAnchor)!, constant: 4).isActive = true
           
            button1.centerXAnchor.constraint(equalTo: (screenView?.centerXAnchor)!, constant: 0).isActive = true
            print(button1.tag)
        }
        if screenButton != nil{
            
            button1.bottomAnchor.constraint(equalTo: (screenButton?.topAnchor)!, constant: 4).isActive = true
            button1.centerXAnchor.constraint(equalTo: (screenButton?.centerXAnchor)!, constant: 0).isActive = true
            print(button1.tag)
            
        }
        if screenImageView != nil{
            button1.bottomAnchor.constraint(equalTo: (screenImageView?.topAnchor)!, constant: 4).isActive = true

            button1.centerXAnchor.constraint(equalTo: (screenImageView?.centerXAnchor)!, constant: 0).isActive = true
            print(button1.tag)
        }
        
        if screenSlider != nil{
            button1.bottomAnchor.constraint(equalTo: (screenSlider?.topAnchor)!, constant: 4).isActive = true

            button1.centerXAnchor.constraint(equalTo: (screenSlider?.centerXAnchor)!, constant: 0).isActive = true
            print(button1.tag)
        }
        if screenLabel != nil{
            button1.bottomAnchor.constraint(equalTo: (screenLabel?.topAnchor)!, constant: 4).isActive = true
            
            button1.centerXAnchor.constraint(equalTo: (screenLabel?.centerXAnchor)!, constant: 0).isActive = true
            print(button1.tag)
        }
        buttonTagIndex += 1
    }
    
    //MARK: - MainView On Top Side
    func setIntroMainViewOnTopButton(screenView : UIView?, screenButton : UIButton? , screenImageView : UIImageView?,screenSlider : UISlider?, superView : UIView?) {
        var button1 = UIButton()
        button1.setImage(UIImage(named: "help 4"), for: .normal)
        button1.tag = Int(buttonTag + String(describing:buttonTagIndex))!
        button1.addTarget(self, action: #selector(AVCamManualCameraViewController.infoButtonAction(_:)), for: .touchUpInside)
        superView?.addSubview(button1)
        
        
        button1.translatesAutoresizingMaskIntoConstraints = false
        button1.widthAnchor.constraint(equalToConstant: 15).isActive = true
        button1.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        if screenView != nil{
            button1.topAnchor.constraint(equalTo: (screenView?.topAnchor)!, constant: -18).isActive = true
            
            button1.centerXAnchor.constraint(equalTo: (screenView?.centerXAnchor)!, constant: 0).isActive = true
            print(button1.tag)
        }
        if screenButton != nil{
            
            button1.topAnchor.constraint(equalTo: (screenButton?.topAnchor)!, constant: -18).isActive = true
            button1.centerXAnchor.constraint(equalTo: (screenButton?.centerXAnchor)!, constant: 0).isActive = true
            print(button1.tag)
            
        }
        if screenImageView != nil{
            button1.topAnchor.constraint(equalTo: (screenImageView?.topAnchor)!, constant: -18).isActive = true
            
            button1.centerXAnchor.constraint(equalTo: (screenImageView?.centerXAnchor)!, constant: 0).isActive = true
            print(button1.tag)
        }
        
        if screenSlider != nil{
            button1.topAnchor.constraint(equalTo: (screenSlider?.topAnchor)!, constant: -18).isActive = true
            
            button1.centerXAnchor.constraint(equalTo: (screenSlider?.centerXAnchor)!, constant: 0).isActive = true
            print(button1.tag)
        }
        buttonTagIndex += 1
    }
    //MARK: - MainView On Bottom Side
    func setIntroMainViewOnBottomButton(screenView : UIView?, screenButton : UIButton? , screenImageView : UIImageView?,screenSlider : UISlider?, superView : UIView?) {
        var button1 = UIButton()
        button1.setImage(UIImage(named: "help 4"), for: .normal)
        button1.tag = Int(buttonTag + String(describing:buttonTagIndex))!
        button1.addTarget(self, action: #selector(AVCamManualCameraViewController.infoButtonAction(_:)), for: .touchUpInside)
        superView?.addSubview(button1)
        
        
        button1.translatesAutoresizingMaskIntoConstraints = false
        button1.widthAnchor.constraint(equalToConstant: 15).isActive = true
        button1.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        if screenView != nil{
            button1.topAnchor.constraint(equalTo: (screenView?.bottomAnchor)!, constant: 12).isActive = true
            
            button1.centerXAnchor.constraint(equalTo: (screenView?.centerXAnchor)!, constant: 0).isActive = true
            print(button1.tag)
        }
        if screenButton != nil{
            
            button1.topAnchor.constraint(equalTo: (screenButton?.bottomAnchor)!, constant: 12).isActive = true
            button1.centerXAnchor.constraint(equalTo: (screenButton?.centerXAnchor)!, constant: 0).isActive = true
            print(button1.tag)
            
        }
        if screenImageView != nil{
            button1.topAnchor.constraint(equalTo: (screenImageView?.bottomAnchor)!, constant: 12).isActive = true
            
            button1.centerXAnchor.constraint(equalTo: (screenImageView?.centerXAnchor)!, constant: 0).isActive = true
            print(button1.tag)
        }
        
        if screenSlider != nil{
            button1.topAnchor.constraint(equalTo: (screenSlider?.bottomAnchor)!, constant: 12).isActive = true
            
            button1.centerXAnchor.constraint(equalTo: (screenSlider?.centerXAnchor)!, constant: 0).isActive = true
            print(button1.tag)
        }
        buttonTagIndex += 1
    }
    @objc func infoButtonAction(_ sender:UIButton){
        print(sender.tag)
        if sender.tag == 9970{
        let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
        AMTooltipView(
                options:AMTooltipViewOptions(
                    textColor: UIColor.white,
                    textBoxBackgroundColor: UIColor.black,
                    addOverlayView: true,
                    lineColor: UIColor.black,
                    dotColor: UIColor.yellow,
                    dotBorderColor: UIColor.yellow
                ),
                message: "If you want to change your selected skope to other like e.g you are in rifle skope mode but now you want to switch back to Binocular skope please tap this option.",
                focusView: mybutton,
                target: self){
            let button = self.view.viewWithTag(sender.tag)
            button?.isHidden = true
            self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
            }
        }else
        if sender.tag == 9971{
            let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
            AMTooltipView(
                options:AMTooltipViewOptions(
                    textColor: UIColor.white,
                    textBoxBackgroundColor: UIColor.black,
                    addOverlayView: true,
                    lineColor: UIColor.black,
                    dotColor: UIColor.yellow,
                    dotBorderColor: UIColor.yellow
                ),
                message: "Use this options for turning flash on , off and on auto mode in which app will self decide to use flash or not.",
                focusView: mybutton,
                target: self){
                    let button = self.view.viewWithTag(sender.tag)
                    button?.isHidden = true
                    self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
            }
            
        }else
            if sender.tag == 9972{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "This option will save state of all settings being applied at time like e.g rotation, focus value, brightness, video quality, slow motion etc you can save them with some unique name so that in future you don’t have to reconfigure every thing again for these specific scene settings.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
        }else if sender.tag == 9973{
                    let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                    AMTooltipView(
                        options:AMTooltipViewOptions(
                            textColor: UIColor.white,
                            textBoxBackgroundColor: UIColor.black,
                            addOverlayView: true,
                            lineColor: UIColor.black,
                            dotColor: UIColor.yellow,
                            dotBorderColor: UIColor.yellow
                        ),
                        message: "Use this option to switch between telephoto and wide angle camera of your phone.",
                        focusView: mybutton,
                        target: self){
                            let button = self.view.viewWithTag(sender.tag)
                            button?.isHidden = true
                            self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
            }
                    
            }else if sender.tag == 9974{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "Load any setting which you saved previously during recordings for specific scenes from this menu.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9975{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "Use this option for switching between front and back camera.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9976{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "Slide up and down for changing brightness, please note sliding below centre will decrease value and sliding up would increase.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9977{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "Slide up and down for changing focus on object, please note sliding below centre will decrease value and sliding up would increase.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9978{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "Slide up and down for changing angle of recording, please note sliding below centre will decrease value and sliding up would increase.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9979{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "Slide up and down for changing brightness level , please note sliding below centre will decrease value and sliding up would increase.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9980{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "Slide up and down for changing focus on object , please note sliding below centre will decrease value and sliding up would increase.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9981{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "Tap + icon to increase angle of recording by 1 degree.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9982{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "Tap - icon to increase angle of recording by 1 degree.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9983{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: false,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "Configure options like slow motion, video / photo quality , geo tagging, stabilisation etc.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9984{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: false,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "Tap on this icon to go from video mode to photo mode. Vise versa when use is in photo mode.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9985{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: false,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "If You want to launch camera app with Binocular Skope attached with your phone then please choose this option as it would have controls settings specific to this Skope.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9986{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "If You want to launch camera app with Rifle Skope attached with your phone then please choose this option as it would have controls settings specific to this Skope.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9987{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "You can use Image stabilization (IS) that helps you to reduce blurring associated with the motion of a camera or other imaging device during exposure.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9988{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "You can use filters which a technique that changes the appearance of an image or part of an image by altering the shades and colors of the pixels in some manner.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9989{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "You can use timer that gives a delay between pressing the shutter release and the shutter's firing.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9990{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "You can use watermarking photos that involves adding some words to a photo, like a stamp, that identifies the photo as your own.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9991{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "You can use GeoTagging feature that helps to add geographical identification metadata to photograph.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9992{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "You can save photographs in JPEG/RAW format or both formats at same time simply select options according to your requirements.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9993{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "You can save photographs with different qualities in order save space in your device simply select qaulity according to your requirements.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9994{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "You can see supported features in your device you can simply tap the features list button.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9995{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "You can use Time-lapse photography which is a technique whereby the frequency at which film frames are captured (the frame rate) is much lower than that used to view the sequence. When played at normal speed, time appears to be moving faster and thus lapsing.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9996{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "You can record Slow motion video which is (commonly abbreviated as slo-mo or slow-mo) is an effect in film-making whereby time appears to be slowed down. A term for creating slow motion film is overcranking which refers to hand cranking an early camera at a faster rate than normal (i.e. faster than 24 frames per second).",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9997{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "You can record 4K resolution video, also called 4K, refers to a horizontal display resolution of approximately 4,000 pixels.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
            }else if sender.tag == 9998{
                let mybutton = self.view.viewWithTag(sender.tag) as! UIButton
                AMTooltipView(
                    options:AMTooltipViewOptions(
                        textColor: UIColor.white,
                        textBoxBackgroundColor: UIColor.black,
                        addOverlayView: true,
                        lineColor: UIColor.black,
                        dotColor: UIColor.yellow,
                        dotBorderColor: UIColor.yellow
                    ),
                    message: "You can save video with different qualities in order save space in your device simply select qaulity according to your requirements.",
                    focusView: mybutton,
                    target: self){
                        let button = self.view.viewWithTag(sender.tag)
                        button?.isHidden = true
                        self.dataHandler.updateIntroductionSettings(key: Int16(sender.tag))
                }
                
        }
    }
    
}

