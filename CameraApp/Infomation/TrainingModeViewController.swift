//
//  TrainingModeViewController.swift
//  CameraApp
//
//  Created by iMac on 16/10/2018.
//  Copyright © 2018 softech. All rights reserved.
//

import UIKit

class TrainingModeViewController: UIViewController {

    let dataHandler = DataHandler()
    @IBOutlet weak var yesButtonOutlet: UIButton!
    @IBOutlet weak var noButtonOutlet: UIButton!
    @IBOutlet weak var modelView: UIView!
    var showIntroButton = AVCamManualCameraViewController()
    
    //MARK: - ------ VIEWCONTROLLERS METHODS -------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.alpha = 0.5
        self.modelView.alpha = 1.0
        
        self.modelView.layer.cornerRadius = 15
        self.modelView.layer.borderWidth = 1
        self.modelView.layer.borderColor = UIColor.white.cgColor
        self.modelView.clipsToBounds = true
        
        self.yesButtonOutlet.layer.borderWidth = 1
        self.yesButtonOutlet.layer.borderColor = UIColor.white.cgColor
        
        self.noButtonOutlet.layer.borderWidth = 1
        self.noButtonOutlet.layer.borderColor = UIColor.white.cgColor
    }

    //MARK: - ------ IBACTIONS -------
    
    @IBAction func yesButtonAction(_ sender: Any) {
        dataHandler.saveTrainingMode(true)
        showFeatureList()
    }
    
    @IBAction func noButtonAction(_ sender: Any) {
        dataHandler.saveTrainingMode(false)
        showFeatureList()
    }
    
    //MARK: - ------ PRIVATE METHODS -------
    
    func showFeatureList(){
        let sB: UIStoryboard = UIStoryboard(name: "Main", bundle: nil) //name of your storyboard
        let newVC = sB.instantiateViewController(withIdentifier: "DeviceInformationViewController") as! DeviceInformationViewController //if you have the restoration ID, otherwise use 'ModalViewController'
        newVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        newVC.modalPresentationStyle =  UIModalPresentationStyle.fullScreen
        newVC.trainingModeVC = self
        self.present(newVC, animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
