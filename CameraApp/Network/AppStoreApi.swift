//
//  AppStoreApi.swift
//  CameraApp
//
//  Created by iMac on 20/02/2019.
//  Copyright © 2019 softech. All rights reserved.
//

import Foundation
import Alamofire
import KeychainSwift
import FirebaseDatabase


class iTunesPaymentResponse{
    
    class func getPurchaseResponse(_ data:Data? , _ base64String : String? ,completionHandler: @escaping (_ payment : Payment, _ error : String? ) ->())  {
        var base64EncodedReceipt = ""
        if let newData = data {
             base64EncodedReceipt = newData.base64EncodedString(options: .init(rawValue: 0))
        }else{
            base64EncodedReceipt = base64String!
        }
        
        let keyChain = KeychainSwift()
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
            
        }else{
            if let payment = keyChain.get("paymentInfo") as? Payment{
                if payment.paid{
                    if payment.type == "iap"{
                        if Int(payment.expiry_date) > Date().millisecondsSince1970{
                            Product.purchased = true
                        }else{
                            Product.purchased = false
                        }
                    }else if payment.type == "promo"{
                        Product.purchased = true
                    }
                }else{
                    Product.purchased = false
                }
                completionHandler(payment, nil)
            }else{
                completionHandler(Payment(), "Failed")
            }
            print("Internet Connection not Available!")
        }
    let requestContents:[String:String] = ["receipt-data":base64EncodedReceipt, "password": "9eecb29cb7bc4b68b89afa1d31d43c8a"]
    Alamofire.request("https://buy.itunes.apple.com/verifyReceipt", method: .post, parameters: requestContents,encoding: JSONEncoding.default, headers: nil).responseJSON {
        response in
        let payment = Payment()
        switch response.result {
        case .success:
            if let data = response.result.value as? [String:Any]{
                if let receipts = data["latest_receipt_info"]{
                    if let receipt = receipts as? NSArray{
                        for rec in receipt{
                            if let recValue = rec as? [String:Any]{
                                if let expireMS = recValue["expires_date_ms"] as? String{
                                    let ref1 = Database.database().reference()
                                    if let uniqueID = keyChain.get("DeviceID"){
                                        if Int(expireMS)! > Date().millisecondsSince1970{
                                            payment.expiry_date = Int(expireMS)!
                                            payment.paid = true
                                            payment.type = "iap"
                                            payment.data = base64EncodedReceipt
                                            ref1.child("UserPayments").child("PaidUsers").child(uniqueID).setValue(["expiry_date":expireMS,"paid":true,"type":"iap","data":base64EncodedReceipt])
                                            Product.purchased = true
                                        }else{
                                            payment.expiry_date = 0
                                            payment.paid = false
                                            payment.type = "iap"
                                            payment.data = base64EncodedReceipt
                                            ref1.child("UserPayments").child("PaidUsers").child(uniqueID).setValue(["expiry_date":"0","paid":false,"type":"iap","data":""])
                                            Product.purchased = false
                                        }
                                        keyChain.set(payment.toJsonString(), forKey: "paymentInfo")
                                        completionHandler(payment, "Success")
                                    }
                                }
                            }
                        }
                    }
                }
                
            }
            
            break
        case .failure(let error):
       
        completionHandler(Payment(), error.localizedDescription)
           
        }
    }
    
}
}
