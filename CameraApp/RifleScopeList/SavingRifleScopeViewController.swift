//
//  SavingRifleScopeViewController.swift
//  CameraApp
//
//  Created by iMac on 01/04/2019.
//  Copyright © 2019 softech. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import UIKit
import AVFoundation
import Photos
import AssetsLibrary
import CoreLocation
import MediaPlayer ////Only for hidding  Volume view
import GLKit
import CoreImage
import SVProgressHUD
import LLVideoEditor
import NVActivityIndicatorView
import MediaWatermark

protocol CloseButtonDelegate {
    func deleteRow(cell: RifleScopeVideosCell)
}

class CoreDataVideos {
    static let shared = CoreDataVideos()
    var rifleVideos: [RifleScopeVideos] = []
    
}

class SavingRifleScopeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CheckVideosDelegate, CloseButtonDelegate {
    

    
    //MARK: - IBOUTLETS & PROPERTIES
    
    
    @IBOutlet weak var doneView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var processingCountLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: NVActivityIndicatorView!
    @IBOutlet weak var backButtonOutlet: UIButton!
    let dataHandler = DataHandler()
    var rifleVideos: [RifleScopeVideos] = []
    var imagePath = ""
    var videoConverted = false
    var processingPercent = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.isEditing = true
        
        rifleVideos = CoreDataVideos.shared.rifleVideos
        if rifleVideos.count > 0 {
            processingCountLabel.text = "\(rifleVideos.count)"
            activityIndicatorView.color = #colorLiteral(red: 0.8250325322, green: 0.8026339412, blue: 0.2692880332, alpha: 1)
            activityIndicatorView.type = .ballRotateChase
            processingCountLabel.isHidden = false
            activityIndicatorView.startAnimating()
            self.view.sendSubview(toBack: doneView )
            
        }else{
            self.view.bringSubview(toFront: doneView)
            processingCountLabel.isHidden = true
            activityIndicatorView.stopAnimating()
        }

       // CheckVideos.shared.getVideo()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        CheckVideos.shared.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)

    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func backButtonTapped(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func deleteRow(cell: RifleScopeVideosCell) {
        
        guard  let indexpath = tableView.indexPath(for: cell)  else {
            return
        }
        let imagePAth = (DataHandler().getDirectoryPath() as NSString).appending("/\(CoreDataVideos.shared.rifleVideos[indexpath.row].path!)")
        let fileName = CoreDataVideos.shared.rifleVideos[indexpath.row].path!.components(separatedBy: "/")
        dataHandler.deleteFile(filePath: imagePAth, fileName: fileName.last! )
        rifleVideos = CoreDataVideos.shared.rifleVideos
        processingCountLabel.text = "\(rifleVideos.count)"
        tableView.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rifleVideos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "riflecell", for: indexPath) as! RifleScopeVideosCell
        cell.indexPath = indexPath
        cell.delegate = self
        
        if indexPath.row == 0 {
            cell.percentLabel.text = processingPercent+"%"
            cell.deleteRowButton.isHidden = true
            
        }else{
            cell.percentLabel.text = ""
        }
        if let imageData = rifleVideos[indexPath.row].thumbnail{
            cell.videoImageView.image = UIImage(data: imageData as Data)
        }
            cell.nameLabel.text = "PhoneSkope Video \(indexPath.row)"
        
        if let fileSizeWithUnit = rifleVideos[indexPath.row].size as? String{
            cell.sizeLabel.text = "Size: "+ByteCountFormatter.string(fromByteCount: Int64(fileSizeWithUnit)!, countStyle: .file)
        }
        if let duration = rifleVideos[indexPath.row].duration {
            cell.durationLabel.text = "Duration: "+duration
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
    
        return UITableViewCellEditingStyle.none
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView (_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        if indexPath.row == 0 {
            return false
        }
        return true
    }
    
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
      //  DispatchQueue.global(qos: .background).async {
        let movedObject = CoreDataVideos.shared.rifleVideos[sourceIndexPath.row]
        movedObject.serialno = Int16(destinationIndexPath.row + 1)
        CoreDataVideos.shared.rifleVideos[destinationIndexPath.row].serialno = Int16(sourceIndexPath.row + 1)
        CoreDataVideos.shared.rifleVideos.remove(at: sourceIndexPath.row)
        CoreDataVideos.shared.rifleVideos.insert(movedObject, at: destinationIndexPath.row)
        self.dataHandler.updateSaveVideoPathArray(sourceIndex: sourceIndexPath.row , destinationIndex: destinationIndexPath.row)

    }
    
    func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        if proposedDestinationIndexPath.row == 0 {
            return IndexPath(row: 1, section: proposedDestinationIndexPath.section)
        }
        return proposedDestinationIndexPath
    }
    
    func sendPerentageToViewController(_ percent: String) {
        rifleVideos = CoreDataVideos.shared.rifleVideos
        if self.rifleVideos.count != 0 {
            self.processingPercent = percent
            DispatchQueue.main.async {
                self.tableView.reloadRows(at: [IndexPath(row:0, section: 0)], with: .none)
            }
        }
    }
    
    func didFinishProcessingVideo() {
      //  CoreDataVideos.shared.rifleVideos = dataHandler.getSaveVideoPath()
        rifleVideos = CoreDataVideos.shared.rifleVideos
        processingCountLabel.text = "\(rifleVideos.count)"
        tableView.reloadData()
        if rifleVideos.count == 0 {
            activityIndicatorView.stopAnimating()
            processingCountLabel.text = ""
            self.view.bringSubview(toFront: doneView)
            
        }else{
            self.view.sendSubview(toBack: doneView )
        }
    }
}

class RifleScopeVideosCell: UITableViewCell {
    @IBOutlet weak var deleteRowButton: UIButton!
    @IBOutlet weak var percentLabel: UILabel!
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    var indexPath: IndexPath?
    
    var delegate: CloseButtonDelegate?
    
    @IBAction func closeButtonAction(_ sender: Any) {
        delegate?.deleteRow(cell: self)
        
    }
    
}
