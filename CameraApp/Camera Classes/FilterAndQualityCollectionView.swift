//
//  FilterAndQualityCollectionView.swift
//  CameraApp
//
//  Created by iMac on 06/09/2018.
//  Copyright © 2018 softech. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Photos
import AssetsLibrary
import CoreLocation
import MediaPlayer ////Only for hidding  Volume view
import GLKit
import CoreImage
import SVProgressHUD
import LLVideoEditor
import NVActivityIndicatorView



extension AVCamManualCameraViewController {
    
    @IBAction func filterModeFunction(_ sender : UIButton?) {
        
        
        self._glkView?.isHidden = false
        configureFilterRendering()
        isFilterPhoto = true
        self.previewView.isHidden = true
        if sender != nil {
            self.filterCollectionView.isHidden = false
        }
        
    }
    
    func configureFilterRendering() {
        
        if self.videoDataOutput == nil {
            let videoDataOutput = AVCaptureVideoDataOutput()
            videoDataOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString) : NSNumber(value: kCVPixelFormatType_32BGRA as UInt32)] as [String : Any]
            
            videoDataOutput.alwaysDiscardsLateVideoFrames = true
            videoDataOutput.setSampleBufferDelegate(self, queue: self.sessionQueue)
            
            self.session.beginConfiguration()
            self.session.sessionPreset = AVCaptureSession.Preset(rawValue: photoQuality)
            self.session.commitConfiguration()
            
            if self.session.canAddOutput(videoDataOutput) {
                self.session.addOutput(videoDataOutput)
                self.videoDataOutput = videoDataOutput
            }
        }
    }
    //MARK:- REMOVE VIDEO DATA SESSION
    func removeVideoDataOutputFromSession() {
        
        if self.videoDataOutput != nil {
            self.session.beginConfiguration()
            self.session.removeOutput(self.videoDataOutput!)
            self.session.commitConfiguration()
            self.videoDataOutput = nil
            self.currentFilter = nil
            self.filterCounter = -1
            self._glkView.isHidden = true
        }
    }
    
    
    func filterRenderingVideoBuffer(_ captureOutput: AVCaptureOutput!, sampleBuffer: CMSampleBuffer!) {
        
        //print("called filterRenderingVideoBuffer")
        
        let pixelBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)!
        
        let copyPixelBuffer = pixelBuffer.copy()
        
        var outputImage = CIImage(cvImageBuffer: copyPixelBuffer)
        
        if self.currentFilter != nil {
            self.currentFilter?.setValue(outputImage, forKey: kCIInputImageKey)
            outputImage = (self.currentFilter?.outputImage!)!
        }
        
        _glkView?.bindDrawable()
        let imageSize = outputImage.extent.size
        var drawFrame = CGRect(x: 0, y:0, width:  _glkView.drawableWidth , height :_glkView.drawableHeight )
        let imageAR = imageSize.width / imageSize.height
        let viewAR = drawFrame.width / drawFrame.height
        
        glClearColor(0.0, 0.0, 0.0, 1.0);
        glClear(0x00004000)
        
        // set the blend mode to "source over" so that CI will use that
        glEnable(0x0BE2);
        glBlendFunc(1, 0x0303);
        //
        ciContext?.draw(outputImage, in: drawFrame, from: outputImage.extent)
        _glkView?.display()
    }
}



//MARK: UICOLLECTION VIEW SECTION
extension AVCamManualCameraViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, AVCaptureVideoDataOutputSampleBufferDelegate {
    
    //FILTER VIEW RENDERING OVERLAY
    
    func convert(cmage:CIImage) -> UIImage
    {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    
    private func imageFromSampleBuffer(sampleBuffer :CMSampleBuffer) -> UIImage {
        
        let imageBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)!
        let ciimage : CIImage = CIImage(cvPixelBuffer: imageBuffer)
        let image : UIImage = self.convert(cmage: ciimage)
        return image
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if isLongPress {
            let touchLocation = longPressGesturePhoto!.location(in: photoButton)
            
            if (!photoButton.bounds.contains(touchLocation)) {
                saveArraysOfPhoto()
            } else {
                
                self.pictureActivityIndicatorView.color = #colorLiteral(red: 0.8250325322, green: 0.8026339412, blue: 0.2692880332, alpha: 1)
                self.pictureActivityIndicatorView.type = .ballRotateChase
                self.pictureActivityIndicatorView.startAnimating()
                if counter % 3 == 0 {
                    do {
                        audioPlayer = try AVAudioPlayer(contentsOf: pianoSound)
                    } catch{}
                    deviceOrientaion = UIDevice.current.orientation
                    audioPlayer.stop()
                    audioPlayer.play()
                    
                   
                    burstShotimages.append(imageFromSampleBuffer(sampleBuffer: sampleBuffer))
                    
                    DispatchQueue.main.async {
                        self.burstShotView.isHidden = false
                        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.hideBurstShotCounter), userInfo: nil, repeats: false)
                        
                        self.picturecountAndAfAeLabel.text = "\(self.burstShotimages.count)"
                    }
                    
                    counter = 0
                }
                counter += 1
                return
            }
        } else {
        }
        if isTimeLapsCapturing {
            timelapsVideoBuffer(output, sampleBuffer: sampleBuffer)
            
        } else if isFilterPhoto {
            
            if connection.isVideoOrientationSupported {
                let curOrientation = UIApplication.shared.statusBarOrientation
                connection.videoOrientation = AVCaptureVideoOrientation(rawValue: curOrientation.rawValue)!
            }
            
            let isVideo = output is AVCaptureVideoDataOutput
            
            if !isVideo {
                return
            }
            
            self.filterRenderingVideoBuffer(output, sampleBuffer: sampleBuffer)
            
        }
        
    }
    
    func addFilterImageToArray() {
        // Loop for creating buttons ------------------------------------------------------------
        for i in 0..<CIFilterNames.count {
            // Create filters for each button
            let ciContext = CIContext(options: nil)
            let coreImage = CIImage(image: UIImage(named: "date")!)
            print("\(CIFilterNames[i])")
            let filter = CIFilter(name: "\(CIFilterNames[i])" )
            
            if (filter != nil) {
                filter!.setDefaults()
                filter!.setValue(coreImage, forKey: kCIInputImageKey)
                let filteredImageData = filter!.value(forKey: kCIOutputImageKey) as! CIImage
                let filteredImageRef = ciContext.createCGImage(filteredImageData, from: filteredImageData.extent)
                let filterImage = UIImage(cgImage: filteredImageRef!);
                filterdModelArray.append(CIFilterModel(image: filterImage, filter: filter, name : stringSpliting((filter!.name))))
            }else {
                let image = UIImage(named: "date")
                filterdModelArray.append(CIFilterModel(image: image, filter: nil, name : "None"))
            }
            
        } // END LOOP ----
        
        filterCollectionView.reloadData()
    }
    
    func stringSpliting(_ originalString : String) -> String {
        var newStringArray: [String] = []
        for character in originalString.characters {
            if String(character) == String(character).uppercased() {
                newStringArray.append(" ")
            }
            newStringArray.append(String(character))
        }
        
        let newString = newStringArray.joined().trimmingCharacters(in: .whitespacesAndNewlines).components(separatedBy: " ")
        
        return newString.last!
    }
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == filterCollectionView
        {
            return filterdModelArray.count
        }
        else if collectionView == photoQualityCollectionView {
            return sessionPhotoQualityArray.count
        }
        else { // videoquality collection view
            return sessionQualityArray.count
        }
        
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        if collectionView == videoQualityCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "videoqualitycell", for: indexPath as IndexPath)
            let sessionObj = sessionQualityArray[indexPath.row]
            let lbl = cell.viewWithTag(777) as? UILabel
            //  lbl?.tag = 777 + indexPath.row
            if sessionObj._pressetName == self.videoQuality {
                lbl?.textColor = UIColor.yellow
                lastSeletedLabelVideo = lbl
            }
            lbl?.text = sessionObj._pressetKey
            return cell
        }
        else if collectionView == photoQualityCollectionView {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoqualitycell", for: indexPath as IndexPath)
            let dicObject = sessionPhotoQualityArray[indexPath.row]
            let lbl = cell.viewWithTag(888) as? UILabel
            if dicObject._pressetName == self.photoQuality {
                lbl?.textColor = UIColor.yellow
                lastSeletedLabelPhoto = lbl
            }
            
            lbl?.text = dicObject._pressetKey
            return cell
            
        }
        else // filtercollectionview
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath)
            let cellImg = cell.viewWithTag(111) as? UIImageView
            let cellLbl = cell.viewWithTag(112) as? UILabel
            cellImg?.image = filterdModelArray[indexPath.row].image
            cellLbl?.text = filterdModelArray[indexPath.row].name
            return cell
        }
        
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("indexpath row no :: \(indexPath.row)")
        let currentcell = collectionView.cellForItem(at: indexPath)
        
        if collectionView == filterCollectionView {
            self.currentFilter = filterdModelArray[indexPath.row].filter
            if self.currentFilter == nil {
                self._glkView?.isHidden = true
                self.previewView.isHidden = false
                removeVideoDataOutputFromSession()
            } else {
                self._glkView?.isHidden = false
                self.filterString = filterdModelArray[indexPath.row].name!
                self.currentFilter = filterdModelArray[indexPath.row].filter
                self.filterCounter = indexPath.row
                configureFilterRendering()
                
            }
        }
            
        else if collectionView == videoQualityCollectionView {
            let sessionObj = sessionQualityArray[indexPath.row]
            
            if self.session.canSetSessionPreset(AVCaptureSession.Preset(rawValue: sessionObj._pressetName))
            {
                //AVCaptureSessionPreset3840x2160
                videoQuality = sessionObj._pressetName
                
                if lastSeletedLabelVideo != nil {
                    lastSeletedLabelVideo.textColor = UIColor.white
                }
                let label = (currentcell?.viewWithTag( 777) as? UILabel)
                
                
                label?.textColor = UIColor.yellow
                
                if captureModeTag == AVCamManualCaptureMode.movie.rawValue {
                    sessionPressetConfiguration()
                }
                
                
            }
            else{
                EZAlertController.alert("Info!", message: "This video quality is not supported by your phone")
            }
            
        }
        else // photo quality collection view
        {
            let dicObject = sessionPhotoQualityArray[indexPath.row]
            photoQuality = dicObject._pressetName
            if lastSeletedLabelPhoto != nil {
                lastSeletedLabelPhoto.textColor = UIColor.white
            }
            
            
            let label = (currentcell?.viewWithTag(888) as? UILabel)
            //   photoQualitySelected = photoQuality
            label?.textColor = UIColor.yellow
            
            
            if captureModeTag == AVCamManualCaptureMode.photo.rawValue {
                sessionPressetConfiguration()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let currentcell = collectionView.cellForItem(at: indexPath)
        
        (currentcell?.viewWithTag(777) as? UILabel)?.textColor = UIColor.white
        (currentcell?.viewWithTag(888) as? UILabel)?.textColor = UIColor.white
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == videoQualityCollectionView
        {
            
            let dicObject = sessionQualityArray[indexPath.row]
            let size: CGSize = dicObject._pressetKey.size(withAttributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14.0)])
            return CGSize(width: size.width + 10.0, height: 55)
            
            
        }
        else{
            
            let screen = UIScreen.main.bounds
            let width = ((screen.width - 45) / 3)
            return CGSize(width: width, height: 120)
        }
        
    }
}
