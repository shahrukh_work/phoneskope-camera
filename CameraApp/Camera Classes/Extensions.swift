//
//  Extensions.swift
//  CameraApp
//
//  Created by iMac on 7/6/18.
//  Copyright © 2018 softech. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD
extension UIView{
    func blink() {
        self.alpha = 0.0
        UIView.animate(withDuration: 0.8, delay: 0.0, options: [.curveLinear, .repeat, .autoreverse], animations: {self.alpha = 1.0}, completion: nil)
    }
    
    func blinkPreview() {
        self.alpha = 0.0
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveLinear], animations: {self.alpha = 1.0}, completion: nil)
    }

}


extension UIImage {
    
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
    public func imageRotatedByDegrees(degrees: CGFloat) -> UIImage {
        //Calculate the size of the rotated view's containing box for our drawing space
        let rotatedViewBox: UIView = UIView(frame: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        let t: CGAffineTransform = CGAffineTransform(rotationAngle: degrees * CGFloat.pi / 180)
        rotatedViewBox.transform = t
        let rotatedSize: CGSize = rotatedViewBox.frame.size
        //Create the bitmap context
        UIGraphicsBeginImageContext(rotatedSize)
        let bitmap: CGContext = UIGraphicsGetCurrentContext()!
        //Move the origin to the middle of the image so we will rotate and scale around the center.
        bitmap.translateBy(x: rotatedSize.width / 2, y: rotatedSize.height / 2)
        //Rotate the image context
        bitmap.rotate(by: (degrees * CGFloat.pi / 180))
        //Now, draw the rotated/scaled image into the context
        bitmap.scaleBy(x: 1.0, y: -1.0)
        bitmap.draw(self.cgImage!, in: CGRect(x: -self.size.width / 2, y: -self.size.height / 2, width: self.size.width, height: self.size.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
   

}
extension UIImage {
    
    func addText(textToDraw: NSString, atCorner: Int, textColor: UIColor?, textFont: UIFont?, quality : String, cameraPos : CameraPosition) -> UIImage {
        
        // Setup the font specific variables
        var _textColor: UIColor
        if textColor == nil {
            _textColor = UIColor.white
        } else {
            _textColor = textColor!
        }
        
        var _textFont: UIFont
        if textFont == nil {
            if Device.isSizeOrLarger(height: .Inches_4_7){
                _textFont = UIFont.systemFont(ofSize:   140)
            }else
            {
            if quality == "AVCaptureSessionPresetPhoto" {
               _textFont = UIFont.systemFont(ofSize:   140)
            }else if quality == "AVCaptureSessionPresetMedium" {
                 _textFont = UIFont.systemFont(ofSize: 20)
            }else {
                _textFont = UIFont.systemFont(ofSize: 10)
                
                }
            }
        
           
                if cameraPos == .front {
                    if(Device.isSizeOrSmaller(height:  .Inches_4)){
                _textFont = UIFont.systemFont(ofSize:   80)
                }
            }
           
        } else {
            _textFont = textFont!
        }
        
        // Setup the image context using the passed image
        UIGraphicsBeginImageContext(size)
        
        // Put the image into a rectangle as large as the original image
        draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        
        let titleParagraphStyle = NSMutableParagraphStyle()
        
        // Setup the font attributes that will be later used to dictate how the text should be drawn
        let textFontAttributes = [
            NSAttributedStringKey.font.rawValue: _textFont,
            NSAttributedStringKey.foregroundColor: _textColor,
            NSAttributedStringKey.paragraphStyle: titleParagraphStyle
            ] as [AnyHashable : NSObject]
        
        // get the bounding-box for the string
        var stringSize = textToDraw.size(withAttributes: textFontAttributes as? [NSAttributedStringKey : Any])
        
        // draw in rect functions like whole numbers
        stringSize.width = ceil(stringSize.width)
        stringSize.height = ceil(stringSize.height)
        
        var rect = CGRect(origin: CGPoint.zero, size: self.size)
        
        switch atCorner {
            
        case 1:
            // top-right
            titleParagraphStyle.alignment = .right
            
        case 2:
            // bottom-right
            rect.origin.y = self.size.height - stringSize.height
            titleParagraphStyle.alignment = .right
            
        case 3:
            // bottom-left
            rect.origin.y = self.size.height - stringSize.height
            
        default:
            // top-left
            // don't need to change anything here
            break
            
        }
        
        // Draw the text into an image
        textToDraw.draw(in: rect, withAttributes: textFontAttributes as? [NSAttributedStringKey : Any])
        
        // Create a new image out of the images we have created
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        // End the context now that we have the image we need
        UIGraphicsEndImageContext()
        
        //Pass the image back up to the caller
        return newImage!
    }
    
}

extension UIImage {
    func waterMarkingOfPhoto(_ imgData : Data,_ watermark : String) -> Data? {

        var returnData : Data!
        
        if let img = UIImage(data: imgData) {
            if let photoQuality = UserDefaults.standard.value(forKey: "photoQuality") as? String {
                
                var rightPadding : CGFloat = 25
                var bottomPadding : CGFloat = 120
                var fontSize : CGFloat = 175.0
                
                if photoQuality == "AVCaptureSessionPresetPhoto" {
                    fontSize = 175.0
                    rightPadding = 40
                    bottomPadding = -140
                }else if photoQuality == "AVCaptureSessionPresetMedium" {
                    fontSize = 24
                    rightPadding = 8
                    bottomPadding = -25
                }else {
                    fontSize = 9
                    rightPadding = 3
                    bottomPadding = -10
                }
                
                let elblWidth = img.size.width/2
                let elblHeight = img.size.height/5
                let eXPoz = img.size.width - (elblWidth + rightPadding)
                let eYPoz = img.size.height - (elblHeight + bottomPadding)
                
                let lblFrame = CGRect(x: eXPoz, y: eYPoz, width: elblWidth, height: elblHeight)
                let waterLbl = UILabel(frame: lblFrame)
                
                waterLbl.textAlignment = .right
                waterLbl.text = watermark
                waterLbl.textColor = UIColor.white
                waterLbl.adjustsFontSizeToFitWidth = true
                waterLbl.font = UIFont.systemFont(ofSize: fontSize)
                waterLbl.backgroundColor = UIColor.blue
                
                let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)

                UIGraphicsBeginImageContextWithOptions(img.size, true, 0)
                let context = UIGraphicsGetCurrentContext()
                
                context!.setFillColor(UIColor.white.cgColor)
                context!.fill(rect)
                img.draw(in: rect, blendMode: .normal, alpha: 1)
                
                waterLbl.drawText(in: CGRect(x: eXPoz, y: eYPoz, width: elblWidth, height: elblHeight))
                
                if let result = UIGraphicsGetImageFromCurrentImageContext() {
                    returnData = UIImageJPEGRepresentation(result, 1.0)
                }
                UIGraphicsEndImageContext()
            }
        }

        return returnData
    }
    
    func textToImage(drawText text: NSString, inImage image: UIImage, atPoint point: CGPoint) -> UIImage {
        let textColor = UIColor.white
        let textFont = UIFont(name: "Helvetica Bold", size: 12)!
        
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        
        let textFontAttributes = [
            NSAttributedStringKey.font.rawValue: textFont,
            NSAttributedStringKey.foregroundColor: textColor,
            ] as [AnyHashable : NSObject]
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        
        let rect = CGRect(origin: point, size: image.size)
        text.draw(in: rect, withAttributes: textFontAttributes as? [NSAttributedStringKey : Any])
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func flipImageLeftRight(_ image: UIImage) -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(image.size, false, image.scale)
        let context = UIGraphicsGetCurrentContext()!
        context.translateBy(x: image.size.width, y: image.size.height)
        context.scaleBy(x: -image.scale, y: -image.scale)
        
        context.draw(image.cgImage!, in: CGRect(origin:CGPoint.zero, size: image.size))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func fixedOrientation() -> UIImage {
        
        if imageOrientation == .up {
            return self
        }
        
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        switch imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: size.width, y: size.height)
            transform = transform.rotated(by: CGFloat(M_PI))
            break
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: size.width, y: 0)
            transform = transform.rotated(by: CGFloat(M_PI_2))
            break
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: size.height)
            transform = transform.rotated(by: CGFloat(-M_PI_2))
            break
        case .up, .upMirrored:
            break
        }
        
        switch imageOrientation {
        case .upMirrored, .downMirrored:
            transform.translatedBy(x: size.width, y: 0)
            transform.scaledBy(x: -1, y: 1)
            break
        case .leftMirrored, .rightMirrored:
            transform.translatedBy(x: size.height, y: 0)
            transform.scaledBy(x: -1, y: 1)
        case .up, .down, .left, .right:
            break
        }
        
        let ctx: CGContext = CGContext(data: nil, width: Int(size.width), height: Int(size.height), bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0, space: (self.cgImage?.colorSpace)!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!
        
        ctx.concatenate(transform)
        
        switch imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.height, height: size.width))
            break
        default:
            ctx.draw(self.cgImage!, in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            break
        }
        
        let cgImage: CGImage = ctx.makeImage()!
        
        return UIImage(cgImage: cgImage)
    }
}
extension CVPixelBuffer {
    
    func copy() -> CVPixelBuffer {
        precondition(CFGetTypeID(self) == CVPixelBufferGetTypeID(), "copy() cannot be called on a non-CVPixelBuffer")
        
        var _copy : CVPixelBuffer?
        CVPixelBufferCreate(
            kCFAllocatorDefault,
            CVPixelBufferGetWidth(self),
            CVPixelBufferGetHeight(self),
            CVPixelBufferGetPixelFormatType(self),
            nil,
            &_copy)
        
        guard let copy = _copy else { fatalError() }
        
        CVPixelBufferLockBaseAddress(self, CVPixelBufferLockFlags.readOnly)
        CVPixelBufferLockBaseAddress(copy, CVPixelBufferLockFlags(rawValue: 0))
        
        
        let copyBaseAddress = CVPixelBufferGetBaseAddress(copy)
        let currBaseAddress = CVPixelBufferGetBaseAddress(self)
        
        memcpy(copyBaseAddress, currBaseAddress, CVPixelBufferGetDataSize(self))
        
        CVPixelBufferUnlockBaseAddress(copy, CVPixelBufferLockFlags(rawValue: 0))
        CVPixelBufferUnlockBaseAddress(self, CVPixelBufferLockFlags.readOnly)
        
        
        return copy
    }
}

