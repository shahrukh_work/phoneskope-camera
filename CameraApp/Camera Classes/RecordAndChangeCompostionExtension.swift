//
//  RecordAndChangeCompostionExtension.swift
//  CameraApp
//
//  Created by iMac on 06/09/2018.
//  Copyright © 2018 softech. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Photos
import AssetsLibrary
import CoreLocation
import MediaPlayer ////Only for hidding  Volume view
import GLKit
import CoreImage
import SVProgressHUD
import LLVideoEditor
import NVActivityIndicatorView
import MediaWatermark


extension AVURLAsset {
    var fileSize: Int? {
        let keys: Set<URLResourceKey> = [.totalFileSizeKey, .fileSizeKey]
        let resourceValues = try? url.resourceValues(forKeys: keys)
        
        return resourceValues?.fileSize ?? resourceValues?.totalFileSize
    }
}


extension AVCamManualCameraViewController {
    //compostionfunction
    func changeComposition(_ outputFileURL : URL){
        // SVProgressHUD.show(withStatus:"Saving video for you may take a while.")
        
        if self.isVideoRecording {
            return
        }
        self.isVideoRecording = false
        let currentAsset = AVAsset( url: outputFileURL)
        let composition = AVMutableComposition.init()
        
        let videoComposition = AVMutableVideoComposition()
        videoComposition.frameDuration = CMTimeMake(1, 30)
        videoComposition.renderScale  = 1.0
        
        let compositionCommentaryTrack: AVMutableCompositionTrack? = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)
        let compositionVideoTrack: AVMutableCompositionTrack? = composition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        var videoScaleFactor: Double = 5.0
        var videoDuration: CMTime = currentAsset.duration
        let clipVideoTrack:AVAssetTrack =  currentAsset.tracks(withMediaType: AVMediaType.video)[0]
        try? compositionVideoTrack?.insertTimeRange(CMTimeRangeMake(kCMTimeZero, currentAsset.duration ), of: clipVideoTrack, at: kCMTimeZero)
        
//        if !isTimeLapsReady {
//            let audioTrack: AVAssetTrack = currentAsset.tracks(withMediaType: AVMediaTypeAudio)[0]
//            try? compositionCommentaryTrack?.insertTimeRange(CMTimeRangeMake(kCMTimeZero, currentAsset.duration), of: audioTrack, at: kCMTimeZero)
//            
//        }
//   
//        if dataHandler.getSaveVideoPath()[0].isSlowMotion {
//            compositionCommentaryTrack?.scaleTimeRange(CMTimeRangeMake(kCMTimeZero, videoDuration), toDuration: CMTimeMake(Int64(Double(videoDuration.value) * videoScaleFactor), videoDuration.timescale))
//            
//            compositionVideoTrack?.scaleTimeRange(CMTimeRangeMake(kCMTimeZero, videoDuration), toDuration: CMTimeMake(Int64(Double(videoDuration.value) * videoScaleFactor), videoDuration.timescale))
//        }else{
//            
//        }
//        
        var isPortrait = false

        var naturalSize = clipVideoTrack.naturalSize
        
        if !isPortrait
        {
            naturalSize = CGSize.init(width: naturalSize.height, height: naturalSize.width)
        }
        
        videoComposition.renderSize = naturalSize
        let x =  naturalSize.width/2
        let y =  naturalSize.height/2
        
        let scale = CGFloat(1.0)
        
        var transform = CGAffineTransform.init(scaleX: CGFloat(-scale), y: CGFloat(scale))
        print(slideVal)
        
        if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft {
            transform =  transform.translatedBy(x: -x, y: y)
            transform =  transform.rotated(by: CGFloat(Double(rotationSlider.value) *  ((2.0*Double.pi / Double(rotationSlider.maximumValue)))))
            transform = transform.translatedBy(x: -y, y:-x )
        } else if UIDevice.current.orientation == UIDeviceOrientation.landscapeRight {
            transform =  transform.translatedBy(x: -x, y: y)
            transform =  transform.rotated(by: CGFloat(Double(rotationSlider.value) *  ((2.0*Double.pi / Double(rotationSlider.maximumValue))) + 3.1415))
            transform = transform.translatedBy(x: -y, y:-x )
        } else{
            transform =  transform.translatedBy(x: -x, y: y)
            transform =  transform.rotated(by: CGFloat(Double(rotationSlider.value) *  ((2.0*Double.pi / Double(rotationSlider.maximumValue)))+1.57))
            transform = transform.translatedBy(x: -y, y:-x )
        }
        
        
    
        let frontLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: compositionVideoTrack!)
            frontLayerInstruction.setTransform(transform, at: kCMTimeZero)
        
        let MainInstruction = AVMutableVideoCompositionInstruction()
        print(composition.duration)
        MainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, composition.duration)
        MainInstruction.layerInstructions = [frontLayerInstruction]
        videoComposition.instructions = [MainInstruction]
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let videoPath = documentsPath+"/cropEditVideo.mov"
        let fileManager = FileManager.default
        
        if fileManager.fileExists(atPath: videoPath)
        {
            try! fileManager.removeItem(atPath: videoPath)
        }
        
        self.stopVideoRecordTimer()
        print("video path \(videoPath)")
        var exportSession = AVAssetExportSession.init(asset: composition, presetName: AVAssetExportPresetHighestQuality)
        
        exportSession?.videoComposition = videoComposition
        exportSession?.outputFileType = AVFileType.mov
        exportSession?.outputURL = URL.init(fileURLWithPath: videoPath)
        exportSession?.videoComposition = videoComposition
        var exportProgress: Float = 0
        let queue = DispatchQueue(label: "Export Progress Queue")
        queue.async(execute: {() -> Void in
            while exportSession != nil {
                //                int prevProgress = exportProgress;
                exportProgress = (exportSession?.progress)!
                print("current progress == \(exportProgress)")
                
                
                DispatchQueue.main.async{
                    var estimatedTime = videoDuration.value/1750
                    var timeInMinutes = 0
                    var timeInHours = 0
                    if estimatedTime >= 60 {
                        timeInMinutes = Int(estimatedTime/60)
                        estimatedTime = Int64(Int(estimatedTime) - Int(timeInMinutes)*60)
                    }
                    if estimatedTime >= 3600{
                        timeInHours = Int(estimatedTime/3600)
                        timeInMinutes = Int(estimatedTime) - Int(timeInHours)*60
                        estimatedTime = Int64(Int(estimatedTime) - Int(timeInMinutes)*60*60)
                    }
                    
                SVProgressHUD.show(withStatus:"\(Int(exportProgress * 100))% \n Estimated time for saving the video is \(timeInHours)hrs:\(timeInMinutes)mins:\(estimatedTime)secs \n ")
                }
                sleep(1)
            }
        })
        exportSession?.exportAsynchronously(completionHandler: {
            
            if exportSession?.status == AVAssetExportSessionStatus.failed
            {
                print("Failed \(exportSession?.error)")
            }else if exportSession?.status == AVAssetExportSessionStatus.completed
            {
                if self.selectedScope{
                    self.sliderValueChanged(sender: self.rotationSlider)
                }
                
                exportSession = nil
                DispatchQueue.main.async {
                    self.isVideoRecording = false
                    if Product.purchased{
                        SVProgressHUD.dismiss() // this will call when purchase
                        self.saveVideoToPhotoLibrary(URL.init(fileURLWithPath: videoPath), error: nil)
                    }else{
                        self.addWaterMark(url: URL.init(fileURLWithPath: videoPath))
                    }
                }
            }
            
        })
    }
    
    func addWaterMark (url : URL){
        if let item = MediaItem(url: url) {
            stopVideoRecordTimer()
            //   SVProgressHUD.show()
            let logoImage = UIImage(named: "water_mark")
            print(videoQuality)
            let firstElement = MediaElement(image: logoImage!)
            if Device.IS_4_7_INCHES_OR_LARGER(){
                firstElement.frame = CGRect(x: 0, y: 0, width: 300, height: 230)
                if videoQuality == AVCaptureSession.Preset.hd4K3840x2160.rawValue{
                    firstElement.frame = CGRect(x: 0, y: 0, width: 400, height: 300)
                }else if videoQuality == AVCaptureSession.Preset.high.rawValue{
                    if !isSlowMotion{
                        firstElement.frame = CGRect(x: 0, y: 0, width: 250, height: 200)
                    }else{
                        firstElement.frame = CGRect(x: 0, y: 0, width: 200, height: 150)
                    }
                    
                }else if videoQuality == AVCaptureSession.Preset.iFrame1280x720.rawValue{
                    firstElement.frame = CGRect(x: 0, y: 0, width: 180, height: 130)
                }else if videoQuality == AVCaptureSession.Preset.iFrame960x540.rawValue{
                    firstElement.frame = CGRect(x: 0, y: 0, width: 160, height: 120)
                }else if videoQuality == AVCaptureSession.Preset.vga640x480.rawValue{
                    firstElement.frame = CGRect(x: 0, y: 0, width: 130, height: 90)
                }else if videoQuality == AVCaptureSession.Preset.medium.rawValue{
                    firstElement.frame = CGRect(x: 0, y: 0, width: 80, height: 50)
                }else if videoQuality == AVCaptureSession.Preset.low.rawValue{
                    firstElement.frame = CGRect(x: 0, y: 0, width: 30, height: 20)
                }
            }else{
                firstElement.frame =  CGRect(x: 0, y: 0, width: 300, height: 270)
                if videoQuality == AVCaptureSession.Preset.hd4K3840x2160.rawValue{
                    firstElement.frame = CGRect(x: 0, y: 0, width: 400, height: 300)
                }else if videoQuality == AVCaptureSession.Preset.high.rawValue{
                    firstElement.frame = CGRect(x: 0, y: 0, width: 250, height: 200)
                }else if videoQuality == AVCaptureSession.Preset.iFrame1280x720.rawValue{
                    firstElement.frame = CGRect(x: 0, y: 0, width: 180, height: 130)
                }else if videoQuality == AVCaptureSession.Preset.iFrame960x540.rawValue{
                    firstElement.frame = CGRect(x: 0, y: 0, width: 160, height: 120)
                }else if videoQuality == AVCaptureSession.Preset.vga640x480.rawValue{
                    firstElement.frame = CGRect(x: 0, y: 0, width: 130, height: 90)
                }else if videoQuality == AVCaptureSession.Preset.medium.rawValue{
                    firstElement.frame = CGRect(x: 0, y: 0, width: 80, height: 50)
                }else if videoQuality == AVCaptureSession.Preset.low.rawValue{
                    firstElement.frame = CGRect(x: 0, y: 0, width: 30, height: 20)
                }
            }
            
            if self.videoDevice?.position == .front{
                if Device.IS_4_7_INCHES_OR_LARGER(){
                    firstElement.frame = CGRect(x: 0, y: 0, width: 180, height: 130)
                }else{
                    firstElement.frame =  CGRect(x: 0, y: 0, width: 180, height: 130)
                }
            }
            
            item.add(elements: [firstElement])
            
            let mediaProcessor = MediaProcessor()
            mediaProcessor.processElements(item: item) { [weak self] (result, error) in
                self?.saveVideoToPhotoLibrary( result.processedUrl!, error: nil)
                //   self?.videoPlayer.playFromBeginning()
            }
        }
        
    }
    
    func fileOutput(_ output: AVCaptureFileOutput, didStartRecordingTo fileURL: URL, from connections: [AVCaptureConnection]){
        // Enable the Record button to let the user stop the recording.
        
        DispatchQueue.main.async { [weak self] in
            self?.startVideoRecordTimer()
            self?.purchaseButtonOutlet.isHidden = true
            self?.screenBrightnessOutlet.isHidden = true
            self?.thumbnailBtn.isHidden = true
            self?.recordButton.isEnabled = true
            // hideviews
            self?.blinkViewRecord.isHidden = false
            self?.blinkRedDot.blink()
            self?.topContainerView.isHidden = true
            self?.menuButton.isHidden = true
            self?.mirrorScrollView.isHidden = true
            
            self?.rotationSlider.isHidden = true
            self?.isoSlider.isHidden = true
            self?.focusSlider.isHidden = true
            self?.focusImageView.isHidden = true
            self?.rotationImageView.isHidden = true
            
            self?.brightImageView.isHidden = true
            self?.upButtonOutlet.isHidden = true
            self?.downButtonOulet.isHidden = true
            
            /////
            self?.hideViews(view: (self?.bottomContainerView)!)
            self?.photoButton2.isHidden = false
            self?.photoModeButton.isHidden = true
            self?.recordButton.buttonState = .recording
            if isSlowMotion || isTimeLapsReady {
                self?.photoButton2.isHidden = true
            }else{
                self?.photoButton2.isHidden = false
            }
            
        }
    }
    func createVideoLayer() -> CALayer {
        // a simple red rectangle
        let layer = CALayer()
        layer.backgroundColor = UIColor.red.cgColor
        layer.frame = CGRect(x: 10, y: 10, width: 100, height: 50)
        return layer
    }
    
    
   func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
        url1 = outputFileURL
        self.screenBrightnessOutlet.isHidden = false
        self.blinkViewRecord.isHidden = true
        self.topContainerView.isHidden = false
        self.menuButton.isHidden = false
        self.isVideoRecording = false
        self.recordButton.buttonState = .normal
        self.photoButton2.isHidden = true
        thumbnailBtn.isHidden = false
        if !Product.purchased{
            self.purchaseButtonOutlet.isHidden = false
            self.purchaseButtonOutlet.alpha = 1.0
        }

        if selectedScope == true {
            self.rotationSlider.isHidden = false
            self.isoSlider.isHidden = false
            self.focusSlider.isHidden = false
            self.focusImageView.isHidden = false
            self.rotationImageView.isHidden = false
            self.brightImageView.isHidden = false
            self.rotationSlider.tintColor = UIColor.yellow
            self.upButtonOutlet.isHidden = false
            self.downButtonOulet.isHidden = false
            self.photoModeButton.isHidden = false
            stopVideoRecordTimer()
            switch AVAudioSession.sharedInstance().recordPermission() {
            case AVAudioSessionRecordPermission.granted:
                
                let currentAsset = AVURLAsset( url: outputFileURL)
                
                let videoData =  NSData(contentsOf: outputFileURL)
                let name = Int(NSDate().timeIntervalSince1970)
                let documentDir = getDirectoryPath()
                let filename = "/\(name).mp4"
                
                let dataPath = documentDir.appending(filename)
                videoData?.write(toFile: dataPath, atomically: false)

                getThumbnail(outputFileURL:outputFileURL , success: { (image) in
                    let thumbnaiImageData = UIImagePNGRepresentation(image)! as NSData
                    self.hmsFrom(seconds: Int(CMTimeGetSeconds(currentAsset.duration))) { [weak self] (h, m, s) in
                        let hours = self?.getStringFrom(seconds: h)
                        let minutes = self?.getStringFrom(seconds: m)
                        let seconds = self?.getStringFrom(seconds: s)

                        if hours != nil && minutes != nil && seconds != nil {
                            print("\(hours!):\(minutes!):\(seconds!)")
                            let time  = "\(hours!):\(minutes!):\(seconds!)"
                            
                            self?.dataHandler.saveVideoPath(path: filename, duration: time, size: "\(currentAsset.fileSize ?? 0)", rotationAngle: (self?.angleValue)!, thumbnail: thumbnaiImageData as NSData, isSlowMotion: isSlowMotion)
                            self?.activityIndicationView.stopAnimating()
                            self?.startOrStopAnimatingIndicator()
                            if CheckVideos.shared.isProcessing{
                               // CheckVideos.shared.getVideo()
                            }else{
                                CheckVideos.shared.getVideo()
                            }
                            
                        }
                    }
                })
//
            case AVAudioSessionRecordPermission.denied:
                self.messageAccessNotGiven(title : "Microphone Permission required", message : "If you want to save rifle scope video you need to grant microphone permission.")
                
            case AVAudioSessionRecordPermission.undetermined:
                self.messageAccessNotGiven(title : "Microphone Permission required", message : "If you want to save rifle scope video you need to grant microphone permission.")
                
            default:
                break
            }
            
        }else{
            if Product.purchased {
                self.saveVideoToPhotoLibrary(outputFileURL, error: nil)
            }else{
                SVProgressHUD.show(withStatus: "Saving video for you.")
                self.addWaterMark(url: outputFileURL)
            }
        }
    }
    
    func didFinishProcessingVideo() {
       let rifleVideos = CoreDataVideos.shared.rifleVideos
        queueVideosListBtnOutlet.setTitle("\(rifleVideos.count)", for: .normal)
        if rifleVideos.count == 0 {
            activityIndicationView.stopAnimating()
            queueVideosListBtnOutlet.setTitle("", for: .normal)
        }
    }
    
    func startOrStopAnimatingIndicator() {
       
        let rifleVideos = CoreDataVideos.shared.rifleVideos
        if rifleVideos.count != 0 {
            if selectedScope {
                self.activityIndicationView.color = #colorLiteral(red: 0.8250325322, green: 0.8026339412, blue: 0.2692880332, alpha: 1)
                self.activityIndicationView.type = .ballRotateChase
                self.queueVideosListBtnOutlet.setTitle("\(rifleVideos.count)", for: .normal)
                self.queueVideosListBtnOutlet.isHidden = false
                self.activityIndicationView.startAnimating()
            }
        }else{
            self.queueVideosListBtnOutlet.setTitle("", for: .normal)
            self.queueVideosListBtnOutlet.isHidden = true
            self.activityIndicationView.stopAnimating()
        }
        SVProgressHUD.dismiss()
    }
    
    func sendPerentageToViewController(_ percent: String) {
        
    }
    
    func getThumbnail(outputFileURL: URL, success:@escaping (UIImage) -> Void){
            let asset = AVAsset(url: outputFileURL)
            let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
            assetImgGenerate.appliesPreferredTrackTransform = true
            let time = CMTimeMake(1, 2)
            var image: UIImage?
            let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            if img != nil {
                let frameImg  = UIImage(cgImage: img!)
                DispatchQueue.main.async(execute: {
                    success(frameImg)
                    // assign your image to UIImageView
                })
            }
    }
    
    func hmsFrom(seconds: Int, completion: @escaping (_ hours: Int, _ minutes: Int, _ seconds: Int)->()) {
        completion(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
        
    }

    func getDirectoryPath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func getImage(){
        let name = Int(NSDate().timeIntervalSince1970)
        let documentDir = getDirectoryPath()
        let fileManager = FileManager.default
        
        let imagePAth = (self.getDirectoryPath() as NSString).appending("/\(name).mp4")
        
        if fileManager.fileExists(atPath: imagePAth){
            self.changeComposition( URL(fileURLWithPath: imagePAth))
        }else{
            
        }
    }
    
    func saveVideoToPhotoLibrary(_ outputFileURL : URL, error : Error?) {
        
        
        let currentBackgroundRecordingID = self.backgroundRecordingID
        self.backgroundRecordingID = UIBackgroundTaskInvalid
        
        let cleanup: ()->() = {
            if FileManager.default.fileExists(atPath: outputFileURL.path) {
                do {
                    try FileManager.default.removeItem(at: outputFileURL)
                } catch _ {}
            }
            
            if currentBackgroundRecordingID != UIBackgroundTaskInvalid {
                UIApplication.shared.endBackgroundTask(currentBackgroundRecordingID)
            }
        }
        
        var success = true
        
        if error != nil {
            NSLog("Error occurred while capturing movie: \(error!)")
            success = (error! as NSError).userInfo[AVErrorRecordingSuccessfullyFinishedKey] as? Bool ?? false
        }
        if success {
            SVProgressHUD.dismiss()
            // Check authorization status.
            PHPhotoLibrary.requestAuthorization {status in
                guard status == .authorized else {
                    cleanup()
                    self.messageAccessNotGiven(title : "Photo Library Permission required", message : "If you want to save video you need to grant Photo Library Permission.")
                    return
                }
                // Save the movie file to the photo library and cleanup.
                PHPhotoLibrary.shared().performChanges({
                    // In iOS 9 and later, it's possible to move the file into the photo library without duplicating the file data.
                    // This avoids using double the disk space during save, which can make a difference on devices with limited free disk space.
                    if #available(iOS 9.0, *) {
                        let options = PHAssetResourceCreationOptions()
                        options.shouldMoveFile = true
                        let changeRequest = PHAssetCreationRequest.forAsset()
                        changeRequest.addResource(with: .video, fileURL: outputFileURL, options: options)
                    } else {
                        //### Error occurred while capturing movie
                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: outputFileURL)
                    }
                }, completionHandler: {success, error in
                    if !success {
                        NSLog("Could not save movie to photo library: \(error!)")
                    }else {
                        DispatchQueue.main.async {[weak self] in
                            SVProgressHUD.dismiss()
                            let value =  "\(outputFileURL)".components(separatedBy: "/")
                            print(value)
                            //  self?.dataHandler.saveRotateInfo(name: value.last!, rotate: (self?.rotationSlider.value)!)
                            self?.loadLatestPHAsset()
                            self?.isoSlider.minimumValue = self?.videoDevice?.activeFormat.minISO ?? 0.0
                            self?.isoSlider.maximumValue = self?.videoDevice?.activeFormat.maxISO ?? 0.0
                        }
                    }
                    cleanup()
                })
            }
        } else {
            cleanup()
        }
        
        // Enable the Camera and Record buttons to let the user switch camera and start another recording.
        DispatchQueue.main.async {[weak self] in
            //            // Only enable the ability to change camera if the device has more than one camera.
            //
            
            self?.cameraIconFlag = true
            self?.photoModeButton.isHidden = false
            
            if (self?.selectedScope)! {
                self?.previewView.transform = CGAffineTransform(scaleX: -1, y: 1)
            }else{
                self?.previewView.transform = CGAffineTransform(scaleX: 1, y: 1)
                
            }
            
            self?.stopVideoRecordTimer()
            if (self?.selectedScope)! {
                self?.switchCameraPhotoTop.isHidden = true
                
            }
            if (self?.selectedScope)! {
                self?.sliderValueChanged(sender: (self?.rotationSlider)!)
            }
            
            
            
        }
    }
}
