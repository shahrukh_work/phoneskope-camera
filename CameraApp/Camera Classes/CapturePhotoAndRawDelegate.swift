//
//  CapturePhotoAndRawDelegate.swift
//  CameraApp
//
//  Created by iMac on 06/09/2018.
//  Copyright © 2018 softech. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Photos
import AssetsLibrary
import CoreLocation
import MediaPlayer ////Only for hidding  Volume view
import GLKit
import CoreImage
import SVProgressHUD
import LLVideoEditor
import NVActivityIndicatorView
import MediaWatermark


typealias SavePhotoToLibraryCompletionHandler = () -> Void
typealias FixingImageCompletionHandler = (_ imageData: Data) -> Void


extension AVCamManualCameraViewController{
    
    //MARK: - CapturePhoto
    
    @available(iOS 10.0, *)
    func capturePhoto() {
        
        previewView.blinkPreview()
        self.bottomContainerView.bringSubview(toFront:pictureActivityIndicatorView )
       
        //  SVProgressHUD.show(withStatus: "Saving Picture for you.")
        let lensStabilizationEnabled = false//self.lensStabilizationControl.selectedSegmentIndex == 1
        var rawEnabled = false
        if IS_RAW_FORMATE == "Jpg"{
            rawEnabled = false
        }else {
            rawEnabled = true
        } //self.rawControl.selectedSegmentIndex == 1
        
        if rawEnabled{
            if self.videoDevice?.position == .front
            {let alert = UIAlertController(title: "", message: "Your front camera does not support raw photo format. Please change the save photo format.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                SVProgressHUD.dismiss()
                return
            }
        }
        
        //        self.sessionQueue.async {
        //            self.configureSession()
        //        }
        var photoSettings = AVCapturePhotoSettings()
        let bracketedSettings: [AVCaptureBracketedStillImageSettings]
        if self.videoDevice?.exposureMode == .custom {
            bracketedSettings = [AVCaptureManualExposureBracketedStillImageSettings.manualExposureSettings(exposureDuration: AVCaptureDevice.currentExposureDuration, iso: AVCaptureDevice.currentISO)]
        } else {
            bracketedSettings = [AVCaptureAutoExposureBracketedStillImageSettings.autoExposureSettings(exposureTargetBias: AVCaptureDevice.currentExposureTargetBias)]
        }
        
        if lensStabilizationEnabled && (photoOutput?.isLensStabilizationDuringBracketedCaptureSupported)! {
            
            if rawEnabled && !(photoOutput?.__availableRawPhotoPixelFormatTypes.isEmpty)! {
                photoSettings = AVCapturePhotoBracketSettings(rawPixelFormatType: (photoOutput?.__availableRawPhotoPixelFormatTypes[0].uint32Value)!, processedFormat: nil, bracketedSettings: bracketedSettings)
            } else {
                photoSettings = AVCapturePhotoBracketSettings(rawPixelFormatType: 0, processedFormat: [AVVideoCodecKey: AVVideoCodecJPEG], bracketedSettings: bracketedSettings)
            }
            
            (photoSettings as! AVCapturePhotoBracketSettings).isLensStabilizationEnabled = true
            
        } else {
            let photoPixelFormatType = kCVPixelFormatType_24BGR
            if rawEnabled && !(photoOutput?.__availableRawPhotoPixelFormatTypes.isEmpty)! {
                if self.currentCameraPosition == .front {
                    photoSettings = AVCapturePhotoSettings()
                }else{
                    
                    photoSettings = AVCapturePhotoBracketSettings(rawPixelFormatType: (photoOutput?.availableRawPhotoPixelFormatTypes[0])!, processedFormat: [AVVideoCodecKey: AVVideoCodecJPEG], bracketedSettings: bracketedSettings)
                }
                
            } else {
                photoSettings = AVCapturePhotoSettings()
            }
            
            // We choose not to use flash when doing manual exposure
            if self.videoDevice?.exposureMode == .custom {
                photoSettings.flashMode = .off
            } else {
                if self.currentCameraPosition == .front {
                    if (photoOutput?.__supportedFlashModes.contains(AVCaptureDevice.FlashMode.auto.rawValue as NSNumber))! {
                        photoSettings.flashMode = flushMode
                    }
                    else
                    {
                        photoSettings.flashMode = .off
                    }
                    //photoSettings?.flashMode = photoOutput.supportedFlashModes.contains(AVCaptureFlashMode.auto.rawValue as NSNumber) ? .auto : .off
                }else if self.currentCameraPosition == .rear {
                    if rawEnabled{
                        
                    }else
                    {
                        photoSettings.flashMode = flushMode
                    }
                }
            }
        }
        
        if !(photoSettings.availablePreviewPhotoPixelFormatTypes.isEmpty ?? true) {
            photoSettings.previewPhotoFormat = [kCVPixelBufferPixelFormatTypeKey as String: photoSettings.availablePreviewPhotoPixelFormatTypes[0]] // The first format in the array is the preferred format
        }
        
        if IS_RAW_FORMATE == "Raw"  || IS_RAW_FORMATE == "RawJpg"{
            if self.videoDevice?.exposureMode == .custom {
                //photoSettings.isAutoStillImageStabilizationEnabled = true
            }
        }
        
        if Device.isSizeOrLarger(height: .Inches_4_7){
            
        }else{
            if IS_RAW_FORMATE == "Raw"{
                let alert = UIAlertController(title: "", message: "Your phone does not support raw photo format. Please change the save photo format.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
        }
        
        photoSettings.isHighResolutionPhotoEnabled = true
        
        
        
        
        let previewLayer = self.previewView.layer as! AVCaptureVideoPreviewLayer
        let videoPreviewLayerVideoOrientation = previewLayer.connection!.videoOrientation
        if IS_RAW_FORMATE == "Raw"  || IS_RAW_FORMATE == "RawJpg"{
            if photoQuality != "AVCaptureSessionPresetPhoto"{
                SVProgressHUD.dismiss()
                let alert = UIAlertController(title: "", message: "You can not change the quality of raw photo! Please select Jpg Format.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
            if zoomScale > 1.0{
                let alert = UIAlertController(title: "", message: "You can not use zoom photo feature on RAW photo format. Please select JPEG format.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
        }else{
        }
        
        
        self.photoOutput?.capturePhoto(with: photoSettings, delegate: self)
        
        
        
    }
}

//MARK: - CapturePhotoDelegate

extension AVCamManualCameraViewController :AVCapturePhotoCaptureDelegate {
    
    //MARK: - rawdidfinishProcessing
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingRawPhoto rawSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        var imageData = AVCapturePhotoOutput.dngPhotoDataRepresentation(forRawSampleBuffer: rawSampleBuffer!, previewPhotoSampleBuffer: previewPhotoSampleBuffer)
        
        if Product.purchased {
            SVProgressHUD.dismiss()
            
        } else {
            SVProgressHUD.dismiss()
        }
        
        if let watermark = UserDefaults.standard.value(forKey: UserDefaultKey.watermark.rawValue) as? String {
            //imageData1 = image.waterMarkingOfPhoto(imageData1!,watermark)
            let alert = UIAlertController(title: "", message: "You can not apply water mark to raw photo. Please remove watermark from settings", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        if currentFilter != nil{
            let alert = UIAlertController(title: "", message: "You can not apply filter to raw photo. Please remove filter from settings", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        PHPhotoLibrary.requestAuthorization {status in
            if status == PHAuthorizationStatus.authorized {
                if #available(iOS 9.0, *) {
                    PHPhotoLibrary.shared().performChanges({
                        let creationRequest = PHAssetCreationRequest.forAsset()
                        
                        if AppDelegate.IS_GEO_TAG_ON {
                            if let validDic = UserDefaults.standard.value(forKey: UserDefaultKey.location.rawValue) as? NSDictionary {
                                
                                if let lat = validDic["lat"] as? Double {
                                    if let lng = validDic["long"] as? Double {
                                        let validLoc = CLLocation(latitude: lat, longitude: lng)
                                        creationRequest.location = validLoc
                                    }
                                }
                            }
                        }
                        else {
                            creationRequest.location = nil
                        }
                        creationRequest.addResource(with: PHAssetResourceType.photo, data: imageData!, options: nil)
                    }, completionHandler: {success, error in
                        if !success {
                            NSLog("Error occured while saving image to photo library: \(error!)")
                            SVProgressHUD.dismiss()
                        }else{
                            SVProgressHUD.dismiss()
                            DispatchQueue.main.async {[weak self] in
                                self?.loadLatestPHAsset()
                            }
                        }
                    })
                } else {
                    let temporaryFileName = ProcessInfo().globallyUniqueString as NSString
                    let temporaryFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(temporaryFileName.appendingPathExtension("jpg")!)
                    let temporaryFileURL = URL(fileURLWithPath: temporaryFilePath)
                    
                    PHPhotoLibrary.shared().performChanges({
                        do {
                            try imageData?.write(to: temporaryFileURL, options: .atomicWrite)
                            PHAssetChangeRequest.creationRequestForAssetFromImage(atFileURL: temporaryFileURL)
                            SVProgressHUD.dismiss()
                        } catch let error {
                            NSLog("Error occured while writing image data to a temporary file: \(error)")
                            SVProgressHUD.dismiss()
                        }
                    }, completionHandler: {success, error in
                        if !success {
                            NSLog("Error occurred while saving image to photo library: \(error!)")
                            SVProgressHUD.dismiss()
                        }
                        
                        // Delete the temporary file.
                        do {
                            try FileManager.default.removeItem(at: temporaryFileURL)
                        } catch _ {}
                    })
                }
            }else{
                SVProgressHUD.dismiss()
                self.messageAccessNotGiven(title: "Photo Library Permission Required", message: "If you want to save photo you need to grant permission to Photo Library.")
            }
        }
        
        
        
        
    }
    
    //MARK: - didfinishProcessing
    
    func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?){
        
        if videoModeButton.isHidden == true{
            // return
        }
        if IS_RAW_FORMATE == "Jpg"  || IS_RAW_FORMATE == "RawJpg"{
            
        }else{
            return
        }
        
        if error != nil {
            NSLog("Error capture still image \(error!)")
        } else if photoSampleBuffer != nil {
            var imageData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: photoSampleBuffer!, previewPhotoSampleBuffer: previewPhotoSampleBuffer)
            
            fixingImage(imageData1: imageData!, { (data) in
                self.savePicturesToLibrary(imageData: data) {
                    print("image saved")
                }
            })
        }
       
    }
    
    //MARK: - ----- SAVE IMAGES ------
    
    func savePicturesToLibrary (imageData: Data, _ completionBlock: @escaping SavePhotoToLibraryCompletionHandler) {
        PHPhotoLibrary.requestAuthorization {status in
            
            if status == PHAuthorizationStatus.authorized {
                
                if #available(iOS 9.0, *) {
                    
                    PHPhotoLibrary.shared().performChanges({
                        let creationRequest = PHAssetCreationRequest.forAsset()
                        
                        if AppDelegate.IS_GEO_TAG_ON {
                            
                            if let validDic = UserDefaults.standard.value(forKey: UserDefaultKey.location.rawValue) as? NSDictionary {
                                
                                if let lat = validDic["lat"] as? Double {
                                    
                                    if let lng = validDic["long"] as? Double {
                                        let validLoc = CLLocation(latitude: lat, longitude: lng)
                                        creationRequest.location = validLoc
                                    }
                                }
                            }
                        }
                            
                        else {
                            creationRequest.location = nil
                        }
                        creationRequest.addResource(with: PHAssetResourceType.photo, data: imageData, options: nil)
                        
                    }, completionHandler: {success, error in
                        
                        if !success {
                            NSLog("Error occured while saving image to photo library: \(error!)")
                            
                        } else {
                            SVProgressHUD.dismiss()
                            DispatchQueue.main.async {[weak self] in
                                self?.loadLatestPHAsset()
                                completionBlock()
                            }
                        }
                    })
                } else {
                    let temporaryFileName = ProcessInfo().globallyUniqueString as NSString
                    let temporaryFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(temporaryFileName.appendingPathExtension("jpg")!)
                    let temporaryFileURL = URL(fileURLWithPath: temporaryFilePath)
                    
                    PHPhotoLibrary.shared().performChanges({
                        
                        do {
                            try imageData.write(to: temporaryFileURL, options: .atomicWrite)
                            PHAssetChangeRequest.creationRequestForAssetFromImage(atFileURL: temporaryFileURL)
                            SVProgressHUD.dismiss()
                            
                        } catch let error {
                            NSLog("Error occured while writing image data to a temporary file: \(error)")
                            SVProgressHUD.dismiss()
                        }
                        
                    }, completionHandler: {success, error in
                        
                        if !success {
                            NSLog("Error occurred while saving image to photo library: \(error!)")
                            SVProgressHUD.dismiss()
                        }
                        
                        // Delete the temporary file.
                        do {
                            try FileManager.default.removeItem(at: temporaryFileURL)
                        } catch _ {}
                    })
                }
            }else{
                SVProgressHUD.dismiss()
                self.messageAccessNotGiven(title: "Photo Library Permission Required", message: "If you want to save photo you need to grant permission to Photo Library.")
            }
        }
    }

//MARK: - WaterMark

func addWaterMarkImageToPhoto(imageData : Data,  success:@escaping (Data) -> Void) {
    var data : Data?
    let firstImage = UIImage(data: imageData)
    let item = MediaItem(image: firstImage!)
    
    let logoImage = UIImage(named: "water_mark")
    
    let firstElement = MediaElement(image: logoImage!)
    if selectedScope{
        if Device.IS_4_7_INCHES_OR_LARGER(){
            firstElement.frame = CGRect(x: 0, y: (firstImage?.size.height)! - 500, width: 650, height: 500)
        }else{
            firstElement.frame = CGRect(x: 0, y: (firstImage?.size.height)! - 500, width: 650, height: 500)
        }
    }else{
        if Device.IS_4_7_INCHES_OR_LARGER(){
            
            firstElement.frame = CGRect(x: 0, y: (firstImage?.size.height)! - 500, width: 650, height: 500)
        }else{
            if self.photoQuality == "AVCaptureSessionPresetPhoto"{
                firstElement.frame = CGRect(x: 0, y: (firstImage?.size.height)! - 500, width: 650, height: 500)
            }
            if photoQuality == "AVCaptureSessionPresetMedium"{
                firstElement.frame = CGRect(x: 0, y: (firstImage?.size.height)! - 80, width: 120, height: 80)
            }else if photoQuality == "AVCaptureSessionPresetLow"{
                firstElement.frame = CGRect(x: 0, y: (firstImage?.size.height)! - 20, width: 30, height: 20)
            }
        }
    }
    if self.videoDevice?.position == .front{
        if Device.IS_4_7_INCHES_OR_LARGER(){
            firstElement.frame = CGRect(x: 0, y: (firstImage?.size.height)! - 350, width: 500, height: 350)
        }else{
            firstElement.frame = CGRect(x: 0, y: (firstImage?.size.height)! - 180, width: 210, height: 180)
            if self.photoQuality == "AVCaptureSessionPresetPhoto"{
                firstElement.frame = CGRect(x: 0, y: (firstImage?.size.height)! - 180, width: 210, height: 180)
            }
            if photoQuality == "AVCaptureSessionPresetMedium"{
                firstElement.frame = CGRect(x: 0, y: (firstImage?.size.height)! - 80, width: 120, height: 80)
            }else if photoQuality == "AVCaptureSessionPresetLow"{
                firstElement.frame = CGRect(x: 0, y: (firstImage?.size.height)! - 20, width: 30, height: 20)
            }
        }
    }
    
    item.add(elements: [firstElement])
    let mediaProcessor = MediaProcessor()
    
    mediaProcessor.processElements(item: item) { (result, error) in
        data = UIImageJPEGRepresentation(result.image!, 1.0)!
        success(data!)
    }
    
}
    
    func fixingImage (imageData1: Data, _ completionBlock: @escaping FixingImageCompletionHandler) {
        self.pictureActivityIndicatorView.color = #colorLiteral(red: 0.8250325322, green: 0.8026339412, blue: 0.2692880332, alpha: 1)
        self.pictureActivityIndicatorView.type = .ballRotateChase
        self.pictureActivityIndicatorView.startAnimating()
        
        DispatchQueue.global(qos: .background).async {
            // do something in background
       
        
        var imageQaulity = ""

        var imageData = Data()
        var imageData1 = imageData1
        var image : UIImage = UIImage(data: imageData1)!
        let beginImage = CIImage(image: image)
        var quality : CGFloat = -1.0
        
        if self.selectedScope == true {
            
            let context = CIContext(options: nil)
            if let currentFilter1 = self.currentFilter {
                currentFilter1.setValue(beginImage, forKey: kCIInputImageKey)
                if let output = currentFilter1.outputImage {
                    
                    if let cgimg = context.createCGImage(output, from: output.extent) {
                        
                        var image1 = UIImage(cgImage:cgimg, scale:image.scale, orientation:image.imageOrientation)
                        image1 =  image1.rotate(radians: Float(CGFloat(Double(self.rotationSlider.value) *  (2.0*Double.pi / Double(self.self.rotationSlider.maximumValue)))))!
                        // image1 = image1.flipImageLeftRight(image1)
                        if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft {
                            image1 = UIImage(cgImage:image1.cgImage!, scale:image1.scale, orientation:.right)
                        }else{
                            image1 = image1.flipImageLeftRight(image1)
                        }
                        var imageData3 = UIImageJPEGRepresentation(image1, 1.0)
                        
                        if Device.isSizeOrLarger(height: .Inches_4){
                            if self.photoQuality == "AVCaptureSessionPresetPhoto"{
                                if let imageData2 = image1.jpeg(.highest) {
                                    // image = UIImage(data: imageData)!
                                    imageData = imageData2
                                    print(imageData.count)
                                    imageQaulity = "AVCaptureSessionPresetPhoto"
                                }
                                
                            }else if self.photoQuality == "AVCaptureSessionPresetMedium"{
                                if let imageData2 = image1.jpeg(.medium) {
                                    // image = UIImage(data: imageData)!
                                    imageData = imageData2
                                    print(imageData.count)
                                    imageQaulity = "AVCaptureSessionPresetMedium"
                                }
                                
                            }else if self.photoQuality == "AVCaptureSessionPresetLow"{
                                if let imageData2 = image1.jpeg(.low) {
                                    // image = UIImage(data: imageData)!
                                    imageData = imageData2
                                    print(imageData.count)
                                    imageQaulity = "AVCaptureSessionPresetLow"
                                    
                                }
                                
                            }
                        }else{
                            imageData = imageData3!
                        }
                        // do something interesting with the processed image
                    }
                }
            }else{
                imageData1 = UIImageJPEGRepresentation(image, 1.0)!
                var image : UIImage = UIImage(data: imageData1)!
                
                image =  image.rotate(radians: Float(CGFloat(Double(self.rotationSlider.value) *  (2.0*Double.pi / Double(self.rotationSlider.maximumValue)))))!
                if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft {
                    
                } else if UIDevice.current.orientation == UIDeviceOrientation.landscapeRight {
                    image = image.fixedOrientation()
                    image = image.flipImageLeftRight(image)
                    
                } else {
                    image = image.flipImageLeftRight(image)
                }
                
                var imageData3 = UIImageJPEGRepresentation(image, 1.0)
                
                if Device.isSizeOrLarger(height: .Inches_4){
                    
                    if self.photoQuality == "AVCaptureSessionPresetPhoto"{
                        if let imageData2 = image.jpeg(.highest) {
                            // image = UIImage(data: imageData)!
                            imageData = imageData2
                            print(imageData.count)
                            imageQaulity = "AVCaptureSessionPresetPhoto"
                        }
                        
                    } else if self.photoQuality == "AVCaptureSessionPresetMedium"{
                        
                        if let imageData2 = image.jpeg(.medium) {
                            // image = UIImage(data: imageData)!
                            imageData = imageData2
                            print(imageData.count)
                            imageQaulity = "AVCaptureSessionPresetMedium"
                        }
                        
                    } else if self.photoQuality == "AVCaptureSessionPresetLow"{
                        if let imageData2 = image.jpeg(.low) {
                            // image = UIImage(data: imageData)!
                            imageData = imageData2
                            print(imageData.count)
                            imageQaulity = "AVCaptureSessionPresetLow"
                            
                        }
                        
                    }
                } else {
                    imageData = imageData3!
                }
            }
            
        }else{
            
            let context = CIContext(options: nil)
            
            if let currentFilter1 = self.currentFilter {
                
                currentFilter1.setValue(beginImage, forKey: kCIInputImageKey)
                // currentFilter1.setValue(0.8, forKey:kCIInputIntensityKey)
                
                if let output = currentFilter1.outputImage {
                    if let cgimg = context.createCGImage(output, from: output.extent) {
                        var image = UIImage(cgImage:cgimg, scale:image.scale, orientation:image.imageOrientation)
                        // image = image.fixedOrientation()
                        var imageData3 = UIImageJPEGRepresentation(image, 1.0)
                        
                        if Device.isSizeOrLarger(height: .Inches_4){
                            if self.photoQuality == "AVCaptureSessionPresetPhoto"{
                                if let imageData2 = image.jpeg(.highest) {
                                    // image = UIImage(data: imageData)!
                                    imageData = imageData2
                                    print(imageData.count)
                                    imageQaulity = "AVCaptureSessionPresetPhoto"
                                }
                                
                            }else if self.photoQuality == "AVCaptureSessionPresetMedium"{
                                if let imageData2 = image.jpeg(.medium) {
                                    // image = UIImage(data: imageData)!
                                    imageData = imageData2
                                    print(imageData.count)
                                    imageQaulity = "AVCaptureSessionPresetMedium"
                                }
                                
                            }else if self.photoQuality == "AVCaptureSessionPresetLow"{
                                if let imageData2 = image.jpeg(.low) {
                                    // image = UIImage(data: imageData)!
                                    imageData = imageData2
                                    print(imageData.count)
                                    imageQaulity = "AVCaptureSessionPresetLow"
                                    
                                }
                                
                            }
                        }else{
                            imageData = imageData3!
                        }
                    }
                    
                }
            }else
            {
                var image1 = UIImage()
                if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
                    
                    if let watermark = UserDefaults.standard.value(forKey: UserDefaultKey.watermark.rawValue) as? String {
                        image1 = UIImage(cgImage:image.cgImage!, scale:image.scale, orientation:.up)
                        imageData = UIImageJPEGRepresentation(image1, 1.0)!
                    }else{
                        image1 = UIImage(cgImage:image.cgImage!, scale:image.scale, orientation:.down)
                        imageData = UIImageJPEGRepresentation(image1, 1.0)!
                        //  imageData = UIImageJPEGRepresentation(image, 1.0)!
                    }
                    
                }
                
                if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
                    
                    var image1 = image.fixedOrientation()
                    imageData = UIImageJPEGRepresentation(image1, 1.0)!
                }
                var imageData3 = imageData
                if Device.isSizeOrLarger(height: .Inches_4){
                    if self.photoQuality == "AVCaptureSessionPresetPhoto"{
                        if let imageData2 = image.jpeg(.highest) {
                            // image = UIImage(data: imageData)!
                            imageData = imageData2
                            print(imageData.count)
                            imageQaulity = "AVCaptureSessionPresetPhoto"
                        }
                        
                    }else if self.photoQuality == "AVCaptureSessionPresetMedium"{
                        if let imageData2 = image.jpeg(.medium) {
                            // image = UIImage(data: imageData)!
                            imageData = imageData2
                            print(imageData.count)
                            imageQaulity = "AVCaptureSessionPresetMedium"
                        }
                        
                    }else if self.photoQuality == "AVCaptureSessionPresetLow"{
                        if let imageData2 = image.jpeg(.low) {
                            // image = UIImage(data: imageData)!
                            imageData = imageData2
                            print(imageData.count)
                            imageQaulity = "AVCaptureSessionPresetLow"
                            
                        }
                        
                    }
                }else{
                    imageData = imageData3
                }
                
                
                
            }
        }
        var lastWaterImage = UIImage(data: imageData)!
        var watermark1 = UserDefaults.standard.value(forKey: UserDefaultKey.watermark.rawValue)
        if let watermark = UserDefaults.standard.value(forKey: UserDefaultKey.watermark.rawValue) as? String {
            if self.selectedScope{
                
                if UIDevice.current.orientation == UIDeviceOrientation.landscapeRight  {
                    if self.videoDevice?.position == .front {
                        
                        lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.up)
                        
                    }else{
                        
                        lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.left)
                    }
                    
                }else if UIDeviceOrientationIsPortrait(UIDevice.current.orientation){
                    lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.up)
                }else if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft  {
                    if self.videoDevice?.position == .front {
                        
                        lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.up)
                        
                    }else{
                        lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.rightMirrored)
                    }
                    
                }
                lastWaterImage = lastWaterImage.addText(textToDraw: watermark as NSString , atCorner: 2, textColor: nil, textFont: nil, quality: imageQaulity, cameraPos : self.currentCameraPosition!)
                
                //                    }
            }else{
                if UIDevice.current.orientation == UIDeviceOrientation.landscapeRight  {
                    if self.videoDevice?.position == .front {
                        
                        lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.up)
                        
                    }else{
                        lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.down)
                    }
                }else if UIDeviceOrientationIsPortrait(UIDevice.current.orientation){
                    lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.right)
                }else if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft  {
                    if self.videoDevice?.position == .front {
                        
                        lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.down)
                        
                    }else{
                        lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.up)
                    }
                }
                //                    if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft {
                //
                //                        lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.down)
                //                    }else if UIDevice.current.orientation == UIDeviceOrientation.landscapeRight {
                //
                //                    }
                lastWaterImage = lastWaterImage.addText(textToDraw: watermark as NSString , atCorner: 2, textColor: nil, textFont: nil, quality: imageQaulity, cameraPos : self.currentCameraPosition!)
            }
            
        } else {
            
            if self.selectedScope{
                if UIDevice.current.orientation == UIDeviceOrientation.landscapeRight  {
                    
                    lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.left)
                }else if UIDeviceOrientationIsPortrait(UIDevice.current.orientation){
                    lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.upMirrored)
                }else if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft  {
                    lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.rightMirrored)
                }
            }else{
                if UIDevice.current.orientation == UIDeviceOrientation.landscapeRight  {
                    if self.videoDevice?.position == .front {
                        lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.up)
                        
                    } else {
                        lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.down)
                    }
                } else if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
                    lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.right)
                } else if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft {
                    if self.videoDevice?.position == .front {
                        lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.down)
                        
                    }else{
                        lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.up)
                    }
                }
                //                    if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft {
                //
                //                        if let watermark1 = UserDefaults.standard.value(forKey: UserDefaultKey.watermark.rawValue) as? String{
                //                            lastWaterImage = lastWaterImage.addText(textToDraw: watermark1 as! NSString, atCorner: 2, textColor: nil, textFont: nil, quality: imageQaulity,cameraPos : self.currentCameraPosition!)
                //                        }else{
                //                            lastWaterImage = UIImage(cgImage:lastWaterImage.cgImage!, scale:lastWaterImage.scale, orientation:.down)
                //
                //                        }
                //
                //
                //                    }else  if UIDevice.current.orientation == UIDeviceOrientation.landscapeRight {
                //                    }
            }
            
        }
        if Product.purchased {
            imageData = UIImageJPEGRepresentation(lastWaterImage, 1.0)!
            completionBlock(imageData)
        }else{
            imageData = UIImageJPEGRepresentation(lastWaterImage, 1.0)!
            self.addWaterMarkImageToPhoto(imageData: imageData, success: { (data) in
                imageData = data
                completionBlock(imageData)
            })
        }
            
        }
        
    }
}
