//
//  VideoFeaturesExtension.swift
//  CameraApp
//
//  Created by iMac on 07/09/2018.
//  Copyright © 2018 softech. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Photos
import AssetsLibrary
import CoreLocation
import MediaPlayer ////Only for hidding  Volume view
import GLKit
import CoreImage
import SVProgressHUD
import LLVideoEditor
import NVActivityIndicatorView


extension AVCamManualCameraViewController{
    
    func setSlowMotion()
    {
        
        rotationSlider.tintColor = UIColor.yellow
        rotationSlider.isUserInteractionEnabled = true
        isTimeLapsReady = false
        lastSelectedLabel?.textColor = UIColor.white
        if isTimeLapsReady{
            setTimeLapse()
        }
        
        if isSlowMotion {
            isSlowMotion = false
            SVProgressHUD.show(withStatus:"Removing Slow motion for you.")
            desiredFps = 0.0
            removeMovieFileOutput()
            slowMoLabel.textColor = UIColor.white
        }
        else
        {
            SVProgressHUD.show(withStatus:"Setting Slow motion for you.")
            isSlowMotion = true
            desiredFps = 240.0
            slowMoLabel.textColor = UIColor.yellow
            lastSelectedLabel = slowMoLabel
        }
        
        
        let queue = DispatchQueue.global(qos: .default)
        queue.async(execute: {() -> Void in
            if self.desiredFps > 0.0 {
                self.switchFormat(withDesiredFPS: self.desiredFps)
            }
            else {
                self.resetFormat()
            }
            DispatchQueue.main.async(execute: {() -> Void in
                if self.desiredFps > 0.0 {
                    self.timeLapsButton.isHidden = true
                    self.recordButton.typeRaw = 1
                    
                }else {
                    self.recordButton.typeRaw = 0
                }
                
                
                self.changeCaptureMode(self.videoModeButton)
                
                
                SVProgressHUD.dismiss()
            })
        })
        self.isoSlider.minimumValue = self.videoDevice?.activeFormat.minISO ?? 0.0
        self.isoSlider.maximumValue = self.videoDevice?.activeFormat.maxISO ?? 0.0
        self.isoSlider.value = self.videoDevice?.iso ?? 0.0
    }
    
    
    
    func setTimeLapse()
    {
        
        upButtonOutlet.isHidden = true
        downButtonOulet.isHidden = true
        if selectedScope {
            self.sliderValueChanged(sender: self.rotationSlider)
        }
        
        isSlowMotion = false
        lastSelectedLabel?.textColor = UIColor.white
        let queue = DispatchQueue.global(qos: .default)
        
        if isTimeLapsReady {
            SVProgressHUD.show(withStatus:"Removing time-laps.")
            isTimeLapsReady = false
            rotationSlider.tintColor = UIColor.yellow
            rotationSlider.isUserInteractionEnabled = true
            desiredFps = 0.0
            removeMovieFileOutput()
        }
        else
        {
            SVProgressHUD.show(withStatus:"Setting time-laps for you.")
            removeMovieFileOutput()
            isTimeLapsReady = true
            timeLapsLabel.textColor = UIColor.yellow
            lastSelectedLabel = timeLapsLabel
            if currentFilter != nil {
                self._glkView.isHidden = true
            }
            queue.async(execute: {() -> Void in
                self.cofingureVideoDataOutput()
            })
            
            return
        }
        
        
        
        
        queue.async(execute: {() -> Void in
            if self.desiredFps > 0.0 {
                self.switchFormat(withDesiredFPS: self.desiredFps)
            }
            else {
                self.resetFormat()
            }
            DispatchQueue.main.async(execute: {() -> Void in
                if self.desiredFps > 0.0 {
                    self.timeLapsButton.isHidden = true
                    self.recordButton.typeRaw = 1
                    
                }else {
                    self.recordButton.typeRaw = 0
                }
                
                self.changeCaptureMode(self.videoModeButton)
                
                SVProgressHUD.dismiss()
            })
        })
    }
    
    
    
    func set4kFormat()
    {
        rotationSlider.tintColor = UIColor.yellow
        rotationSlider.isUserInteractionEnabled = true
        let sessionObj = sessionQualityArray[0]
        
        if videoQuality == sessionObj._pressetName {
            
            is4kVideo = false
            f4kVideoLabel.textColor = UIColor.white
            
            videoQuality = sessionQualityArray[1]._pressetName
            
            if captureModeTag == AVCamManualCaptureMode.movie.rawValue {
                sessionPressetConfiguration()
            }
        }
        else
        {
            if self.session.canSetSessionPreset(AVCaptureSession.Preset(rawValue: sessionObj._pressetName))
            {
                //AVCaptureSessionPreset3840x2160
                videoQuality = sessionObj._pressetName
                f4kVideoLabel.textColor = UIColor.yellow
                is4kVideo = true
                if captureModeTag == AVCamManualCaptureMode.movie.rawValue {
                    sessionPressetConfiguration()
                }
                self.videoMenu.isHidden = true
                
                
            }
            else
            {
                EZAlertController.alert("Info!", message: "This video quality is not supported by your phone")
            }
        }
        
        
        
    }
    
}
