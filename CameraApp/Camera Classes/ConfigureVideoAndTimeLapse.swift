//
//  ConfigureVideoAndTimeLapse.swift
//  CameraApp
//
//  Created by iMac on 07/09/2018.
//  Copyright © 2018 softech. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Photos
import AssetsLibrary
import CoreLocation
import MediaPlayer ////Only for hidding  Volume view
import GLKit
import CoreImage
import SVProgressHUD
import LLVideoEditor
import NVActivityIndicatorView

extension AVCamManualCameraViewController : AVCaptureAudioDataOutputSampleBufferDelegate {
    
    func cofingureVideoDataOutput() {
        
        if self.videoDevice != nil {
            
            // video output
            let videoDataOutput = AVCaptureVideoDataOutput()
            
            videoDataOutput.setSampleBufferDelegate(self, queue: self.recordingQueue)
            videoDataOutput.alwaysDiscardsLateVideoFrames = true
            
            videoDataOutput.videoSettings = [
                kCVPixelBufferPixelFormatTypeKey as AnyHashable : Int(kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange)
                ] as! [String : Any]
            
            self.session.beginConfiguration()
            self.session.sessionPreset = AVCaptureSession.Preset.iFrame1280x720
            self.session.commitConfiguration()
            
            
            // audio output
            let audioDataOutput = AVCaptureAudioDataOutput()
            audioDataOutput.setSampleBufferDelegate(self, queue: recordingQueue)
            if self.session.canAddOutput(audioDataOutput) {
                self.session.addOutput(audioDataOutput)
            }
            
            if self.session.canAddOutput(videoDataOutput) {
                self.session.addOutput(videoDataOutput)
                heightVideo = videoDataOutput.videoSettings["Height"] as! Int!
                widthVideo = videoDataOutput.videoSettings["Width"] as! Int!
            }
            
            DispatchQueue.main.async { [weak self] in
                self?.recordButton.isEnabled = true
                
                self?.photoButton.isHidden = true
                self?.recordButton.isHidden = true
                self?.timeLapsButton.isHidden = false
                self?.bottomContainerView.bringSubview(toFront: (self?.photoModeButton)!)
                self?.bottomContainerView.bringSubview(toFront: (self?.timeLapsButton)!)
                SVProgressHUD.dismiss()
            }
        }
    }
    
    func timelapsVideoBuffer(_ captureOutput: AVCaptureOutput!, sampleBuffer: CMSampleBuffer!) {
        
        guard isTimeLapsCapturing else { return }
        
        let isVideo = captureOutput is AVCaptureVideoDataOutput
        
        
        if videoWriter == nil && !isVideo {
            let fileManager = FileManager()
            if fileManager.fileExists(atPath: timeLapsFilePath) {
                do {
                    try fileManager.removeItem(atPath: timeLapsFilePath)
                } catch _ {
                }
            }
            
            let fmt = CMSampleBufferGetFormatDescription(sampleBuffer)
            let asbd = CMAudioFormatDescriptionGetStreamBasicDescription(fmt!)
            
//            Logger.log("setup writer")
//            if isFilterPhoto{
//                videoWriter = TimeLapseVideoWriter(
//                    fileUrl: timeLapsFilePathUrl,
//                    height: 600, width:390,
//                    channels: Int((asbd?.pointee.mChannelsPerFrame)!),
//                    samples: (asbd?.pointee.mSampleRate)!
//                )
//            }else{
            if isFilterPhoto {
                if UIDevice.current.orientation == UIDeviceOrientation.portrait{
                    videoWriter = TimeLapseVideoWriter(
                    fileUrl: timeLapsFilePathUrl,
                    height:610 , width:380,
                    channels: Int((asbd?.pointee.mChannelsPerFrame)!),
                    samples: (asbd?.pointee.mSampleRate)!)
                }else{
                videoWriter = TimeLapseVideoWriter(
                    fileUrl: timeLapsFilePathUrl,
                    height:400 , width:580,
                    channels: Int((asbd?.pointee.mChannelsPerFrame)!),
                    samples: (asbd?.pointee.mSampleRate)!)
                }
            
            }else{
                videoWriter = TimeLapseVideoWriter(
                    fileUrl: timeLapsFilePathUrl,
                    height:heightVideo , width:widthVideo,
                    channels: Int((asbd?.pointee.mChannelsPerFrame)!),
                    samples: (asbd?.pointee.mSampleRate)!)
            }
   //         }
        }
        
        if !isVideo {
            return
        }
        
        currentSkipCount += 1
        guard currentSkipCount == skipFrameSize else { return }
        currentSkipCount = 0
        var buffer = sampleBuffer
        
        if lasttimeStamp.value == 0 {
            lasttimeStamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
            
        } else {
            lasttimeStamp = CMTimeAdd(lasttimeStamp, CMTimeMake(1, 30))
            buffer = setTimeStamp(sampleBuffer, newTime: lasttimeStamp)
        }
        
        Logger.log("write")
        videoWriter?.write(buffer!, isVideo: isVideo)
    }
    
    func setTimeStamp(_ sample: CMSampleBuffer, newTime: CMTime) -> CMSampleBuffer {
        var count: CMItemCount = 0
        CMSampleBufferGetSampleTimingInfoArray(sample, 0, nil, &count);
        var info = [CMSampleTimingInfo](repeating: CMSampleTimingInfo(duration: CMTimeMake(0, 0), presentationTimeStamp: CMTimeMake(0, 0), decodeTimeStamp: CMTimeMake(0, 0)), count: count)
        CMSampleBufferGetSampleTimingInfoArray(sample, count, &info, &count);
        
        for i in 0..<count {
            info[i].decodeTimeStamp = newTime
            info[i].presentationTimeStamp = newTime
        }
        
        var out: CMSampleBuffer?
        CMSampleBufferCreateCopyWithNewTiming(nil, sample, count, &info, &out);
        return out!
    }
    
    class TimeLapseVideoWriter : NSObject {
        var fileWriter: AVAssetWriter!
        var videoInput: AVAssetWriterInput!
        var audioInput: AVAssetWriterInput!
        
        init(fileUrl:URL!, height:Int, width:Int, channels:Int, samples:Float64){
            fileWriter = try? AVAssetWriter(outputURL: fileUrl, fileType: AVFileType.mov)
            
            let videoOutputSettings: Dictionary<String, AnyObject> = [
                AVVideoCodecKey : AVVideoCodecH264 as AnyObject,
                AVVideoWidthKey : width as AnyObject,
                AVVideoHeightKey : height as AnyObject
            ]
            videoInput = AVAssetWriterInput(mediaType: AVMediaType.video, outputSettings: videoOutputSettings)
            // videoInput.
            //  videoInput.expectsMediaDataInRealTime = true
            if !UIDeviceOrientationIsPortrait(UIDeviceOrientation.portrait){
                videoInput.transform = CGAffineTransform(rotationAngle: CGFloat.pi/2)
            }
            fileWriter.add(videoInput)
            
            let audioOutputSettings: Dictionary<String, AnyObject> = [
                AVFormatIDKey : Int(kAudioFormatMPEG4AAC) as AnyObject,
                AVNumberOfChannelsKey : channels as AnyObject,
                AVSampleRateKey : samples as AnyObject,
                AVEncoderBitRateKey : 128000 as AnyObject
            ]
            audioInput = AVAssetWriterInput(mediaType: AVMediaType.audio, outputSettings: audioOutputSettings)
            audioInput.expectsMediaDataInRealTime = true
            fileWriter.add(audioInput)
        }
        
        func write(_ sample: CMSampleBuffer, isVideo: Bool){
            if CMSampleBufferDataIsReady(sample) {
                if fileWriter.status == AVAssetWriterStatus.unknown {
                    Logger.log("Start writing, isVideo = \(isVideo), status = \(fileWriter.status.rawValue)")
                    let startTime = CMSampleBufferGetPresentationTimeStamp(sample)
                    fileWriter.startWriting()
                    fileWriter.startSession(atSourceTime: startTime)
                }
                if fileWriter.status == AVAssetWriterStatus.failed {
                    Logger.log("Error occured, isVideo = \(isVideo), status = \(fileWriter.status.rawValue), \(fileWriter.error!.localizedDescription)")
                    return
                }
                if isVideo {
                    if videoInput.isReadyForMoreMediaData {
                        videoInput.append(sample)
                    }
                }else{
                    if audioInput.isReadyForMoreMediaData {
                        audioInput.append(sample)
                    }
                }
            }
        }
        
        func finish(_ callback: @escaping (Void) -> Void){
            if fileWriter.status.rawValue == 0 {
                return
            }
            fileWriter.finishWriting(completionHandler: callback)
        }
    }
    
    
    @IBAction func start_OR_Stop_TimeLapsVideo() {
        self.mirrorScrollView.isHidden = true
        
        if !isTimeLapsCapturing {
            startVideoRecordTimer()
            startTimeLapsVide()
        }else {

            stopVideoRecordTimer()
            stopTimeLapsVideo()
            
            if currentFilter != nil {
                self._glkView.isHidden = false
            }
            
        }
    }
    
    func startTimeLapsVide(){
        if !isTimeLapsCapturing{
            Logger.log("in")
            if UIDevice.current.isMultitaskingSupported {
                self.backgroundRecordingID = UIApplication.shared.beginBackgroundTask(expirationHandler: nil)
            }
            blinkRedDot.blink()
            self.topContainerView.isHidden = true
            self.blinkViewRecord.isHidden = false
            self.menuButton.isHidden = true
            self.photoModeButton.isHidden = true
            self.thumbnailBtn.isHidden = true
            
            
            
            isTimeLapsCapturing = true
            timeLapsButton.buttonState = .recording
        }
    }
    
    
    func stopTimeLapsVideo(){
        if isTimeLapsCapturing {
            isTimeLapsCapturing = false
            timeLapsButton.buttonState = .normal
            blinkViewRecord.isHidden = true
            self.topContainerView.isHidden = false
            self.menuButton.isHidden = false
            self.photoModeButton.isHidden = false
            self.thumbnailBtn.isHidden = false
            
            
            
            
            
            DispatchQueue.main.async(execute: { () -> Void in
                Logger.log("in")
               
                    self.videoWriter!.finish { () -> Void in
                    Logger.log("Recording finished.")
                    self.videoWriter = nil
                   // self.changeComposition(self.timeLapsFilePathUrl)
                    self.saveVideoToPhotoLibrary(self.timeLapsFilePathUrl, error: nil)
                    
                }
               
            })
        }
    }
}
