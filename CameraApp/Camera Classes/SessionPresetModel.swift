//
//  SessionPresetModel.swift
//  CameraApplication
//
//  Created by Ali Apple on 8/17/17.
//  Copyright © 2017 softech. All rights reserved.
//

import Foundation

class SessionPresetModel {
    var _pressetName : String = ""
    var _pressetKey : String = ""
    var _index : Int = 0
    var _quality : String = ""
    
    
    init(data : NSDictionary) {
        print(data)
        self._pressetName =  data.value(forKey: "value") as? String ?? ""
        self._pressetKey =  (data.value(forKey: "key") as? String) ?? ""
        self._index = (data.value(forKey: "index") as? Int) ?? 0
        self._quality =  (data.value(forKey: "quality") as? String) ?? ""
    }
}
