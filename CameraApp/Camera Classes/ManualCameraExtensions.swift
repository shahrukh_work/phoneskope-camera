//
//  ManualCameraExtensions.swift
//  CameraApp
//
//  Created by iMac on 06/09/2018.
//  Copyright © 2018 softech. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Photos
import AssetsLibrary
import CoreLocation
import MediaPlayer ////Only for hidding  Volume view
import GLKit
import CoreImage
import SVProgressHUD
import LLVideoEditor
import NVActivityIndicatorView

//MARK: - VIDEO START TIMER SECTION
 extension AVCamManualCameraViewController {
    
     func startVideoRecordTimer() {
        videoTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
        startTimerRecordLbl.isHidden = false
    }
    
    @objc private func runTimedCode() {
        videoCountrer += 1
        hmsFrom(seconds: videoCountrer) { [weak self] (h, m, s) in
            let hours = self?.getStringFrom(seconds: h)
            let minutes = self?.getStringFrom(seconds: m)
            let seconds = self?.getStringFrom(seconds: s)
            
            if hours != nil && minutes != nil && seconds != nil {
                print("\(hours!):\(minutes!):\(seconds!)")
                DispatchQueue.main.async {
                    self?.startTimerRecordLbl.text = "\(hours!) : \(minutes!) : \(seconds!)"
                }
            }
        }
    }
    
     func stopVideoRecordTimer() {
        if videoTimer != nil {
            videoTimer.invalidate()
            videoTimer = nil
            videoCountrer = 0
            self.startTimerRecordLbl.text = "00:00:00"
            self.startTimerRecordLbl.isHidden = true
        }
    }
    
    func getStringFrom(seconds: Int) -> String {
        return seconds < 10 ? "0\(seconds)" : "\(seconds)"
    }
}


//MARK: - TIMER BASE SECTION
extension AVCamManualCameraViewController {
    
     var snapShotTimerLimit : Int {
        set {
            UserDefaults.standard.setValue(newValue, forKey: UserDefaultKey.snaptimer.rawValue)
            UserDefaults.standard.synchronize()
        }get {
            return UserDefaults.standard.value(forKey: UserDefaultKey.snaptimer.rawValue) as? Int ?? 0
        }
    }
    
    func timerViewAppearFunction() {
        
        self.hideViews(view: topContainerSubTimerView)
        self.savedSnapShotState()
    }
    
    @IBAction  func timerViewDisappearFunction(_ sender : UIButton) {
        snapShotTimerLimit = sender.tag
        if snapShotTimerLimit > 0 {
            snapShotToggleProperty = true
        }else {
            snapShotToggleProperty = false
        }
        
        savedSnapShotState()
        
        delay(0.1) { [weak self] in
            self?.topContainerSubTimerView.isHidden = true
        }
    }
    
    
    @IBAction  func timerButtonSelectionFunction(_ sender : UIButton) {
        timerViewDisappearFunction(sender)
    }
    
     func startCameraSnapShotTimer() {
        
        let screenSize = self.view.bounds.size
        let x = (screenSize.width - 100)/2
        let y = (screenSize.height - 100)/2
        self.snapShotCounter = Float(snapShotTimerLimit)
        
        snapShotTimerLbl = UILabel(frame: CGRect(x: x, y: y, width: 100, height: 100))
        snapShotTimerLbl.font = UIFont.boldSystemFont(ofSize: 80)
        snapShotTimerLbl.textColor = UIColor.white
        snapShotTimerLbl.textAlignment = .center
        snapShotTimerLbl.text = "\(snapShotTimerLimit)"
        self.view.addSubview(snapShotTimerLbl)
        
        snapShotTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(snapShotTimerRepeatCode), userInfo: nil, repeats: true)
    }
    
    @objc private func snapShotTimerRepeatCode() {
        
        print("snapshot timer :::: \(snapShotCounter)")
        //snapShotTimerFlash()
        snapShotCounter -= Float(0.5)
        
        if snapShotCounter == 0.0 {
            stopCameraSnapShotTimer()
            if Int(snapShotCounter) == 0 {
                if #available(iOS 10.0, *) {
                    self.capturePhoto()
                    snapShotToggleProperty = true
                } else {
                    self.snapStillImage()
                }
            }
            
            return
        }
        
        if snapShotCounter.truncatingRemainder(dividingBy: 1) == 0 {
            //it's an integer
            snapShotTimerLbl.text = "\(Int(snapShotCounter))"
        }
    }
    
     func stopCameraSnapShotTimer() {
        if snapShotTimer != nil {
            snapShotTimer.invalidate()
            snapShotTimer = nil
            snapShotCounter = 0
            
            if snapShotTimerLbl != nil {
                snapShotTimerLbl.removeFromSuperview()
                snapShotTimerLbl = nil
            }
        }
    }
    
     func snapShotTimerFlash() {
        if let device = AVCaptureDevice.default(for: AVMediaType.video), device.hasTorch {
            do {
                try device.lockForConfiguration()
                let torchOn = !device.isTorchActive
                try device.setTorchModeOn(level: 1.0)
                device.torchMode = torchOn ? .on : .off
                device.unlockForConfiguration()
            } catch {
                print("error")
            }
        }
    }
    
    
     func savedSnapShotState() {
        switch snapShotTimerLimit {
        case 1:
            oneSecondBtn.titleLabel?.textColor = UIColor.yellow
            threeSecondBtn.titleLabel?.textColor = UIColor.white
            tenSecondBtn.titleLabel?.textColor = UIColor.white
        case 3:
            threeSecondBtn.titleLabel?.textColor = UIColor.yellow
            oneSecondBtn.titleLabel?.textColor = UIColor.white
            tenSecondBtn.titleLabel?.textColor = UIColor.white
        case 10:
            tenSecondBtn.titleLabel?.textColor = UIColor.yellow
            oneSecondBtn.titleLabel?.textColor = UIColor.white
            threeSecondBtn.titleLabel?.textColor = UIColor.white
        default:
            oneSecondBtn.titleLabel?.textColor = UIColor.white
            threeSecondBtn.titleLabel?.textColor = UIColor.white
            tenSecondBtn.titleLabel?.textColor = UIColor.white
        }
    }
}


//MARK:- FLUSH BASE SECTION

extension AVCamManualCameraViewController {
    
    var flushMode : AVCaptureDevice.FlashMode {
        set {
            UserDefaults.standard.setValue(newValue.rawValue, forKey: UserDefaultKey.flush.rawValue)
            UserDefaults.standard.synchronize()
        }
        get {
            let flushValue = UserDefaults.standard.value(forKey: UserDefaultKey.flush.rawValue) as? Int ?? 2
            return AVCaptureDevice.FlashMode(rawValue: flushValue)!
        }
    }
    
    
    func flushViewAppearFunction() {
        self.photoMenuSV.isHidden = true
        self.videoMenu.isHidden = true
        self.hideViews(view: topContainerSubFlushView)
        self.flushButtonSetting()
    }
    
    
    @IBAction  func flushViewDisappearFunction(_ sender : UIButton) {
        flushMode = AVCaptureDevice.FlashMode(rawValue: sender.tag)!
        flushButtonSetting()
        
    }
    
    @IBAction  func flushButtonSelectionFunction(_ sender : UIButton) {
        flushViewDisappearFunction(sender)
    }
    
     func flushButtonSetting() {
        switch flushMode {
        case .off:
            
            
            offFlushBtn.setTitleColor(UIColor.yellow, for: UIControlState.normal)
            autoFlushBtn.setTitleColor(UIColor.white, for: UIControlState.normal)
            onFlushBtn.setTitleColor(UIColor.white, for: UIControlState.normal)
            
            photoFlashLabel.text = "off"
        case .on:
            onFlushBtn.setTitleColor(UIColor.yellow, for: UIControlState.normal)
            autoFlushBtn.setTitleColor(UIColor.white, for: UIControlState.normal)
            
            offFlushBtn.setTitleColor(UIColor.white, for: UIControlState.normal)
            
            photoFlashLabel.text = "on"
        case .auto:
            
            autoFlushBtn.setTitleColor(UIColor.yellow, for: UIControlState.normal)
            onFlushBtn.setTitleColor(UIColor.white, for: UIControlState.normal)
            
            offFlushBtn.setTitleColor(UIColor.white, for: UIControlState.normal)
            photoFlashLabel.text = "auto"
        }
    }
}


//MARK: - GESTURE SECTION

extension AVCamManualCameraViewController : UIGestureRecognizerDelegate {
    
    func pinchAction(sender:UIPinchGestureRecognizer) {
        
        if zoomLabelTimer != nil
        {
            zoomLabelTimer.invalidate()
        }
        if lastRectangle != nil{
            
            removeRectangle()
            
        }
        pinchZoom(sender)
        
    }
    
    func pinchZoom(_ pinch: UIPinchGestureRecognizer) {
        if let validDevice =  self.videoDevice {
            do {
                try validDevice.lockForConfiguration()
                //defer {getDevice?.unlockForConfiguration()}
                
                zoomScale = min(maxZoomScale, max(1.0, min(beginZoomScale * pinch.scale,  validDevice.activeFormat.videoMaxZoomFactor)))
                validDevice.videoZoomFactor = zoomScale
                validDevice.unlockForConfiguration()
                zoomLabel.isHidden = false
                zoomLabel.text = "\(String(format: "%.1f", zoomScale))x"
                zoomLabelTimer =  Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(hideZoomLabel), userInfo: nil, repeats: false)
                print("video factor value :: \(zoomScale)")
                
            } catch {
                print("\(error.localizedDescription)")
            }
        }
    }
    
    
    //Delegate methods
    public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer.isKind(of: UIPinchGestureRecognizer.self) {
            beginZoomScale = zoomScale;
        }
        
        return true
    }
}


//MARK: - MANUAL FOCUS SECTION
extension AVCamManualCameraViewController {
    
    @IBAction  func focusViewAppearFunction(_ sender : UIButton) {
        focusCheck = !focusCheck
        //self.hideViews(view: manualHUDFocusView)
        self.photoMenuSV.isHidden = true
        self.videoMenu.isHidden = true
        if focusCheck {
            self.hideViews(view:lensPositionSlider)
            self.ISOSlider.isHidden = true
            self.lensPositionSlider.isHidden = false
            
            focusImageView.isHidden = false
            focusImageView.image = UIImage(named : "FocusSlider")
            self.ISOSlider.minimumValue = self.videoDevice?.activeFormat.minISO ?? 0.0
            self.ISOSlider.maximumValue = self.videoDevice?.activeFormat.maxISO ?? 0.0
            self.ISOSlider.value = self.videoDevice?.iso ?? 0.0
        }else{
            if self.lensPositionSlider.isHidden && self.ISOSlider.isHidden {
                focusImageView.isHidden = true
            }else if !self.ISOSlider.isHidden || self.lensPositionSlider.isHidden  {
                focusImageView.isHidden = false
            }else{
                focusImageView.isHidden = true
            }
            self.lensPositionSlider.isHidden = true
        }
        
    }
    
    @IBAction  func focusViewDisappearFunction(_ sender : UIButton) {
        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.manualHUDFocusView.isHidden = true
            self?.topContainerView.isHidden = false
        }
    }
}


//MARK: - MANUAL EXPOSURE SECTION
extension AVCamManualCameraViewController {
    
    @IBAction  func exposureViewAppearFunction(_ sender : UIButton) {
        isoCheck = !isoCheck
        self.photoMenuSV.isHidden = true
        self.videoMenu.isHidden = true
        if isoCheck {
            self.hideViews(view:ISOSlider)
            self.ISOSlider.isHidden = false
            self.lensPositionSlider.isHidden = true
            focusImageView.isHidden = false
            focusImageView.image = UIImage(named : "BrightnessSlider")
            self.ISOSlider.minimumValue = self.videoDevice?.activeFormat.minISO ?? 0.0
            self.ISOSlider.maximumValue = self.videoDevice?.activeFormat.maxISO ?? 0.0
            self.ISOSlider.value = self.videoDevice?.iso ?? 0.0
            
            self.isoSlider.minimumValue = self.videoDevice?.activeFormat.minISO ?? 0.0
            self.isoSlider.maximumValue = self.videoDevice?.activeFormat.maxISO ?? 0.0
            self.isoSlider.value = self.videoDevice?.iso ?? 0.0
        }else{
            if self.lensPositionSlider.isHidden && self.ISOSlider.isHidden {
                focusImageView.isHidden = true
            }else if self.ISOSlider.isHidden || !self.lensPositionSlider.isHidden  {
                focusImageView.isHidden = false
            }else{
                focusImageView.isHidden = true
            }
            
            self.ISOSlider.isHidden = true
        }
        
    }
    
    @IBAction  func exposureViewDisappearFunction(_ sender : UIButton) {
        self.focusImageView.isHidden = true
        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.manualHUDExposureView.isHidden = true
            self?.topContainerView.isHidden = false
            
        }
    }
}


//MARK: - SAVE PHOTO SECTauthorizION
extension AVCamManualCameraViewController {
    
     var IS_RAW_FORMATE : String  {
        set {
            UserDefaults.standard.set(newValue, forKey: UserDefaultKey.rawphoto.rawValue)
            UserDefaults.standard.synchronize()
        }
        get {
            
            if let rawValue = UserDefaults.standard.string(forKey: UserDefaultKey.rawphoto.rawValue){
                
                return rawValue
            }else{
                UserDefaults.standard.set("Jpg", forKey: UserDefaultKey.rawphoto.rawValue)
                return "Jpg"
            }
            
        }
    }
    @IBAction  func photoViewDisappearFunction(_ sender : UIButton) {
        
        
        
        if sender.tag == 1 {
            IS_RAW_FORMATE = "Raw"
            self.rawJpgButtonColorSetting()
        }else if sender.tag == 2{
            IS_RAW_FORMATE = "RawJpg"
            self.rawJpgButtonColorSetting()
        }
        else if sender.tag == 0 {
            IS_RAW_FORMATE = "Jpg"
            self.rawJpgButtonColorSetting()
        }
  
    }
    
    func rawJpgButtonColorSetting() {
        
        if IS_RAW_FORMATE == "Raw" {
            rawPhotoButton.setTitleColor(UIColor.yellow, for: UIControlState.normal)
            jpgPhotoButton.setTitleColor(UIColor.white, for: UIControlState.normal)
            jpgRawButtonOutlet.setTitleColor(UIColor.white, for: UIControlState.normal)
        }else if IS_RAW_FORMATE == "Jpg" {
            rawPhotoButton.setTitleColor(UIColor.white, for: UIControlState.normal)
            jpgRawButtonOutlet.setTitleColor(UIColor.white, for: UIControlState.normal)
            jpgPhotoButton.setTitleColor(UIColor.yellow, for: UIControlState.normal)
        }else if IS_RAW_FORMATE == "RawJpg" {
            jpgRawButtonOutlet.setTitleColor(UIColor.yellow, for: UIControlState.normal)
            rawPhotoButton.setTitleColor(UIColor.white, for: UIControlState.normal)
            jpgPhotoButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        }
    }
    

}

//MARK:- LAST SAVED VIDEO OR IMAGE SECTION
extension AVCamManualCameraViewController {
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        photoManager.requestImage(for: asset, targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFill, options: option, resultHandler: {(result, _)->Void in
            thumbnail = result ?? UIImage(named: "Photo Camera Icon")!
            self.pictureActivityIndicatorView.stopAnimating()
        })
        
        return thumbnail
    }
    
    func loadLatestPHAsset() {
        
        let fetchAsset = PHAsset.fetchAssets(with: nil)
        
        if let phAssest =  fetchAsset.lastObject {
            let thumbImg = getAssetThumbnail(asset: phAssest)
            timeValue = CMTimeMake(Int64(phAssest.duration), 10000)
            
            
            self.bottomContainerView.bringSubview(toFront: self.thumbnailBtn)
            self.thumbnailBtn.setImage(thumbImg, for: .normal)
            
        }
    }
    
    @IBAction func thumbnailImageButtonFunction() {
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .authorized {
            switch photos {
            case .authorized:
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                if let pickerCont = storyBoard.instantiateViewController(withIdentifier: "AVCamMediaVC") as? AVCamMediaVC {
                    self.present(pickerCont, animated: true, completion: nil)
                    
                    
                }
            case .denied:
                self.messageAccessNotGiven(title : "Photo Library Permission required", message : "If you want to show thumbnail image you need to grant photo library permission.")
            default:
                break
            }
        }
        
        
    }
}





