//
//  AAPLCameraViewController.swift
//  AVCamManual
//
//  Translated by OOPer in cooperation with shlab.jp, on 2015/4/26.
//
//
/*
 Copyright (C) 2015 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information
 
 Abstract:
 View controller for camera interface.
 */

var CIFilterNames = [
    "None",
    "CIPhotoEffectMono",
    "CIPhotoEffectTonal",
    "CIPhotoEffectNoir",
    "CIPhotoEffectFade",
    "CIPhotoEffectChrome",
    "CIPhotoEffectProcess",
    "CIPhotoEffectTransfer",
    "CIPhotoEffectInstant",
]

struct CIFilterModel {
    var image : UIImage?
    var filter : CIFilter?
    var name : String?
    
}

typealias ImageBufferHandler = ((_ imageBuffer: CVPixelBuffer, _ timestamp: CMTime, _ outputBuffer: CVPixelBuffer?) -> ())


import UIKit
import KeychainSwift
import AVFoundation
import Photos
import AssetsLibrary
import CoreLocation
import MediaPlayer ////Only for hidding  Volume view
import GLKit
import CoreImage
import SVProgressHUD
import LLVideoEditor
import NVActivityIndicatorView
import StoreKit


var CapturingStillImageContext = 0 //### iOS < 10.0
var SessionRunningContext = 0
var FocusModeContext = 0
var ExposureModeContext = 0
var WhiteBalanceModeContext = 0
var LensPositionContext = 0
var ExposureDurationContext = 0
var ISOContext = 0
var ExposureTargetBiasContext = 0
var ExposureTargetOffsetContext = 0
var DeviceWhiteBalanceGainsContext = 0
var LensStabilizationContext = 0 //### iOS < 10.0
var VolumModeContext = 0
var lastRectangle:CustomCircle? = nil
var lastBigRectangle:CustomCircle? = nil
var lastSelectedLabel:UILabel? = nil
var isSlowMotion = false
var isTimeLapsReady = false
var isTelephoto = false
var is4kVideo = false



enum AVCamManualSetupResult: Int {
    case success
    case cameraNotAuthorized
    case sessionConfigurationFailed
}

enum AVCamManualCaptureMode: Int {
    case photo
    case movie
}

enum CameraPosition {
    case front
    case rear
}

public enum KeyPath : String {
    case volum = "outputVolume"
    case autoFocus = "adjustingFocus"
}

public enum UserDefaultKey : String {
    case flush = "FlushMode"
    case snaptimer = "SnapShotTimer"
    case rawphoto = "RawPhotoFormat"
    case location = "location"
    case watermark = "watermark"
}

struct Product {
    static var purchased = false
    
}

//### Compatibility types...
private protocol AVCaptureDeviceDiscoverySessionType: class {
    @available(iOS 10.0, *)
    var devices: [AVCaptureDevice]! { get }
}

//@available(iOS 10.0, *)
//extension AVCaptureDevice.DiscoverySession: AVCaptureDeviceDiscoverySessionType {
//   public var devices: [AVCaptureDevice]! {
//
//    }
//}

//@objc protocol AVCapturePhotoOutputType {
//    @available(iOS 10.0, *)
//    var isLensStabilizationDuringBracketedCaptureSupported: Bool {get}
//    @available(iOS 10.0, *)
//    @objc(availableRawPhotoPixelFormatTypes)
//    var __availableRawPhotoPixelFormatTypes: [NSNumber] {get}
//    @available(iOS 10.0, *)
//    var isHighResolutionCaptureEnabled: Bool {get @objc(setHighResolutionCaptureEnabled:) set}
//    @available(iOS 10.0, *)
//    @objc(supportedFlashModes)
//    var __supportedFlashModes: [NSNumber] {get}
//    @available(iOS 10.0, *)
//    func connection(withMediaType mediaType: String!) -> AVCaptureConnection!
//    @available(iOS 10.0, *)
//    @objc(capturePhotoWithSettings:delegate:)
//    func capturePhoto(with settings: AVCapturePhotoSettings, delegate: AVCapturePhotoCaptureDelegate)
//}
//
// @available(iOS 10.0, *)
// extension AVCapturePhotoOutput:AVCapturePhotoOutputType {
// }

@objc(AVCamManualCameraViewController)
@objcMembers class AVCamManualCameraViewController: UIViewController,AVCaptureFileOutputRecordingDelegate, UIPickerViewDelegate, UIPickerViewDataSource,  CLLocationManagerDelegate, CheckVideosDelegate{
    
    //MARK: - ------ Properties ------
    
    var isAudioPermissionGranted = false
    var burstShotimages:[UIImage] = []
    var isShooting = false
    var counter = 0
    var isLongPress: Bool = false
    var isFocusLongPress = false
    fileprivate var startDateInterval: TimeInterval = 0
    let pianoSound = URL(fileURLWithPath: Bundle.main.path(forResource: "camerasound", ofType: "mp3")!)
    var audioPlayer = AVAudioPlayer()
    var longPressGesturePhoto: UILongPressGestureRecognizer?
    var deviceOrientaion: UIDeviceOrientation?
    var isFilterTriggered = false
    
    
    let dataHandlerButton = DataHandler()
    var buttonTag = "99"
    var buttonTagIndex = 70
    var notificationHandler = NotificationCenter.default
    var item : UITextInputAssistantItem! = nil
    var item2 : UITextInputAssistantItem! = nil
    var rifleValue1 : Rifle? = nil
    var slideVal = 0.0
    
    let dataHandler = DataHandler()
    let picker = UIPickerView()
    var rifleValue : [Rifle]!
    var selectedScope : Bool = false
    let volumeView = MPVolumeView()
    var mediaPicker : UIImagePickerController!
    
    var replicatorInstances: Int = 4
    var replicatorLayer = CAReplicatorLayer()
    var filterString = ""
    var url1  : URL!
    var isVideoRecording = false
    var angleValue = 0.0
    
    var isRotationLock = false
    var lastSeletedLabelVideo : UILabel!
    var lastSeletedLabelPhoto : UILabel!
    var photoQualitySelected = ""
    var  snapShotTimer1 = Timer()
    
    var isoCheck = false
    var focusCheck = false
    var checkVolume = false
    let status  = CLLocationManager.authorizationStatus()
    var locationMgr = CLLocationManager()
    var timeValue : CMTime!
    
    var currentCameraPosition : CameraPosition?
    var beginZoomScale  =  CGFloat(1.0)
    var maxZoomScale = CGFloat(5.0)//CGFloat.greatestFiniteMagnitude
    var zoomScale  =  CGFloat(1.0)
    var activityView = UIActivityIndicatorView()
    
    var videoTimer: Timer!
    var videoCountrer = 0
    var snapShotTimer: Timer!
    var snapShotCounter : Float = 0.0
    var snapShotToggleProperty = true
    var zoomLabelTimer : Timer!
    var burstShotTimer: Timer!
    
    
    //MARk: - ------- IBOUtlets -------
    @IBOutlet weak var screenBrightnessOutlet: UIButton!
    @IBOutlet weak var featureListButtonOutlet: UIButton!
    @IBOutlet weak var videoQualityButtonOutlet: UIButton!
    @IBOutlet weak var photoQualityButtonOutlet: UIButton!
    @IBOutlet weak var stablizationButtonOutlet: UIButton!
    @IBOutlet weak var telePhotoButtonOulet: UIButton!
    @IBOutlet weak var queueVideosListBtnOutlet: UIButton!
    
    @IBOutlet weak var rotateButton1: UIButton!
    @IBOutlet weak var rotateButton2: UIButton!
    @IBOutlet weak var activityIndicationView: NVActivityIndicatorView!
    
    @IBOutlet weak var scopeButtonMenu: UIButton!
    @IBOutlet weak var selectRifleOulet: UIButton!
    @IBOutlet weak var saveRifleOutlet: UIButton!
    @IBOutlet weak var focusLeftViewButton: UIButton!
    @IBOutlet weak var isoLeftViewButton: UIButton!
    @IBOutlet weak var rifleSettingsTextField: UITextField!
    
    @IBOutlet weak var focusLeftView: UIView!
    @IBOutlet weak var isoLeftView: UIView!
    @IBOutlet weak var upButtonOutlet: UIButton!
    @IBOutlet weak var downButtonOulet: UIButton!
    @IBOutlet weak var binocularScopeOutlet: UIButton!
    @IBOutlet weak var focusSlider: UISlider!
    @IBOutlet weak var rotationSlider: UISlider!
    @IBOutlet weak var isoSlider: UISlider!
    
    @IBOutlet weak var mirrorOnOutlet: UIButton!
    @IBOutlet weak var mirrorOffOutlet: UIButton!
    @IBOutlet weak var rifleScopeOutlet: UIButton!
    @IBOutlet weak var phoneScopeImageView: UIImageView!
    @IBOutlet weak var selectScopeView: UIView!
    @IBOutlet weak var scopeStackView: UIStackView!
    @IBOutlet weak var stackViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var blinkRedDot: UIView!
    @IBOutlet weak var blinkViewRecord: UIView!
    @IBOutlet weak var rotationImageView: UIImageView!
    @IBOutlet weak var brightImageView: UIImageView!
    @IBOutlet weak var focusImageView: UIImageView!
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var gunImageView: UIImageView!
    
    //MARK : - photo topbar outlets
    @IBOutlet weak var topContainerView: UIView!
    @IBOutlet weak var photoMenu: UIView!
    @IBOutlet weak var photoFlashLabel: UILabel!
    @IBOutlet weak var switchCameraPhotoTop: UIControl!
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var geoTaggingLabel: UILabel!
    @IBOutlet weak var fileTypeLabel: UILabel!
    
    @IBOutlet weak var photFlashView: UIControl!
    @IBOutlet weak var fileTypeView: UIView!
    @IBOutlet weak var stabilizationView: UIView!
    @IBOutlet weak var geoTagView: UIView!
    @IBOutlet weak var turnOffTrainingModeOutlet: UIButton!
    @IBOutlet weak var turnOffTrainingModeView: UIView!
    
    //MARK: - ------ VIDEO TAPBAR OUTLET -------
    
    @IBOutlet weak var videoMenu: UIView!
    @IBOutlet weak var timeLapsLabel: UILabel!
    @IBOutlet weak var slowMoLabel: UILabel!
    @IBOutlet weak var f4kVideoLabel: UILabel!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var zoomLabel: UILabel!
    //WATER MARK 
    @IBOutlet weak var waterMarkView: UIView!
    @IBOutlet weak var waterMarkLabel: UILabel!
    
    
    
    //MARK: - ------ TiMER OUTLETS -------
    @IBOutlet weak var topContainerSubTimerView: UIView!
    @IBOutlet  weak var oneSecondBtn: UIButton!
    @IBOutlet  weak var threeSecondBtn: UIButton!
    @IBOutlet  weak var tenSecondBtn: UIButton!
    var snapShotTimerLbl : UILabel!
    
    
    @IBOutlet weak var photoMenuSV: UIScrollView!
    @IBOutlet weak var geoTagOnnButton: UIButton!
    @IBOutlet weak var geoTagOffButton: UIButton!
    @IBOutlet weak var stabilizationOffButton: UIButton!
    @IBOutlet weak var stabilizationOnnButton: UIButton!
    
    //MARK: - ------ FLASH MODE -------
    
    @IBOutlet weak var topContainerSubFlushView: UIView!
    @IBOutlet  weak var autoFlushBtn: UIButton!
    @IBOutlet  weak var onFlushBtn: UIButton!
    @IBOutlet  weak var offFlushBtn: UIButton!
    
    //MARK: - ------ VIDEO RECORDING --------
    
    @IBOutlet  var startTimerRecordLbl: UILabel!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet  weak var photoButton2: UIButton!
    @IBOutlet weak var settingContainerView: UIView!
    
    
    var sessionQualityArray : [SessionPresetModel] = []
    var sessionPhotoQualityArray : [SessionPresetModel] = []
    
    //MARK: - ------ VIDEO QUALITY GETTER & SETTER -------
    
    var videoQuality : String {
        set {
            UserDefaults.standard.set(newValue, forKey: "videoQuality")
            UserDefaults.standard.synchronize()
        }
        get {
            if (UserDefaults.standard.value(forKey: "videoQuality") == nil) {
                UserDefaults.standard.set(AVCaptureSession.Preset.high, forKey: "videoQuality")
                UserDefaults.standard.synchronize()
            }
            
            return UserDefaults.standard.value(forKey: "videoQuality") as? String ?? AVCaptureSession.Preset.high.rawValue
        }
    }
    
    //MARK: - ------ SCREEN BRIGHTNESS GETTER & SETTER -------
    
    var screenBrightness : Double {
        set{
            UserDefaults.standard.set(newValue, forKey: "brightness")
            UserDefaults.standard.synchronize()
        }
        get {
            if (UserDefaults.standard.value(forKey: "brightness") == nil) {
                UserDefaults.standard.set(0.7, forKey: "brightness")
                UserDefaults.standard.synchronize()
            }
            
            return UserDefaults.standard.value(forKey: "brightness") as? Double ?? 0.5
        }
    }
    
    //MARK: - ------ STABILIZATION GETTER & SETTER -------
    
    var stabilizationOnOffString : String {
        set {
            UserDefaults.standard.set(newValue, forKey: "stabilizationOnOff")
            UserDefaults.standard.synchronize()
        }
        get {
            if (UserDefaults.standard.value(forKey: "stabilizationOnOff") == nil) {
                UserDefaults.standard.set(AVCaptureSession.Preset.high, forKey: "stabilizationOnOff")
                UserDefaults.standard.synchronize()
            }
            
            return UserDefaults.standard.value(forKey: "stabilizationOnOff") as? String ?? AVCaptureSession.Preset.high.rawValue
        }
    }
    
    //MARK: ------- VOLUME UP GETTER & SETTER --------
    
    var volumeUpDown : Float? {
        set {
            UserDefaults.standard.set(newValue, forKey: "volume")
            UserDefaults.standard.synchronize()
        }
        get {
            if (UserDefaults.standard.value(forKey: "volume") == nil) {
                UserDefaults.standard.set(1.0, forKey: "volume")
                UserDefaults.standard.synchronize()
            }
            
            return UserDefaults.standard.value(forKey: "volume") as? Float
        }
    }
    
    //MARK: -------  FIRST TIME GETTER & SETTER --------
    
    var isFirstTime : Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: "firsttime")
            UserDefaults.standard.synchronize()
        }
        get {
            if (UserDefaults.standard.value(forKey: "firsttime") == nil) {
                UserDefaults.standard.set(AVCaptureSession.Preset.photo, forKey: "firsttime")
                UserDefaults.standard.synchronize()
            }
            
            return UserDefaults.standard.value(forKey: "firsttime") as? Bool ?? true
        }
    }
    
    var photoQuality : String {
        set {
            UserDefaults.standard.set(newValue, forKey: "photoQuality")
            UserDefaults.standard.synchronize()
        }
        get {
            if (UserDefaults.standard.value(forKey: "photoQuality") == nil) {
                UserDefaults.standard.set(AVCaptureSession.Preset.photo, forKey: "photoQuality")
                UserDefaults.standard.synchronize()
            }
            
            return UserDefaults.standard.value(forKey: "photoQuality") as? String ?? AVCaptureSession.Preset.photo.rawValue
        }
    }
    
    var captureModeTag = 0
    
    
    //MARK: ------- PURHCASE --------
    
    @IBOutlet weak var purchaseButtonOutlet: UIButton!
    @IBOutlet weak var videoQualityCollectionView: UICollectionView!
    @IBOutlet weak var photoQualityCollectionView: UICollectionView!
    @IBOutlet  weak var waterMarkField: UITextField!
    @IBOutlet weak var previewView: AVCamManualPreviewView!
    @IBOutlet weak var cameraUnavailableLabel: UILabel!
    
    //MARK: ------- BOTTOM BAR --------
    
    @IBOutlet weak var burstShotView: RoundEdgesButton!
    @IBOutlet weak var picturecountAndAfAeLabel: UILabel!
    @IBOutlet weak var pictureActivityIndicatorView: NVActivityIndicatorView!
    @IBOutlet weak var resumeButton: UIButton!
    @IBOutlet weak var recordButton: KYShutterButton!
    @IBOutlet weak var videoModeButton: UIButton!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var photoModeButton: UIButton!
    
    //MARK: ------- ISO LENS EXPOSURE --------
    
    @IBOutlet weak var HUDButton: UIButton!
    @IBOutlet weak var manualSegments: UISegmentedControl! //###
    @IBOutlet weak var manualHUD: UIView!
    var focusModes: [AVCaptureDevice.FocusMode] = []
    @IBOutlet weak var manualHUDFocusView: UIView!
    @IBOutlet weak var focusModeControl: UISegmentedControl!
    @IBOutlet weak var lensPositionSlider: UISlider! {
        didSet {
            lensPositionSlider.setThumbImage(UIImage(named: "Circle"), for: .normal)
        }
    }
    
    @IBOutlet weak var lensPositionNameLabel: UILabel!
    @IBOutlet weak var lensPositionValueLabel: UILabel!
    let exposureModes: [AVCaptureDevice.ExposureMode] = [.continuousAutoExposure, .custom]
    @IBOutlet weak var manualHUDExposureView: UIView!
    @IBOutlet weak var exposureModeControl: UISegmentedControl!
    @IBOutlet weak var exposureDurationSlider: UISlider! {
        didSet {
            exposureDurationSlider.setThumbImage(UIImage(named: "Circle"), for: .normal)
        }
    }
    
    @IBOutlet weak var exposureDurationNameLabel: UILabel!
    @IBOutlet weak var exposureDurationValueLabel: UILabel!
    @IBOutlet weak var ISOSlider: UISlider! {
        didSet {
            ISOSlider.setThumbImage(UIImage(named: "Circle"), for: .normal)
        }
    }
    @IBOutlet weak var ISONameLabel: UILabel!
    @IBOutlet weak var ISOValueLabel: UILabel!
    @IBOutlet weak var exposureTargetBiasSlider: UISlider! {
        didSet {
            exposureTargetBiasSlider.setThumbImage(UIImage(named: "Circle"), for: .normal)
        }
    }
    @IBOutlet weak var exposureTargetBiasNameLabel: UILabel!
    @IBOutlet weak var exposureTargetBiasValueLabel: UILabel!
    @IBOutlet weak var exposureTargetOffsetSlider: UISlider! {
        didSet {
            
            
            exposureTargetOffsetSlider.setThumbImage(UIImage(named: "Circle"), for: .normal)
        }
    }
    @IBOutlet weak var exposureTargetOffsetNameLabel: UILabel!
    @IBOutlet weak var exposureTargetOffsetValueLabel: UILabel!
    private var whiteBalanceModes: [AVCaptureDevice.WhiteBalanceMode] = []
    @IBOutlet weak var manualHUDWhiteBalanceView: UIView!
    @IBOutlet weak var whiteBalanceModeControl: UISegmentedControl!
    @IBOutlet weak var temperatureSlider: UISlider! {
        didSet {
            temperatureSlider.setThumbImage(UIImage(named: "Circle"), for: .normal)
        }
    }
    @IBOutlet weak var temperatureNameLabel: UILabel!
    @IBOutlet weak var temperatureValueLabel: UILabel!
    @IBOutlet weak var tintSlider: UISlider! {
        didSet {
            tintSlider.setThumbImage(UIImage(named: "Circle"), for: .normal)
        }
    }
    @IBOutlet weak var tintNameLabel: UILabel!
    @IBOutlet weak var tintValueLabel: UILabel!
    @IBOutlet weak var rifleView: UIView!
    @IBOutlet weak var binocularView: UIView!
    @IBOutlet weak var manualHUDLensStabilizationView: UIView!
    @IBOutlet weak var lensStabilizationControl: UISegmentedControl!
    
    //MARK: ------- RAW & JPG --------
    
    @IBOutlet weak var rawControl: UISegmentedControl!
    @IBOutlet weak var rawPhotoButton: UIButton!
    @IBOutlet weak var jpgPhotoButton: UIButton!
    @IBOutlet weak var jpgRawButtonOutlet: UIButton!
    @IBOutlet weak var mirrorScrollView: UIScrollView!
    @IBOutlet weak var mirrorView: UIView!
    
    //MARK: ------- SESSION MANANGEMENT --------
    var sessionQueue: DispatchQueue!
    @objc dynamic var session: AVCaptureSession!
    @objc dynamic var videoDeviceInput: AVCaptureDeviceInput?
    fileprivate var videoDeviceDiscoverySession: AVCaptureDeviceDiscoverySessionType?
    @objc dynamic var videoDevice: AVCaptureDevice?
    let audioSession = AVAudioSession.sharedInstance()
    @objc dynamic var movieFileOutput: AVCaptureMovieFileOutput?
    var videoDataOutput : AVCaptureVideoDataOutput?
    
    
    @objc dynamic weak var photoOutput: AVCapturePhotoOutput?
    @objc dynamic var stillImageOutput: AVCaptureStillImageOutput? //### iOS < 10.0
    private var inProgressPhotoCaptureDelegates: [Int64: AVCamManualPhotoCaptureDelegate] = [:]
    
    // Utilities.
    private var setupResult: AVCamManualSetupResult = .success
    private var isSessionRunning: Bool = false
    var backgroundRecordingID: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    let kExposureDurationPower = 5.0 // Higher numbers will give the slider more sensitivity at shorter durations
    let kExposureMinimumDuration = 1.0/1000 // Limit exposure duration to a useful range
    
    //MARK: ------- SLOW MOTION --------
    var defaultFormat : AVCaptureDevice.Format!
    var defaultVideoMaxFrameDuration : CMTime!
    var desiredFps: Float64 = 0.0
    
    //MARK: ------- TIME LAPSE --------
    let recordingQueue = DispatchQueue(label: "com.takecian.RecordingQueue", attributes: [])
    var currentSkipCount = 0
    var lasttimeStamp = CMTimeMake(0, 0)
    var isTimeLapsCapturing = false
    
    let skipFrameSize = 5
    var videoWriter : TimeLapseVideoWriter?
    var heightVideo = 0
    var widthVideo  = 0
    
    var heightFilter = 0
    var widthFilter  = 0
    @IBOutlet weak var timeLapsButton: KYShutterButton!
    
    var timeLapsFilePath: String {
        get {
            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let documentsDirectory = paths[0] as String
            let filePath : String = "\(documentsDirectory)/video.mov"
            return filePath
        }
    }
    
    var timeLapsFilePathUrl: URL {
        get {
            return URL(fileURLWithPath: timeLapsFilePath)
        }
    }
    
    @IBOutlet weak var leftCenterView: UIView!
    @IBOutlet weak var thumbnailBtn: UIButton!
    
    let backgroundQueue = DispatchQueue(label: "com.app.queue", qos: .background)
    let photoManager = PHImageManager.default()
    
    
    //MARK: ------- FILTER OUTLETS --------
    
    var isFilterPhoto = false
    var filterCounter = -1
    var _glkViewController : GLKViewController?
    @IBOutlet weak var _glkView: GLKView!
    var ciContext: CIContext?
    var eaglContext: EAGLContext?
    var currentFilter : CIFilter?
    var filterdModelArray : [CIFilterModel] = []
    @IBOutlet weak var filterCollectionView: UICollectionView!
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBOutlet weak var timeLapsContentWidth: NSLayoutConstraint!
    @IBOutlet weak var videoSVcontentwidth: NSLayoutConstraint!
    @IBOutlet weak var photoSVcontentwidth: NSLayoutConstraint!
    @IBOutlet weak var photoSvWith: NSLayoutConstraint!
    
    
    //MARK: - -------- UIVIEWCONTROLLER - METHODS ---------
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        CoreDataVideos.shared.rifleVideos = dataHandler.getSaveVideoPath()
        getPaymentInformation()
        setIntroButtons()
        getValuesForRifle ()
        updateCurrentLocation()
        SVProgressHUD.setDefaultMaskType(.clear)
        //  NVActivityIndicatorView(frame: self.view.frame, type: NVActivityIndicatorType() NVActivityIndicatorType(rawValue: 29), color: UIColor.yellow, padding: 2)
        
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.black
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.white
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.donePicker(sender:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.cancelPicker(sender:)) )
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        turnOffTrainingModeOutlet.alpha = 1.0
        turnOffTrainingModeOutlet.layer.borderWidth = 1
        turnOffTrainingModeOutlet.layer.borderColor = UIColor.white.cgColor
        turnOffTrainingModeOutlet.layer.cornerRadius = 5
        turnOffTrainingModeOutlet.clipsToBounds = true
        
        
        
        
        picker.delegate = self
        picker.dataSource = self
        rifleSettingsTextField.inputView = picker
        rifleSettingsTextField.inputView?.autoresizingMask = UIViewAutoresizing.flexibleHeight
        rifleSettingsTextField.inputAccessoryView = toolBar
        print(previewView.frame.size.height )
        print(self.view.layer.frame.size.height)
        volumeView.frame = CGRect(x: 2000, y: -20, width: 0, height: 0);
        self.view.addSubview(volumeView)
        self.view.bringSubview(toFront: coverView)
        
        
        rotationSlider.transform = CGAffineTransform.init(rotationAngle:-CGFloat.pi/2)
        isoSlider.transform = CGAffineTransform.init(rotationAngle:-CGFloat.pi/2)
        focusSlider.transform = CGAffineTransform.init(rotationAngle:-CGFloat.pi/2)
        lensPositionSlider.transform = CGAffineTransform.init(rotationAngle:-CGFloat.pi/2)
        
        ISOSlider.transform = CGAffineTransform.init(rotationAngle:-CGFloat.pi/2)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(AVCamManualCameraViewController.rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
        self.isoSlider.minimumValue = self.videoDevice?.activeFormat.minISO ?? 0.0
        self.isoSlider.maximumValue = self.videoDevice?.activeFormat.maxISO ?? 0.0
        self.isoSlider.value = self.videoDevice?.iso ?? 0.0
        
        
        
        if stabilizationOnOffString == nil || stabilizationOnOffString == "" {
            
        }else{
            if stabilizationOnOffString == "on"
            {
                stabilizationOnnButton.setTitleColor(UIColor.yellow, for: .normal)
                
            }else if  stabilizationOnOffString == "off"{
                
                stabilizationOffButton.setTitleColor(UIColor.yellow, for: .normal)
                
            }
        }
        
        loadPlistFileOfSessionPreset()
        
        DispatchQueue.main.async {
            self.loadLatestPHAsset()
        }
        
        
        self.filterCollectionView.delegate = self
        self.filterCollectionView.dataSource = self
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        
        
        LocationService.sharedInstance.delegate = self
        
        if let _ = UserDefaults.standard.value(forKey: UserDefaultKey.location.rawValue) as? NSDictionary {
            AppDelegate.IS_GEO_TAG_ON = true
            LocationService.sharedInstance.startUpdatingLocation()
        } else {
            AppDelegate.IS_GEO_TAG_ON = false
            LocationService.sharedInstance.stopUpdatingLocation()
        }
        
        // Disable UI. The UI is enabled if and only if the session starts running.
        //  self.cameraButton.isEnabled = false
        self.recordButton.isEnabled = false
        self.photoMenuSV.isHidden = true
        self.videoMenu.isHidden = true
        self.waterMarkView.isHidden = true
        self.fileTypeView.isHidden = true
        
        self.stabilizationView.isHidden = true
        self.geoTagView.isHidden = true
        self.photoQualityCollectionView.isHidden = true
        self.videoQualityCollectionView.isHidden = true
        
        
        videoMenu.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        mirrorView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        topContainerSubTimerView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        waterMarkView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        bottomContainerView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        manualHUDFocusView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        manualHUDExposureView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        geoTagView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        stabilizationView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        fileTypeView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        photoMenuSV.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        photoQualityCollectionView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        videoQualityCollectionView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        zoomLabel.layer.cornerRadius = zoomLabel.frame.width/2
        zoomLabel.layer.masksToBounds = true
        zoomLabel.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        zoomLabel.isHidden = true
        // Top bar actions
        switchCameraPhotoTop.addTarget(self, action:#selector(chooseNewCamera(_:)), for: .touchUpInside)
        
        let flashGesture = UITapGestureRecognizer(target: self, action: #selector(self.flushViewAppearFunction))
        photFlashView.isUserInteractionEnabled = true
        photFlashView.addGestureRecognizer(flashGesture)
        
        
        let rotateGesture = UIRotationGestureRecognizer(target: self, action: #selector(self.rotateGesture))
        
        
        let TimerTap = UITapGestureRecognizer(target: self, action: #selector(self.timerViewAppearFunction))
        timerLabel.isUserInteractionEnabled = true
        timerLabel.addGestureRecognizer(TimerTap)
        
        let WaterMarkTap = UITapGestureRecognizer(target: self, action: #selector(self.waterMarkViewAppear))
        waterMarkLabel.isUserInteractionEnabled = true
        waterMarkLabel.addGestureRecognizer(WaterMarkTap)
        
        let geoTagTap = UITapGestureRecognizer(target: self, action: #selector(self.geotagviewAppear))
        geoTaggingLabel.isUserInteractionEnabled = true
        geoTaggingLabel.addGestureRecognizer(geoTagTap)
        
        let fileTypeTap = UITapGestureRecognizer(target: self, action: #selector(self.fileTypeViewAppear))
        fileTypeLabel.isUserInteractionEnabled = true
        fileTypeLabel.addGestureRecognizer(fileTypeTap)
        
        let timeLapseTap = UITapGestureRecognizer(target: self, action: #selector(self.setTimeLapse))
        timeLapsLabel.isUserInteractionEnabled = true
        timeLapsLabel.addGestureRecognizer(timeLapseTap)
        
        let slomotionTap = UITapGestureRecognizer(target: self, action: #selector(self.setSlowMotion))
        slowMoLabel.isUserInteractionEnabled = true
        slowMoLabel.addGestureRecognizer(slomotionTap)
        
        let f4kTap = UITapGestureRecognizer(target: self, action: #selector(self.set4kFormat))
        f4kVideoLabel.isUserInteractionEnabled = true
        f4kVideoLabel.addGestureRecognizer(f4kTap)
        if Device.IS_4_INCHES_OR_SMALLER() {
            f4kVideoLabel.textColor = UIColor.white
            rawPhotoButton.isUserInteractionEnabled = false
            jpgRawButtonOutlet.isUserInteractionEnabled = false
        }else{
            f4kVideoLabel.textColor = UIColor.yellow
        }
        
        
        self.manualHUDFocusView.isHidden = true
        self.manualHUDExposureView.isHidden = true
        
        
        // Create the AVCaptureSession.
        self.session = AVCaptureSession()
        
        // Create a device discovery session
        if #available(iOS 10.0, *) {
            let deviceTypes: [AVCaptureDevice.DeviceType] = [.builtInWideAngleCamera, .builtInDuoCamera, .builtInTelephotoCamera]
            self.videoDeviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: deviceTypes, mediaType: AVMediaType.video, position: .unspecified) as? AVCaptureDeviceDiscoverySessionType
        }
        
        // Setup the preview view.
        self.previewView.session = self.session
        
        // Communicate with the session and other session objects on this queue.
        self.sessionQueue = DispatchQueue(label: "session queue", attributes: [])
        self.setupResult = .success
        
        // Check video authorization status. Video access is required and audio access is optional.
        // If audio access is denied, audio is not recorded during movie recording.
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .authorized:
            // The user has previously granted access to the camera.
            break
        case .notDetermined:
            // The user has not yet been presented with the option to grant video access.
            // We suspend the session queue to delay session running until the access request has completed.
            // Note that audio access will be implicitly requested when we create an AVCaptureDeviceInput for audio during session setup.
            self.sessionQueue.suspend()
            AVCaptureDevice.requestAccess(for: AVMediaType.video) {granted in
                if !granted {
                    self.setupResult = .cameraNotAuthorized
                }
                self.sessionQueue.resume()
            }
        default:
            // The user has previously denied access.
            self.setupResult = .cameraNotAuthorized
        }
        
        // Setup the capture session.
        // In general it is not safe to mutate an AVCaptureSession or any of its inputs, outputs, or connections from multiple threads at the same time.
        // Why not do all of this on the main queue?
        // Because -[AVCaptureSession startRunning] is a blocking call which can take a long time. We dispatch session setup to the sessionQueue
        // so that the main queue isn't blocked, which keeps the UI responsive.
        
        self.sessionQueue.async {
            self.configureSession()
        }
        
        self.addFilterImageToArray()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //   AppUtility.lockOrientation(.portrait)
        let rifleVideoArray = dataHandler.getSaveVideoPath()
        if rifleVideoArray.count != 0 {
            self.startOrStopAnimatingIndicator()
            if !CheckVideos.shared.isProcessing {
                CheckVideos.shared.getVideo()
            }
            
        }else{
            self.startOrStopAnimatingIndicator()
        }
        
        CheckVideos.shared.delegate = self
        if Product.purchased{
            purchaseButtonOutlet.isHidden = true
        }
        
        notificationHandler.addObserver(self, selector: #selector(AVCamManualCameraViewController.volumeChanged(_:)), name: NSNotification.Name("AVSystemController_SystemVolumeDidChangeNotification"), object: nil)
        
        self.sessionQueue.async {
            switch self.setupResult {
            case .success:
                // Only setup observers and start the session running if setup succeeded.
                self.addObservers()
                self.session.startRunning()
                self.isSessionRunning = self.session.isRunning
            case .cameraNotAuthorized:
                self.messageAccessNotGiven(title : "Camera Permission required", message : "In order you this application you need to give camera permission.")
            case .sessionConfigurationFailed:
                break;
                //                DispatchQueue.main.async {
                //                    let message = NSLocalizedString("Unable to capture media", comment: "Alert message when something goes wrong during capture session configuration")
                //                    let alertController = UIAlertController(title: "AVCamManual", message: message, preferredStyle: .alert)
                //                    let cancelAction = UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil)
                //                    alertController.addAction(cancelAction)
                //                    self.present(alertController, animated: true, completion: nil)
                //                }
            }
        }
        showIntroButtons()
        selectedScopeIntroButtons()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.view.bringSubview(toFront: selectScopeView)
        isAudioPermissionGranted = checkAudioDevicePermission ()
        if isFirstTime  {
            showTrainingModeModal()
            isFirstTime = false
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.sessionQueue.async {
            if self.setupResult == .success {
                self.session.stopRunning()
                self.removeObservers()
            }
        }
        
        notificationHandler.removeObserver(self)
        
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        var text=""
        switch UIDevice.current.orientation{
        case .portrait:
            text="Portrait"
        case .portraitUpsideDown:
            text="PortraitUpsideDown"
        case .landscapeLeft:
            rotated()
        case .landscapeRight:
            rotated()
        default:
            text="Another"
        }
        NSLog("You have moved: \(text)")
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        let deviceOrientation = UIDevice.current.orientation
        
        if UIDeviceOrientationIsLandscape(deviceOrientation) {
            
            rotateButton2.isHidden = true
            rotateButton1.isHidden = false
            
        }else{
            rotateButton2.isHidden = false
            rotateButton1.isHidden = true
        }
        
        if UIDeviceOrientationIsPortrait(deviceOrientation) || UIDeviceOrientationIsLandscape(deviceOrientation) {
            
            guard let previewLayer = self.previewView.layer as? AVCaptureVideoPreviewLayer else{return}
            //previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
            
            previewLayer.connection!.videoOrientation = AVCaptureVideoOrientation(rawValue: deviceOrientation.rawValue)!
            filterCollectionView!.collectionViewLayout.invalidateLayout()
            rotated()
        }
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.allButUpsideDown
    }
    
    override var shouldAutorotate : Bool {
        // Disable autorotation of the interface when recording is in progress.
        return !(self.movieFileOutput?.isRecording ?? false);
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    //MARK: - -------- UIPICKERCONTROLLER DELEGATE & DATSOURCE ---------
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return rifleValue.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return rifleValue[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if rifleValue.count != 0{
            rifleValue1 = rifleValue[row]
        }
        
    }
    
    
    //MARK: - ------ UITEXTFIELD METHODS -------
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.resignFirstResponder()
        return false
    }
    
    
    
    
    //MARK: - ------ UISLIDER ACTIONS -------
    
    @IBAction func sliderValueChanged(sender: UISlider) {
        var currentValue = Int(sender.value)
        if sender.tag == 71{
            let sliderValue:Double = Double(sender.value)
            slideVal = sliderValue
            
            
            var transform = CGAffineTransform(scaleX: -1, y: 1)
            let angle = CGFloat(Double(sliderValue) * 2.0 * (Double.pi / Double(sender.maximumValue)))
            angleValue = Double(angle)
            transform = transform.rotated(by:angle)
            zoomLabel.isHidden = false
            let value = "\(String(format: "%d", Int((Double(sliderValue) * 2.0 * (180 / Double(sender.maximumValue))))))&deg;"
            zoomLabel.text = value.replacingOccurrences(of: "&deg;", with: "\u{00B0}")
            zoomLabelTimer =  Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(hideZoomLabel), userInfo: nil, repeats: false)
            previewView.transform = transform
            _glkView.transform = transform
            
        }else if sender.tag == 72{
            do {
                var val2 = 0.00001
                sender.minimumValue = self.videoDevice?.activeFormat.minISO ?? 0.0
                sender.maximumValue = self.videoDevice?.activeFormat.maxISO ?? 0.0
                // sender.value = self.videoDevice?.iso ?? 0.0
                
                print("iso slider value   :%7f",Double(sender.value/(sender.maximumValue)))
                if Double(sender.value/(sender.maximumValue)) == 0.03125
                {
                    val2 = 0.000001
                }else if Double(sender.value/(sender.maximumValue)) == 0.036184{
                    val2 = 0.000001
                }else{
                    val2 = Double(sender.value/(sender.maximumValue) )
                }
                
                
                let p = pow((val2), kExposureDurationPower) // Apply power function to expand slider's (low-end range
                let minDurationSeconds = max(CMTimeGetSeconds(self.videoDevice!.activeFormat.minExposureDuration), kExposureMinimumDuration)
                let maxDurationSeconds = CMTimeGetSeconds(self.videoDevice!.activeFormat.maxExposureDuration)
                let newDurationSeconds = p * ( 0.11111 - minDurationSeconds ) + minDurationSeconds; // Scale from 0-1 slider range to actual duration
                
                print("min duration  : \(minDurationSeconds)")
                print("max duration  : \(maxDurationSeconds)")
                print("new duration  : \(newDurationSeconds)")
                do {
                    
                    try self.videoDevice!.lockForConfiguration()
                    
                    var val = Double(round(sender.value))
                    print(val)
                    self.videoDevice!.setExposureModeCustom(duration: AVCaptureDevice.currentExposureDuration, iso: Float(val), completionHandler: nil)
                    
                    self.videoDevice?.setExposureModeCustom(duration: CMTimeMakeWithSeconds(newDurationSeconds, 1000*1000*1000), iso: AVCaptureDevice.currentISO, completionHandler: nil)
                    self.videoDevice!.unlockForConfiguration()
                } catch let error {
                    NSLog("Could not lock device for configuration: \(error)")
                }
                
                
            } catch let error {
                NSLog("Could not lock device for configuration: \(error)")
            }
            
        }else if sender.tag == 73{
            do {
                try self.videoDevice!.lockForConfiguration()
                self.videoDevice!.setFocusModeLocked(lensPosition: sender.value, completionHandler: nil)
                self.videoDevice!.unlockForConfiguration()
            } catch let error {
                NSLog("Could not lock device for configuration: \(error)")
            }
        }
        
        
    }
    
    // MARK : - SliderButtons
    
    @IBAction func upSliderAction(_ sender: Any) {
        let oldValue = rotationSlider.value
        let value360 = rotationSlider.maximumValue / 360
        if oldValue < rotationSlider.maximumValue {
            let newValue = oldValue + value360
            rotationSlider.value = newValue
            sliderValueChanged(sender: rotationSlider)
        }
    }
    
    
    @IBAction func downSliderAction(_ sender: Any) {
        let oldValue = rotationSlider.value
        let value360 = rotationSlider.maximumValue / 360
        if oldValue > rotationSlider.minimumValue {
            let newValue = oldValue - value360
            rotationSlider.value = newValue
            sliderValueChanged(sender: rotationSlider)
        }
    }
    
    
    @objc func rotateGesture(sender: UIRotationGestureRecognizer){
        
        var angle1 = atan2f(Float( (sender.view?.transform.b)!), Float( (sender.view?.transform.a)!))
        angle1 = Float(Double(angle1) * (180 / M_PI))
        
        sender.view?.transform = (sender.view?.transform)!.rotated(by: sender.rotation)
        sender.rotation = 0
        var angle = atan2f(Float((sender.view?.transform.b)!), Float( (sender.view?.transform.a)!))
        angle = Float(Double(angle) * (180 / M_PI))
        print(angle)
        
        if angle1 < angle{
            print("greater")
            
        }else{
            print("less")
        }
        zoomLabel.isHidden = false
        let value = "\(String(format: "%d", Int(angle)))&deg;"
        zoomLabel.text = value.replacingOccurrences(of: "&deg;", with: "\u{00B0}")
        zoomLabelTimer =  Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(hideZoomLabel), userInfo: nil, repeats: false)
        
        
        print("rotate gesture")
    }
    
    
    
    
    func geotagviewAppear() {
        hideViews(view: geoTagView)
        
        if AppDelegate.IS_GEO_TAG_ON {
            geoTagOnnButton.setTitleColor(UIColor.yellow, for: UIControlState.normal)
            geoTagOffButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        }else {
            geoTagOnnButton.setTitleColor(UIColor.white, for: UIControlState.normal)
            geoTagOffButton.setTitleColor(UIColor.yellow, for: UIControlState.normal)
        }
        
    }
    
    
    
    
    func hideViews(view:UIView) {
        
        self.manualHUDExposureView.isHidden = true
        self.manualHUDFocusView.isHidden = true
        // self.settingContainerView.isHidden = true
        self.waterMarkView.isHidden = true
        self.fileTypeView.isHidden = true
        self.stabilizationView.isHidden = true
        self.geoTagView.isHidden = true
        self.mirrorView.isHidden = true
        self.filterCollectionView.isHidden = true
        //  self.mirrorScrollView.isHidden = true
        self.topContainerSubTimerView.isHidden = true
        self.topContainerSubFlushView.isHidden = true
        self.videoQualityCollectionView.isHidden = true
        self.photoQualityCollectionView.isHidden = true
        if photoMenuSV.isHidden && mirrorScrollView.isHidden{
            purchaseButtonOutlet.alpha = 0.5
        }else{
            purchaseButtonOutlet.alpha = 1.0
        }
        
        view.isHidden = false
        
    }
    
    func setTelephoto(sender:UIButton)
    {
        
        self.hideViews(view:bottomContainerView)
        
        if isTelephoto {
            
            
            
            let device = AVCaptureDevice.default(.builtInWideAngleCamera, for:AVMediaType.video, position: .unspecified)
            
            
            let videoDeviceInput: AVCaptureDeviceInput
            
            do {
                videoDeviceInput = try AVCaptureDeviceInput(device:device!)
            } catch {
                NSLog("Could not create video device input: \(error)")
                self.setupResult = .sessionConfigurationFailed
                self.session.commitConfiguration()
                return
            }
            self.session.beginConfiguration()
            self.session.removeInput(self.videoDeviceInput!)
            self.session.addInput(videoDeviceInput)
            self.videoDeviceInput = videoDeviceInput
            self.videoDevice = device
            // self.session.input
            self.session.commitConfiguration()
            sender.setImage(UIImage(named: "telecame"),for: .normal);
            
            isTelephoto = false
            //          self.changeCaptureMode(self.videoModeButton)
            
            
        }
        else {
            ////////////
            sender.setImage(UIImage(named: "wideangle"),for: .normal);
            
            if let device = AVCaptureDevice.default(.builtInTelephotoCamera, for:AVMediaType.video, position: .unspecified) {
                
                let videoDeviceInput: AVCaptureDeviceInput
                
                do {
                    videoDeviceInput = try AVCaptureDeviceInput(device:device)
                } catch {
                    NSLog("Could not create video device input: \(error)")
                    self.setupResult = .sessionConfigurationFailed
                    self.session.commitConfiguration()
                    return
                }
                self.session.beginConfiguration()
                self.session.removeInput(self.videoDeviceInput!)
                self.session.addInput(videoDeviceInput)
                self.videoDeviceInput = videoDeviceInput
                self.videoDevice = device
                //                if captureModeTag == AVCamManualCaptureMode.movie.rawValue{
                //                    self.changeCaptureMode(self.videoModeButton)
                //                }
                self.session.commitConfiguration()
                isTelephoto = true
                
            }
            else {
                
                sender.setImage(UIImage(named: "wideangle"),for: .normal);
                EZAlertController.alert("Info!", message: "Telephoto camera not available in this phone")
            }
        }
        
    }
    
    
    
    
    
    //MARK: HUD
    
    private func configureManualHUD() {
        
        // Manual focus controls
        self.focusModes = [.continuousAutoFocus, .locked]
        
        self.focusModeControl.isEnabled = (self.videoDevice != nil)
        if let videoDevice = self.videoDevice {//###
            self.focusModeControl.selectedSegmentIndex = self.focusModes.index(of: videoDevice.focusMode)!
            for mode in self.focusModes {
                self.focusModeControl.setEnabled(videoDevice.isFocusModeSupported(mode), forSegmentAt: self.focusModes.index(of: mode)!)
            }
        }
        
        self.lensPositionSlider.minimumValue = 0.0
        self.lensPositionSlider.maximumValue = 1.0
        self.lensPositionSlider.value = self.videoDevice?.lensPosition ?? 0
        
        self.focusSlider.minimumValue = 0.0
        self.focusSlider.maximumValue = 1.0
        self.focusSlider.value = self.videoDevice?.lensPosition ?? 0
        
        // Manual exposure controls
        //self.exposureModes = [.continuousAutoExposure, .custom]
        
        
        self.exposureModeControl.isEnabled = (self.videoDevice != nil)
        if let videoDevice = self.videoDevice {
            self.exposureModeControl.selectedSegmentIndex = self.exposureModes.index(of: videoDevice.exposureMode)!
            for mode in self.exposureModes {
                self.exposureModeControl.setEnabled(videoDevice.isExposureModeSupported(mode), forSegmentAt: self.exposureModes.index(of: mode)!)
            }
        }
        
        // Use 0-1 as the slider range and do a non-linear mapping from the slider value to the actual device exposure duration
        self.exposureDurationSlider.minimumValue = 0
        self.exposureDurationSlider.maximumValue = 1
        let exposureDurationSeconds = CMTimeGetSeconds(self.videoDevice?.exposureDuration ?? CMTime())
        let minExposureDurationSeconds = max(CMTimeGetSeconds(self.videoDevice?.activeFormat.minExposureDuration ?? CMTime()), kExposureMinimumDuration)
        let maxExposureDurationSeconds = CMTimeGetSeconds(self.videoDevice?.activeFormat.maxExposureDuration ?? CMTime())
        // Map from duration to non-linear UI range 0-1
        let p = (exposureDurationSeconds - minExposureDurationSeconds) / (maxExposureDurationSeconds - minExposureDurationSeconds) // Scale to 0-1
        self.exposureDurationSlider.value = Float(pow(p, 1 / kExposureDurationPower)) // Apply inverse power
        self.exposureDurationSlider.isEnabled = (self.videoDevice != nil && self.videoDevice!.exposureMode == .custom)
        
        print("minISOValue ==== \(String(describing: self.videoDevice?.activeFormat.minISO))")
        print("maxISOValue ==== \(String(describing: self.videoDevice?.activeFormat.maxISO))")
        
        self.ISOSlider.minimumValue = self.videoDevice?.activeFormat.minISO ?? 0.0
        self.ISOSlider.maximumValue = self.videoDevice?.activeFormat.maxISO ?? 0.0
        self.ISOSlider.value = self.videoDevice?.iso ?? 0.0
        //changehere
        self.isoSlider.minimumValue = 0
        self.isoSlider.maximumValue = self.videoDevice?.activeFormat.maxISO ?? 0.0
        self.isoSlider.value = self.videoDevice?.iso ?? 0.0
        self.exposureTargetBiasSlider.minimumValue = self.videoDevice?.minExposureTargetBias ?? 0.0
        self.exposureTargetBiasSlider.maximumValue = self.videoDevice?.maxExposureTargetBias ?? 0.0
        self.exposureTargetBiasSlider.value = self.videoDevice?.exposureTargetBias ?? 0.0
        self.exposureTargetBiasSlider.isEnabled = (self.videoDevice != nil)
        
        // Manual white balance controls
        self.whiteBalanceModes = [.continuousAutoWhiteBalance, .locked]
        
        
    }
    
    private func set(_ slider: UISlider, highlight color: UIColor) {
        slider.tintColor = color
        
        if slider === self.lensPositionSlider {
            self.lensPositionNameLabel.textColor = slider.tintColor
            //  self.lensPositionValueLabel.textColor = slider.tintColor
        } else if slider === self.exposureDurationSlider {
            self.exposureDurationNameLabel.textColor = slider.tintColor
            self.exposureDurationValueLabel.textColor = slider.tintColor
        } else if slider === self.ISOSlider {
            self.ISONameLabel.textColor = slider.tintColor
            self.ISOValueLabel.textColor = slider.tintColor
        } else if slider === self.exposureTargetBiasSlider {
            self.exposureTargetBiasNameLabel.textColor = slider.tintColor
            self.exposureTargetBiasValueLabel.textColor = slider.tintColor
        } else if slider === self.temperatureSlider {
            self.temperatureNameLabel.textColor = slider.tintColor
            self.temperatureValueLabel.textColor = slider.tintColor
        } else if slider === self.tintSlider {
            self.tintNameLabel.textColor = slider.tintColor
            self.tintValueLabel.textColor = slider.tintColor
        }
    }
    
    @IBAction func sliderTouchBegan(_ slider: UISlider) {
        self.set(slider, highlight: UIColor(red: 0.0, green: 122.0/255.0, blue: 1.0, alpha: 1.0))
    }
    
    @IBAction func sliderTouchEnded(_ slider: UISlider) {
        self.set(slider, highlight: UIColor.yellow)
    }
    
    //MARK: -------- Session Management ----------
    
    // Should be called on the session queue
    private func configureSession() {
        guard self.setupResult == .success else {
            return
        }
        
        self.session.beginConfiguration()
        
        if self.session.canSetSessionPreset(AVCaptureSession.Preset(rawValue: photoQuality)) {
            self.session.sessionPreset = AVCaptureSession.Preset(rawValue: photoQuality)
        }else {
            self.session.sessionPreset = AVCaptureSession.Preset.photo
        }
        
        // Add video input
        let videoDevice: AVCaptureDevice!
        if #available(iOS 10.0, *) {
            videoDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for:AVMediaType.video, position: .unspecified)
            
            if videoDevice.position == .front {
                self.currentCameraPosition = .front
            }else if videoDevice.position == .back {
                self.currentCameraPosition = .rear
            }
        } else {
            videoDevice = AVCamManualCameraViewController.deviceWithMediaType(AVMediaType.video.rawValue, preferringPosition: .back )
            if videoDevice.position == .front {
                self.currentCameraPosition = .front
            }else if videoDevice.position == .back {
                self.currentCameraPosition = .rear
            }
        }
        
        let videoDeviceInput: AVCaptureDeviceInput
        
        do {
            videoDeviceInput = try AVCaptureDeviceInput(device:videoDevice)
        } catch {
            NSLog("Could not create video device input: \(error)")
            self.setupResult = .sessionConfigurationFailed
            self.session.commitConfiguration()
            return
        }
        
        if self.session.canAddInput(videoDeviceInput) {
            self.session.addInput(videoDeviceInput)
            self.videoDeviceInput = videoDeviceInput
            self.videoDevice = videoDevice
            
            // save the default format
            self.defaultFormat = self.videoDevice?.activeFormat;
            self.defaultVideoMaxFrameDuration = self.videoDevice?.activeVideoMaxFrameDuration;
            
            // FOCUS
            
            
            DispatchQueue.main.async {
                /*
                 Why are we dispatching this to the main queue?
                 Because AVCaptureVideoPreviewLayer is the backing layer for AVCamManualPreviewView and UIView
                 can only be manipulated on the main thread.
                 Note: As an exception to the above rule, it is not necessary to serialize video orientation changes
                 on the AVCaptureVideoPreviewLayer’s connection with other session manipulation.
                 
                 Use the status bar orientation as the initial video orientation. Subsequent orientation changes are
                 handled by -[AVCamManualCameraViewController viewWillTransitionToSize:withTransitionCoordinator:].
                 */
                
                let singleTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.focusAndExposeTap(tap:)))
                singleTapGesture.numberOfTapsRequired = 1
                singleTapGesture.delegate = self
                self.previewView.addGestureRecognizer(singleTapGesture)
                
                let singleTapGestureGLK = UITapGestureRecognizer(target: self, action: #selector(self.focusAndExposeTap(tap:)))
                singleTapGestureGLK.numberOfTapsRequired = 1
                singleTapGestureGLK.delegate = self
                self._glkView.addGestureRecognizer(singleTapGestureGLK)
                
                let longPressGesture = UILongPressGestureRecognizer( target: self, action: #selector(self.focusAndExposeLongPress(tap:)))
                longPressGesture.minimumPressDuration = 0.5
                longPressGesture.delegate = self
                self.previewView.addGestureRecognizer(longPressGesture)
                
                let longPressGestureGLK = UILongPressGestureRecognizer( target: self, action: #selector(self.focusAndExposeLongPress(tap:)))
                longPressGestureGLK.minimumPressDuration = 0.5
                self._glkView.addGestureRecognizer(longPressGestureGLK)
                
                self.longPressGesturePhoto = UILongPressGestureRecognizer( target: self, action: #selector(self.captureBurstShot(tap:)))
                self.longPressGesturePhoto!.minimumPressDuration = 0.5
                self.photoButton.addGestureRecognizer(self.longPressGesturePhoto!)
                
                
                
                let statusBarOrientation = UIApplication.shared.statusBarOrientation
                var initialVideoOrientation = AVCaptureVideoOrientation.portrait
                if statusBarOrientation != UIInterfaceOrientation.unknown {
                    initialVideoOrientation = AVCaptureVideoOrientation(rawValue: statusBarOrientation.rawValue)!
                }
                
                let previewLayer = self.previewView.layer as! AVCaptureVideoPreviewLayer
                
                
                previewLayer.frame = self.previewView.bounds
                
                previewLayer.connection!.videoOrientation = initialVideoOrientation
                previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                
                
                let PreviewViewZoomGesture = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchAction(sender:)))
                PreviewViewZoomGesture.delegate = self
                let glkZoomGesture = UIPinchGestureRecognizer(target: self, action: #selector(self.pinchAction(sender:)))
                glkZoomGesture.delegate = self
                self.previewView.addGestureRecognizer(PreviewViewZoomGesture)
                self._glkView.addGestureRecognizer(glkZoomGesture)
                
                self.eaglContext = EAGLContext(api: .openGLES2)
                self.ciContext = CIContext(eaglContext: self.eaglContext!)
                
                
                
                
                self._glkView?.context = self.eaglContext!;
                self._glkView?.isHidden = true
            }
        } else {
            NSLog("Could not add video device input to the session")
            self.setupResult = .sessionConfigurationFailed
            self.session.commitConfiguration()
            return
        }
        
       
            // Add audio input
            let audioDevice = AVCaptureDevice.default(for: AVMediaType.audio)
            let audioDeviceInput: AVCaptureDeviceInput!
            do {
                audioDeviceInput = try AVCaptureDeviceInput(device: audioDevice!)
            } catch let error {
                audioDeviceInput = nil
                NSLog("Could not create audio device input: \(error)")
            }
        
            isAudioPermissionGranted = checkAudioDevicePermission()
            if isAudioPermissionGranted  {
            if self.session.canAddInput(audioDeviceInput) {
                self.session.addInput(audioDeviceInput)
                
                DispatchQueue.main.async {[weak self] in
                    
                    
                    do {
                        try self?.audioSession.setActive(true)
                    } catch {
                        print("some error")
                    }
                    
                }
            } else {
                NSLog("Could not add audio device input to the session")
            }
        }
        //
        if #available(iOS 10.0, *) {
            // Add photo output
            let photoOutput = AVCapturePhotoOutput()
            if self.session.canAddOutput(photoOutput) {
                self.session.addOutput(photoOutput)
                self.photoOutput = photoOutput
                photoOutput.isHighResolutionCaptureEnabled = true
                
                self.inProgressPhotoCaptureDelegates = [:]
            } else {
                NSLog("Could not add photo output to the session")
                self.setupResult = .sessionConfigurationFailed
                self.session.commitConfiguration()
                return
            }
        } else {
            let movieFileOutput = AVCaptureMovieFileOutput()
            if self.session.canAddOutput(movieFileOutput) {
                self.session.addOutput(movieFileOutput)
                if let connection = movieFileOutput.connection(with: AVMediaType.video), connection.isVideoStabilizationSupported {
                    connection.preferredVideoStabilizationMode = .auto
                }
                self.movieFileOutput = movieFileOutput
            } else {
                NSLog("Could not add movie file output to the session")
                self.setupResult = .sessionConfigurationFailed
                self.session.commitConfiguration()
                return
            }
            
            let stillImageOutput = AVCaptureStillImageOutput()
            if self.session.canAddOutput(stillImageOutput) {
                self.session.addOutput(stillImageOutput)
                self.stillImageOutput = stillImageOutput
                self.stillImageOutput!.outputSettings = [AVVideoCodecKey : AVVideoCodecJPEG]
                self.stillImageOutput!.isHighResolutionStillImageOutputEnabled = true
                self.stillImageOutput!.automaticallyEnablesStillImageStabilizationWhenAvailable = true
            } else {
                NSLog("Could not add still image output to the session")
                self.setupResult = .sessionConfigurationFailed
                self.session.commitConfiguration()
                return
            }
        }
        
        // We will not create an AVCaptureMovieFileOutput when configuring the session because the AVCaptureMovieFileOutput does not support movie recording with AVCaptureSessionPresetPhoto
        self.backgroundRecordingID = UIBackgroundTaskInvalid
        
        self.session.commitConfiguration()
        
        DispatchQueue.main.async {
            self.configureManualHUD()
        }
    }
    
    func swipePreviewGestureFunction(_ recognizer : UISwipeGestureRecognizer)  {
        
        if captureModeTag == AVCamManualCaptureMode.photo.rawValue {
            swipeBothViewGestureFunction(recognizer)
        }
        
    }
    
    func swipeGKLGestureFunction(_ recognizer : UISwipeGestureRecognizer) {
        if captureModeTag == AVCamManualCaptureMode.photo.rawValue {
            swipeBothViewGestureFunction(recognizer)
        }
    }
    
    func swipeBothViewGestureFunction(_ recognizer : UISwipeGestureRecognizer) {
        if recognizer.direction == UISwipeGestureRecognizerDirection.right {
            print("Swipe Right")
            
            if self.filterCounter > -1 {
                self.filterCounter = self.filterCounter - 1
                
                if self.filterCounter == -1 {
                    self.removeVideoDataOutputFromSession()
                }else {
                    if let filter = filterdModelArray[filterCounter].filter {
                        self.currentFilter = filter
                    }else {
                        self.filterCounter = self.filterCounter - 1
                        self.currentFilter = filterdModelArray[filterCounter].filter
                    }
                }
            }
        }
        else if recognizer.direction == UISwipeGestureRecognizerDirection.left {
            print("Swipe Left")
            
            if filterCounter == -1 {
                self.filterModeFunction(nil)
            }
            
            if self.filterCounter < self.filterdModelArray.count - 1 {
                self.filterCounter = self.filterCounter + 1
                
                if let filter = filterdModelArray[filterCounter].filter {
                    self.currentFilter = filter
                }else {
                    self.filterCounter = self.filterCounter + 1
                    self.currentFilter = filterdModelArray[filterCounter].filter
                }
            }
        }
    }
    
    
    @IBAction func resumeInterruptedSession(_: AnyObject) {
        self.sessionQueue.async {
            // The session might fail to start running, e.g., if a phone or FaceTime call is still using audio or video.
            // A failure to start the session running will be communicated via a session runtime error notification.
            // To avoid repeatedly failing to start the session running, we only try to restart the session running in the
            // session runtime error handler if we aren't trying to resume the session running.
            self.session.startRunning()
            self.isSessionRunning = self.session.isRunning
            if !self.session.isRunning {
                DispatchQueue.main.async {
                    let message = NSLocalizedString("Unable to resume", comment: "Alert message when unable to resume the session running" )
                    let alertController = UIAlertController(title: "AVCamManual", message: message, preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil)
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            } else {
                DispatchQueue.main.async {
                    self.resumeButton.isHidden = true
                    
                    self.view.bringSubview(toFront: self.videoMenu)
                    self.bottomContainerView.bringSubview(toFront: self.photoModeButton)
                }
            }
        }
    }
    
    @IBAction func changeCaptureMode(_ captureModeControl: UIButton) {
        isAudioPermissionGranted = checkAudioDevicePermission ()
        if isAudioPermissionGranted {
            self.hideViews(view: bottomContainerView)
            self.photoMenuSV.isHidden = true
            self.videoMenu.isHidden = true
            
            captureModeTag = captureModeControl.tag
            
            
            if captureModeControl.tag == AVCamManualCaptureMode.photo.rawValue {
                self.recordButton.isEnabled = false
                switchCameraPhotoTop.isHidden = false
                previewView.transform = CGAffineTransform(scaleX: 1, y: 1)
                self.sessionQueue.async {
                    // Remove the AVCaptureMovieFileOutput from the session because movie recording is not supported with AVCaptureSessionPresetPhoto. Additionally, Live Photo
                    // capture is not supported when an AVCaptureMovieFileOutput is connected to the session.
                    if (self.movieFileOutput != nil) {
                        if (self.movieFileOutput?.isRecording)! {
                            DispatchQueue.main.async { [weak self] in
                                self?.movieFileOutput!.stopRecording()
                            }
                        }else {
                            self.session.beginConfiguration()
                            self.session.removeOutput(self.movieFileOutput!)
                            
                            if self.session.canSetSessionPreset(AVCaptureSession.Preset(rawValue: self.photoQuality)) {
                                self.session.sessionPreset = AVCaptureSession.Preset(rawValue: self.photoQuality)
                                
                            }else {
                                self.photoQuality = AVCaptureSession.Preset.photo.rawValue
                                self.session.sessionPreset = AVCaptureSession.Preset(rawValue: self.photoQuality)
                            }
                            
                            if self.currentFilter != nil, self.videoDataOutput != nil {
                                if self.session.canAddOutput(self.videoDataOutput!) {
                                    self.session.addOutput(self.videoDataOutput!)
                                }
                            }
                            
                            self.session.commitConfiguration()
                            self.movieFileOutput = nil
                        }
                    }else {
                        self.session.beginConfiguration()
                        self.session.sessionPreset = AVCaptureSession.Preset(rawValue: self.photoQuality)
                        self.session.commitConfiguration()
                    }
                    DispatchQueue.main.async {[weak self] in
                        
                        self?.ISOSlider.minimumValue = self?.videoDevice?.activeFormat.minISO ?? 0.0
                        self?.ISOSlider.maximumValue = self?.videoDevice?.activeFormat.maxISO ?? 0.0
                        self?.ISOSlider.value = self?.videoDevice?.iso ?? 0.0
                        self?.ISOValueLabel.text = String(Int((self?.ISOSlider.value)!))
                        
                        self?.isoSlider.minimumValue = self?.videoDevice?.activeFormat.minISO ?? 0.0
                        self?.isoSlider.maximumValue = self?.videoDevice?.activeFormat.maxISO ?? 0.0
                        self?.isoSlider.value = self?.videoDevice?.iso ?? 0.0
                        
                        self?.thumbnailBtn.isHidden = false
                        self?.recordButton.buttonState = .normal
                        self?.photoButton2.isHidden = true
                        //    self?.cameraButton.isHidden = false
                        
                        self?.timeLapsButton.isHidden = true
                        
                        
                        self?.photoButton.isEnabled = true
                        self?.photoButton.isHidden = false
                        
                        
                        
                        // self?.photoModeButton.isHidden = true
                        self?.recordButton.isHidden = true
                        if (self?.cameraIconFlag)! {
                            
                            self?.recordButton.isHidden = false
                            //  self?.videoModeButton.isHidden = true
                            self?.cameraIconFlag = false
                        }else{
                            self?.photoModeButton.isHidden = true
                            
                            self?.recordButton.isHidden = true
                            self?.videoModeButton.isHidden = false
                        }
                        
                        self?.view.bringSubview(toFront: (self?.photoMenu)!)
                        
                        self?.bottomContainerView.bringSubview(toFront: (self?.photoButton)!)
                        self?.bottomContainerView.bringSubview(toFront: (self?.videoModeButton)!)
                        
                        if self?.currentFilter != nil {
                            self?._glkView?.isHidden = false
                            self?.configureFilterRendering()
                            self?.isFilterPhoto = true
                        }
                        
                        self?._glkView.isHidden = true
                        self?.previewView.isHidden = false
                        self?.currentFilter = nil
                        
                    }
                }
                if self.selectedScope == true {
                    self.switchCameraPhotoTop.isHidden = true
                }else{
                    self.switchCameraPhotoTop.isHidden = false
                }
            } else if captureModeControl.tag == AVCamManualCaptureMode.movie.rawValue {
                if currentFilter != nil{
                    self._glkView?.isHidden = true
                    self.previewView.isHidden = false
                    removeVideoDataOutputFromSession()
                    currentFilter = nil
                    
                }
                
                self.sessionQueue.async {
                    
                    if isTimeLapsReady == false {
                        let movieFileOutput = AVCaptureMovieFileOutput()
                        
                        if self.session.canAddOutput(movieFileOutput) {
                            self.session.beginConfiguration()
                            self.session.addOutput(movieFileOutput)
                            
                            
                            if self.videoDataOutput != nil {
                                self.session.removeOutput(self.videoDataOutput!)
                            }
                            
                            print(self.videoQuality)
                            if self.session.canSetSessionPreset(AVCaptureSession.Preset(rawValue: self.videoQuality)) {
                                self.session.sessionPreset = AVCaptureSession.Preset(rawValue: self.videoQuality)
                                
                            }else {
                                self.videoQuality = AVCaptureSession.Preset.high.rawValue
                                self.session.sessionPreset = AVCaptureSession.Preset(rawValue: self.videoQuality)
                            }
                            // MARK: mirroredPhotos
                            print(self.selectedScope)
                            let connection = movieFileOutput.connection(with: AVMediaType.video)
                            
                            if self.selectedScope {
                                connection?.isVideoMirrored = true
                            }else{
                                self.previewView.transform = CGAffineTransform(scaleX: 1, y: 1)
                                connection?.isVideoMirrored = false
                                
                            }
                            
                            if connection?.isVideoStabilizationSupported ?? false {
                                connection?.preferredVideoStabilizationMode = .auto
                            }
                            self.session.commitConfiguration()
                            self.movieFileOutput = movieFileOutput
                        }
                    }else {
                        
                        if self.videoDataOutput != nil {
                            self.session.beginConfiguration()
                            self.session.removeOutput(self.videoDataOutput!)
                            self.session.commitConfiguration()
                        }
                        
                        let queue = DispatchQueue.global(qos: .default)
                        queue.async(execute: {() -> Void in
                            self.cofingureVideoDataOutput()
                        })
                    }
                    DispatchQueue.main.async { [weak self] in
                        
                        self?.photoModeButton.isHidden = false
                        self?.photoButton.isHidden = true
                        self?.videoModeButton.isHidden = true
                        
                        
                        self?.view.bringSubview(toFront: (self?.videoMenu)!)
                        
                        
                        if self?.currentFilter != nil {
                            self?._glkView?.isHidden = true
                        }
                        
                        if isTimeLapsReady {
                            self?.bottomContainerView.bringSubview(toFront: (self?.timeLapsButton)!)
                            self?.timeLapsButton.isHidden = false
                            self?.recordButton.isEnabled = false
                            self?.recordButton.isHidden = true
                            
                            self?.ISOSlider.minimumValue = self?.videoDevice?.activeFormat.minISO ?? 0.0
                            self?.ISOSlider.maximumValue = self?.videoDevice?.activeFormat.maxISO ?? 0.0
                            self?.ISOSlider.value = self?.videoDevice?.iso ?? 0.0
                            self?.ISOValueLabel.text = String(Int((self?.ISOSlider.value)!))
                            
                            self?.isoSlider.minimumValue = self?.videoDevice?.activeFormat.minISO ?? 0.0
                            self?.isoSlider.maximumValue = self?.videoDevice?.activeFormat.maxISO ?? 0.0
                            self?.isoSlider.value = self?.videoDevice?.iso ?? 0.0
                            
                        }else {
                            self?.timeLapsButton.isHidden = true
                            self?.recordButton.isEnabled = true
                            self?.recordButton.isHidden = false
                            self?.bottomContainerView.bringSubview(toFront: (self?.recordButton)!)
                            self?.switchFormat(withDesiredFPS: (self?.desiredFps)!)
                        }
                        
                        self?.bottomContainerView.bringSubview(toFront: (self?.photoModeButton)!)
                    }
                }
                self._glkView.isHidden = true
                self.previewView.isHidden = false
                currentFilter = nil
                if self.selectedScope == true {
                    self.sliderValueChanged(sender: (self.rotationSlider)!)
                    self.switchCameraPhotoTop.isHidden = true
                    // self.sliderValueChanged(sender: (self.isoSlider)!)
                }else{
                    self.switchCameraPhotoTop.isHidden = false
                }
            }
        }
    }
    
    //MARK: -------- Device Configuration ----------
    
    func chooseNewCamera(_: Any) {
        if #available(iOS 10.0, *) {
            chooseNewCamera()
        } else {
            changeCamera()
        }
    }
    
    @available(iOS 10.0, *)
    private func chooseNewCamera() {
        //        if isSlowMotion{
        //            isSlowMotion = false
        //            SVProgressHUD.show(withStatus:"Removing Slow motion for you.")
        //            desiredFps = 0.0
        //            removeMovieFileOutput()
        //            slowMoLabel.textColor = UIColor.white
        //        }
        //
        //
        //        if isTimeLapsReady {
        //            SVProgressHUD.show(withStatus:"Removing time-laps.")
        //            self.isTimeLapsReady = false
        //            rotationSlider.tintColor = UIColor.yellow
        //            rotationSlider.isUserInteractionEnabled = true
        //            desiredFps = 0.0
        //            removeMovieFileOutput()
        //        }
        
        let devices = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera, .builtInTelephotoCamera, .builtInWideAngleCamera], mediaType: .video, position: .unspecified)
        
        for device in devices.devices {
            print(device.localizedName)
            if selectedScope == true{
                if device.localizedName == "Front Camera"{
                    return
                }else{
                    
                }
            }
            if self.videoDevice?.position == .front {
                if device.localizedName == "Back Camera" {
                    self.changeCameraWithDevice(device)
                    isoLeftViewButton.isHidden = false
                    focusLeftViewButton.isHidden = false
                    break
                }
            }else if self.videoDevice?.position == .back {
                if device.localizedName == "Front Camera" {
                    isoLeftViewButton.isHidden = true
                    focusLeftViewButton.isHidden = true
                    ISOSlider.isHidden = true
                    lensPositionSlider.isHidden = true
                    focusImageView.isHidden = true
                    self.changeCameraWithDevice(device)
                    
                    break
                }
            }
        }
    }
    
    private func changeCameraWithDevice(_ newVideoDevice: AVCaptureDevice) {
        // Check if device changed
        if newVideoDevice === self.videoDevice {
            return
        }
        
        //self.manualHUD.isUserInteractionEnabled = false
        //  self.cameraButton.isEnabled = false
        self.recordButton.isEnabled = false
        self.photoButton.isEnabled = false
        //self.captureModeControl.isEnabled = false
        //self.HUDButton.isEnabled = false
        
        self.sessionQueue.async {
            let newVideoDeviceInput = try! AVCaptureDeviceInput(device: newVideoDevice)
            
            self.session.beginConfiguration()
            
            // Remove the existing device input first, since using the front and back camera simultaneously is not supported
            self.session.removeInput(self.videoDeviceInput!)
            if self.session.canAddInput(newVideoDeviceInput) {
                NotificationCenter.default.removeObserver(self, name: .AVCaptureDeviceSubjectAreaDidChange, object: self.videoDevice)
                
                NotificationCenter.default.addObserver(self, selector: #selector(self.subjectAreaDidChange), name: .AVCaptureDeviceSubjectAreaDidChange, object: newVideoDevice)
                
                self.session.addInput(newVideoDeviceInput)
                self.videoDeviceInput = newVideoDeviceInput
                self.videoDevice = newVideoDevice
            } else {
                self.session.addInput(self.videoDeviceInput!)
            }
            
            if self.videoDevice?.position == .front {
                self.currentCameraPosition = .front
            }else if self.videoDevice?.position == .back {
                self.currentCameraPosition = .rear
            }
            
            let connection = self.movieFileOutput?.connection(with: AVMediaType.video)
            if connection?.isVideoStabilizationSupported ?? false {
                connection!.preferredVideoStabilizationMode = .auto
            }
            
            self.session.commitConfiguration()
            
            DispatchQueue.main.async {
                self.configureManualHUD()
                self.recordButton.isEnabled = true
                self.photoButton.isEnabled = true
                
            }
        }
    }
    
    @IBAction func changeFocusMode(_ control: UISegmentedControl) {
        let mode = self.focusModes[control.selectedSegmentIndex]
        
        do {
            try self.videoDevice!.lockForConfiguration()
            if self.videoDevice!.isFocusModeSupported(mode) {
                self.videoDevice!.focusMode = mode
            } else {
                NSLog("Focus mode %@ is not supported. Focus mode is %@.", mode.description, self.videoDevice!.focusMode.description)
                self.focusModeControl.selectedSegmentIndex = self.focusModes.index(of: self.videoDevice!.focusMode)!
            }
            self.videoDevice!.unlockForConfiguration()
        } catch let error {
            NSLog("Could not lock device for configuration: \(error)")
        }
    }
    
    @IBAction func changeLensPosition(_ control: UISlider) {
        do {
            try self.videoDevice!.lockForConfiguration()
            self.videoDevice!.setFocusModeLocked(lensPosition: control.value, completionHandler: nil)
            self.videoDevice!.unlockForConfiguration()
        } catch let error {
            NSLog("Could not lock device for configuration: \(error)")
        }
    }
    
    
    
    private func focusWithMode(_ focusMode: AVCaptureDevice.FocusMode, exposeWithMode exposureMode: AVCaptureDevice.ExposureMode, atDevicePoint point: CGPoint, monitorSubjectAreaChange: Bool) {
        guard let device = self.videoDevice else {
            print("videoDevice unavailable")
            return
        }
        self.sessionQueue.async {
            if self.selectedScope == false {
                do {
                    try device.lockForConfiguration()
                    // Setting (focus/exposure)PointOfInterest alone does not initiate a (focus/exposure) operation.
                    // Call -set(Focus/Exposure)Mode: to apply the new point of interest.
                    if focusMode != .locked && device.isFocusPointOfInterestSupported && device.isFocusModeSupported(focusMode) {
                        device.focusPointOfInterest = point
                        device.focusMode = focusMode
                    }
                    
                    if exposureMode == .locked && focusMode == .locked {
                        device.exposurePointOfInterest = point
                        device.exposureMode = exposureMode
                        device.focusMode = focusMode
                    }
                    
                    if exposureMode != .custom && device.isExposurePointOfInterestSupported && device.isExposureModeSupported(exposureMode) {
                        device.exposurePointOfInterest = point
                        device.exposureMode = exposureMode
                    }
                    
                    
                    device.isSubjectAreaChangeMonitoringEnabled = monitorSubjectAreaChange
                    device.unlockForConfiguration()
                } catch let error {
                    NSLog("Could not lock device for configuration: \(error)")
                }
            }
        }
    }
    
    @IBAction func focusAndExposeTap(tap: UITapGestureRecognizer) {
        UIScreen.main.brightness = CGFloat(0.75)
        screenBrightnessOutlet.isSelected = false
        isFocusLongPress = false
        removeBigRectangle()
        let devicePoint = (self.previewView.layer as! AVCaptureVideoPreviewLayer).captureDevicePointConverted(fromLayerPoint: tap.location(in: tap.view))
        self.focusWithMode(.continuousAutoFocus, exposeWithMode: .continuousAutoExposure, atDevicePoint: devicePoint, monitorSubjectAreaChange: true)
    }
    
    
    
    func focusAndExposeLongPress(tap: UILongPressGestureRecognizer) {
        isFocusLongPress = true
        removeRectangle()
        removeBigRectangle()
        let circleCenter = tap.location(in: view)
        
        if tap.view == previewView || tap.view == _glkView
        {
            if selectedScope == true {
                
            } else {
                burstShotView.isHidden = false
                burstShotTimer =  Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(hideBurstShotCounter), userInfo: nil, repeats: false)
                picturecountAndAfAeLabel.text = "AF/AE Locked"
                showRectangleBig(centerCircle: circleCenter, width: 80, height: 80)
            }
        }
        let devicePoint = (self.previewView.layer as! AVCaptureVideoPreviewLayer).captureDevicePointConverted(fromLayerPoint: tap.location(in: tap.view))
        self.focusWithMode(.continuousAutoFocus, exposeWithMode: .continuousAutoExposure, atDevicePoint: devicePoint, monitorSubjectAreaChange: true)
        self.focusWithMode(.locked, exposeWithMode: .locked, atDevicePoint: devicePoint, monitorSubjectAreaChange: false)
    }
    
    @IBAction func changeExposureMode(_ control: UISegmentedControl) {
        let mode = self.exposureModes[control.selectedSegmentIndex]
        
        do {
            try self.videoDevice!.lockForConfiguration()
            if self.videoDevice!.isExposureModeSupported(mode) {
                self.videoDevice!.exposureMode = mode
            } else {
                NSLog("Exposure mode %@ is not supported. Exposure mode is %@.", mode.description, self.videoDevice!.exposureMode.description)
                self.exposureModeControl.selectedSegmentIndex = self.exposureModes.index(of: self.videoDevice!.exposureMode)!
            }
            self.videoDevice!.unlockForConfiguration()
        } catch let error {
            NSLog("Could not lock device for configuration: \(error)")
        }
    }
    
    @IBAction func changeExposureDuration(_ control: UISlider) {
        
        let p = pow(Double(control.value), kExposureDurationPower) // Apply power function to expand slider's low-end range
        let minDurationSeconds = max(CMTimeGetSeconds(self.videoDevice!.activeFormat.minExposureDuration), kExposureMinimumDuration)
        let maxDurationSeconds = CMTimeGetSeconds(self.videoDevice!.activeFormat.maxExposureDuration)
        let newDurationSeconds = p * ( maxDurationSeconds - minDurationSeconds ) + minDurationSeconds; // Scale from 0-1 slider range to actual duration
        
        do {
            try self.videoDevice!.lockForConfiguration()
            self.videoDevice?.setExposureModeCustom(duration: CMTimeMakeWithSeconds(newDurationSeconds, 1000*1000*1000), iso: AVCaptureDevice.currentISO, completionHandler: nil)
            self.videoDevice!.unlockForConfiguration()
        } catch let error {
            NSLog("Could not lock device for configuration: \(error)")
        }
    }
    
    @IBAction func changeISO(_ control: UISlider) {
        //isoBrigh
        do {
            try self.videoDevice!.lockForConfiguration()
            
            self.videoDevice!.setExposureModeCustom(duration: AVCaptureDevice.currentExposureDuration, iso: control.value, completionHandler: nil)
            
            
            self.videoDevice!.unlockForConfiguration()
        } catch let error {
            NSLog("Could not lock device for configuration: \(error)")
        }
    }
    
    @IBAction func changeExposureTargetBias(_ control: UISlider) {
        
        do {
            try self.videoDevice!.lockForConfiguration()
            self.videoDevice!.setExposureTargetBias(control.value, completionHandler: nil)
            self.videoDevice!.unlockForConfiguration()
        } catch let error {
            NSLog("Could not lock device for configuration: \(error)")
        }
    }
    
    @IBAction func changeWhiteBalanceMode(_ control: UISegmentedControl) {
        let mode = self.whiteBalanceModes[control.selectedSegmentIndex]
        
        do {
            try self.videoDevice!.lockForConfiguration()
            if self.videoDevice!.isWhiteBalanceModeSupported(mode) {
                self.videoDevice!.whiteBalanceMode = mode
            } else {
                NSLog("White balance mode %@ is not supported. White balance mode is %@.", mode.description, self.videoDevice!.whiteBalanceMode.description)
                self.whiteBalanceModeControl.selectedSegmentIndex = self.whiteBalanceModes.index(of: self.videoDevice!.whiteBalanceMode)!
            }
            self.videoDevice!.unlockForConfiguration()
        } catch let error {
            NSLog("Could not lock device for configuration: \(error)")
        }
    }
    
    private func setWhiteBalanceGains(_ gains: AVCaptureDevice.WhiteBalanceGains) {
        
        do {
            try self.videoDevice!.lockForConfiguration()
            let normalizedGains = self.normalizedGains(gains) // Conversion can yield out-of-bound values, cap to limits
            self.videoDevice!.setWhiteBalanceModeLocked(with: normalizedGains, completionHandler: nil)
            self.videoDevice!.unlockForConfiguration()
        } catch let error {
            NSLog("Could not lock device for configuration: \(error)")
        }
    }
    
    @IBAction func changeTemperature(_: AnyObject) {
        let temperatureAndTint = AVCaptureDevice.WhiteBalanceTemperatureAndTintValues(
            temperature: self.temperatureSlider.value,
            tint: self.tintSlider.value
        )
        
        self.setWhiteBalanceGains(self.videoDevice!.deviceWhiteBalanceGains(for: temperatureAndTint))
    }
    
    @IBAction func changeTint(_: AnyObject) {
        let temperatureAndTint = AVCaptureDevice.WhiteBalanceTemperatureAndTintValues(
            temperature: self.temperatureSlider.value,
            tint: self.tintSlider.value
        )
        
        self.setWhiteBalanceGains(self.videoDevice!.deviceWhiteBalanceGains(for: temperatureAndTint))
    }
    
    @IBAction func lockWithGrayWorld(_: AnyObject) {
        self.setWhiteBalanceGains(self.videoDevice!.grayWorldDeviceWhiteBalanceGains)
    }
    
    private func normalizedGains(_ gains: AVCaptureDevice.WhiteBalanceGains) -> AVCaptureDevice.WhiteBalanceGains {
        var g = gains
        
        g.redGain = max(1.0, g.redGain)
        g.greenGain = max(1.0, g.greenGain)
        g.blueGain = max(1.0, g.blueGain)
        
        g.redGain = min(self.videoDevice!.maxWhiteBalanceGain, g.redGain)
        g.greenGain = min(self.videoDevice!.maxWhiteBalanceGain, g.greenGain)
        g.blueGain = min(self.videoDevice!.maxWhiteBalanceGain, g.blueGain)
        
        return g
    }
    
    //MARK: - ----- Capturing Photos --------
    
    @IBAction func capturePhoto(_ sender : UIButton?) {
        
        if snapShotTimerLimit > 0 {
            // snapShotToggleProperty = true
            if snapShotToggleProperty {
                startCameraSnapShotTimer()
                snapShotToggleProperty = !snapShotToggleProperty
            } else {
                stopCameraSnapShotTimer()
                snapShotToggleProperty = !snapShotToggleProperty
            }
        } else {
            
            if #available(iOS 10.0, *) {
                self.capturePhoto()
                
            } else {
                self.snapStillImage()
            }
        }
    }
    
    
    @IBAction func capturePhotoWhileRecording(_ sender : UIButton) {
        
        if #available(iOS 10.0, *) {
            self.capturePhoto()
            
        } else {
            self.snapStillImage()
        }
    }
    
    
    
    
    
    
    
    
    
    
    //MARK: Recording Movies
    
    @IBAction func toggleMovieRecording(_: Any) {
        
        self.recordButton.isEnabled = true
        
        
        
        let previewLayer = self.previewView.layer as! AVCaptureVideoPreviewLayer
        let previewLayerVideoOrientation = previewLayer.connection!.videoOrientation
        self.sessionQueue.async {
            if !(self.movieFileOutput?.isRecording ?? false) {
                if UIDevice.current.isMultitaskingSupported {
                    
                    self.backgroundRecordingID = UIApplication.shared.beginBackgroundTask(expirationHandler: nil)
                }
                
                let movieConnection = self.movieFileOutput?.connection(with: AVMediaType.video)
                movieConnection?.videoOrientation = previewLayerVideoOrientation
                
                // Start recording to temporary file
                let outputFileName = ProcessInfo.processInfo.globallyUniqueString
                let outputFileURL: URL
                
                if #available(iOS 10.0, *) {
                    outputFileURL = FileManager.default.temporaryDirectory.appendingPathComponent(outputFileName).appendingPathExtension("mov")
                } else {
                    outputFileURL = URL(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true).appendingPathComponent(outputFileName).appendingPathExtension("mov")
                }
                
                if (movieConnection?.isActive)! {
                    print("Yes active")
                }
                DispatchQueue.main.async {
                self.movieFileOutput!.startRecording(to: outputFileURL, recordingDelegate: self)
                self.isVideoRecording = true
                self.recordButton.buttonState = .recording
                }
                
            } else {
                DispatchQueue.main.async {
                SVProgressHUD.show()
                self.recordButton.buttonState = .normal
                
                self.movieFileOutput!.stopRecording()
                
                self.isVideoRecording = false
                }
            }
        }
    }
    
    
    var cameraIconFlag = false
    
    
    @objc func subjectAreaDidChange(_ notificaiton: Notification) {
        let devicePoint = CGPoint(x: 0.5, y: 0.5)
        self.focusWithMode(.continuousAutoFocus, exposeWithMode: .continuousAutoExposure, atDevicePoint: devicePoint, monitorSubjectAreaChange: false)
    }
    @objc  
    func sessionRuntimeError(_ notification: Notification) {
        let error = notification.userInfo![AVCaptureSessionErrorKey]! as! NSError
        NSLog("Capture session runtime error: %@", error)
        
        if error.code == AVError.Code.mediaServicesWereReset.rawValue {
            self.sessionQueue.async {
                // If we aren't trying to resume the session, try to restart it, since it must have been stopped due to an error (see -[resumeInterruptedSession:])
                if self.isSessionRunning {
                    self.session.startRunning()
                    self.isSessionRunning = self.session.isRunning
                } else {
                    DispatchQueue.main.async {
                        self.resumeButton.isHidden = false
                    }
                }
            }
        } else {
            self.resumeButton.isHidden = false
        }
    }
    
    @objc @available(iOS 9.0, *)
    func sessionWasInterrupted(_ notification: Notification) {
        // In some scenarios we want to enable the user to restart the capture session.
        // For example, if music playback is initiated via Control Center while using AVCamManual,
        // then the user can let AVCamManual resume the session running, which will stop music playback.
        // Note that stopping music playback in Control Center will not automatically resume the session.
        // Also note that it is not always possible to resume, see -[resumeInterruptedSession:].
        // In iOS 9 and later, the notification's userInfo dictionary contains information about why the session was interrupted
        let reason = AVCaptureSession.InterruptionReason(rawValue: notification.userInfo![AVCaptureSessionInterruptionReasonKey]! as! Int)!
        NSLog("Capture session was interrupted with reason %ld", reason.rawValue)
        
        if reason == .audioDeviceInUseByAnotherClient ||
            reason == .videoDeviceInUseByAnotherClient {
            // Simply fade-in a button to enable the user to try to resume the session running.
            self.resumeButton.isHidden = false
            self.resumeButton.alpha = 0.0
            UIView.animate(withDuration: 0.25, animations: {
                self.resumeButton.alpha = 1.0
            })
        } else if reason == .videoDeviceNotAvailableWithMultipleForegroundApps {
            // Simply fade-in a label to inform the user that the camera is unavailable.
            self.cameraUnavailableLabel.isHidden = false
            self.cameraUnavailableLabel.alpha = 0.0
            UIView.animate(withDuration: 0.25, animations: {
                self.cameraUnavailableLabel.alpha = 1.0
            })
        }
    }
    @objc  
    func sessionInterruptionEnded(_ notification: Notification) {
        NSLog("Capture session interruption ended")
        
        if !self.resumeButton.isHidden {
            UIView.animate(withDuration: 0.25, animations: {
                self.resumeButton.alpha = 0.0
            }, completion: {finished in
                self.resumeButton.isHidden = true
            })
        }
        if !self.cameraUnavailableLabel.isHidden {
            UIView.animate(withDuration: 0.25, animations: {
                self.cameraUnavailableLabel.alpha = 0.0
            }, completion: {finished in
                self.cameraUnavailableLabel.isHidden = true
            })
        }
    }
    
    //MARK: ### Compatibility
    @available(iOS, deprecated: 10.0)
    private func changeCamera() {
        //   self.cameraButton.isEnabled = false
        self.recordButton.isEnabled = false
        //self.stillButton.isEnabled = false
        self.photoButton.isEnabled = false
        
        self.sessionQueue.async {
            var preferredPosition = AVCaptureDevice.Position.unspecified
            
            switch self.videoDevice!.position {
            case .unspecified,
                 .front:
                
                preferredPosition = .back
            case .back:
                preferredPosition = .front
            }
            
            let newVideoDevice = AVCamManualCameraViewController.deviceWithMediaType(AVMediaType.video.rawValue, preferringPosition: preferredPosition)
            let newVideoDeviceInput: AVCaptureDeviceInput!
            do {
                newVideoDeviceInput = try AVCaptureDeviceInput(device: newVideoDevice!)
            } catch _ {
                newVideoDeviceInput = nil
            }
            
            self.session.beginConfiguration()
            
            
            // Remove the existing device input first, since using the front and back camera simultaneously is not supported.
            self.session.removeInput(self.videoDeviceInput!)
            if self.session.canAddInput(newVideoDeviceInput) {
                NotificationCenter.default.removeObserver(self,
                                                          name: .AVCaptureDeviceSubjectAreaDidChange, object: self.videoDevice)
                
                NotificationCenter.default.addObserver(self, selector: #selector(self.subjectAreaDidChange), name: .AVCaptureDeviceSubjectAreaDidChange, object: newVideoDevice)
                
                self.session.addInput(newVideoDeviceInput)
                self.videoDeviceInput = newVideoDeviceInput
                self.videoDevice = newVideoDevice
            } else {
                self.session.addInput(self.videoDeviceInput!)
            }
            
            let connection = self.movieFileOutput!.connection(with: AVMediaType.video)
            if (connection?.isVideoStabilizationSupported)! {
                connection?.preferredVideoStabilizationMode = .auto
            }
            
            self.session.commitConfiguration()
            
            DispatchQueue.main.async {
                //  self.cameraButton.isEnabled = true
                self.recordButton.isEnabled = true
                //self.stillButton.isEnabled = true
                self.photoButton.isEnabled = true
                
                self.configureManualHUD()
            }
        }
    }
    
    @available(iOS, deprecated: 10.0)
    private class func deviceWithMediaType(_ mediaType: String, preferringPosition position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        let devices = AVCaptureDevice.devices(for: AVMediaType(rawValue: mediaType)) as! [AVCaptureDevice]
        var captureDevice = devices.first
        
        for device in devices {
            if device.position == position {
                captureDevice = device
                break
            }
        }
        
        return captureDevice
    }
    
    @available(iOS, deprecated: 10.0)
    func snapStillImage() {
        self.sessionQueue.async {
            let stillImageConnection = self.stillImageOutput!.connection(with: AVMediaType.video)
            let previewLayer = self.previewView.layer as! AVCaptureVideoPreviewLayer
            
            // Update the orientation on the still image output video connection before capturing.
            stillImageConnection?.videoOrientation = previewLayer.connection!.videoOrientation
            
            // Flash set to Auto for Still Capture
            if self.videoDevice!.exposureMode == .custom {
                AVCamManualCameraViewController.setFlashMode(.off, forDevice: self.videoDevice!)
            } else {
                AVCamManualCameraViewController.setFlashMode(.auto, forDevice: self.videoDevice!)
            }
            
            let lensStabilizationEnabled: Bool
            if #available(iOS 9.0, *) {
                lensStabilizationEnabled = self.stillImageOutput!.isLensStabilizationDuringBracketedCaptureEnabled
            } else {
                lensStabilizationEnabled = false
            }
            if !lensStabilizationEnabled {
                // Capture a still image
                
                self.stillImageOutput?.captureStillImageAsynchronously(from: self.stillImageOutput!.connection(with: AVMediaType.video)!) {imageDataSampleBuffer, error in
                    
                    if error != nil {
                        NSLog("Error capture still image \(error!)")
                    } else if imageDataSampleBuffer != nil {
                        let imageData1 = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer!)!
                        let image : UIImage = UIImage(data: imageData1)!
                        image.rotate(radians: 1.2)
                        let imageData = UIImagePNGRepresentation(image)
                        
                        
                        PHPhotoLibrary.requestAuthorization {status in
                            if status == PHAuthorizationStatus.authorized {
                                if #available(iOS 9.0, *) {
                                    PHPhotoLibrary.shared().performChanges({
                                        PHAssetCreationRequest.forAsset().addResource(with: PHAssetResourceType.photo, data: imageData!, options: nil)
                                    }, completionHandler: {success, error in
                                        if !success {
                                            NSLog("Error occured while saving image to photo library: \(error!)")
                                        }
                                    })
                                } else {
                                    let temporaryFileName = ProcessInfo().globallyUniqueString as NSString
                                    let temporaryFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent(temporaryFileName.appendingPathExtension("jpg")!)
                                    let temporaryFileURL = URL(fileURLWithPath: temporaryFilePath)
                                    
                                    PHPhotoLibrary.shared().performChanges({
                                        do {
                                            try imageData?.write(to: temporaryFileURL, options: .atomicWrite)
                                            PHAssetChangeRequest.creationRequestForAssetFromImage(atFileURL: temporaryFileURL)
                                        } catch let error {
                                            NSLog("Error occured while writing image data to a temporary file: \(error)")
                                        }
                                    }, completionHandler: {success, error in
                                        if !success {
                                            NSLog("Error occurred while saving image to photo library: \(error!)")
                                        }
                                        
                                        // Delete the temporary file.
                                        do {
                                            try FileManager.default.removeItem(at: temporaryFileURL)
                                        } catch _ {}
                                    })
                                }
                            }
                        }
                    }
                }
            } else {
                if #available(iOS 9.0, *) {
                    // Capture a bracket
                    let bracketSettings: [AVCaptureBracketedStillImageSettings]
                    if self.videoDevice!.exposureMode == .custom {
                        bracketSettings = [AVCaptureManualExposureBracketedStillImageSettings.manualExposureSettings(exposureDuration: AVCaptureDevice.currentExposureDuration, iso: AVCaptureDevice.currentISO)]
                    } else {
                        bracketSettings = [AVCaptureAutoExposureBracketedStillImageSettings.autoExposureSettings(exposureTargetBias: AVCaptureDevice.currentExposureTargetBias)];
                    }
                    
                    self.stillImageOutput!.captureStillImageBracketAsynchronously(from: self.stillImageOutput!.connection(with: AVMediaType.video)!,
                                                                                  withSettingsArray: bracketSettings
                        
                    ) {imageDataSampleBuffer, stillImageSettings, error in
                        if error != nil {
                            NSLog("Error bracketing capture still image \(error!)")
                        } else if imageDataSampleBuffer != nil {
                            NSLog("Lens Stabilization State: \(CMGetAttachment(imageDataSampleBuffer!, kCMSampleBufferAttachmentKey_StillImageLensStabilizationInfo, nil)!)")
                            let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer!)
                            
                            PHPhotoLibrary.requestAuthorization {status in
                                if status == PHAuthorizationStatus.authorized {
                                    PHPhotoLibrary.shared().performChanges({
                                        PHAssetCreationRequest.forAsset().addResource(with: PHAssetResourceType.photo, data: imageData!, options: nil)
                                    }, completionHandler: {success, error in
                                        if !success {
                                            NSLog("Error occured while saving image to photo library: \(error!)")
                                        }
                                    })
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @available(iOS, deprecated: 10.0)
    class func setFlashMode(_ flashMode: AVCaptureDevice.FlashMode, forDevice device: AVCaptureDevice) {
        if device.hasFlash && device.isFlashModeSupported(flashMode) {
            do {
                try device.lockForConfiguration()
                device.flashMode = flashMode
                device.unlockForConfiguration()
            } catch let error {
                NSLog("Could not lock device for configuration: \(error)")
            }
        }
    }
    
    
    func changeLensStabilization() {
        
        
        if self.stillImageOutput!.isLensStabilizationDuringBracketedCaptureEnabled {
            
            if self.stillImageOutput!.isLensStabilizationDuringBracketedCaptureSupported {
                self.photoButton.isEnabled = false
                self.sessionQueue.async {
                    
                    // Still image capture will be done with the bracketed capture API.
                    self.stillImageOutput!.isLensStabilizationDuringBracketedCaptureEnabled = true
                    let bracketSettings: [AVCaptureBracketedStillImageSettings]
                    if self.videoDevice!.exposureMode == AVCaptureDevice.ExposureMode.custom {
                        bracketSettings = [AVCaptureManualExposureBracketedStillImageSettings.manualExposureSettings(exposureDuration: AVCaptureDevice.currentExposureDuration, iso: AVCaptureDevice.currentISO)]
                    } else {
                        bracketSettings = [AVCaptureAutoExposureBracketedStillImageSettings.autoExposureSettings(exposureTargetBias: AVCaptureDevice.currentExposureTargetBias)]
                    }
                    self.stillImageOutput!.prepareToCaptureStillImageBracket(from: self.stillImageOutput!.connection(with: AVMediaType.video)!,
                                                                             withSettingsArray: bracketSettings
                    ) {prepared, error in
                        if error != nil {
                            NSLog("Error preparing for bracketed capture \(error!)")
                        }
                        DispatchQueue.main.async {
                            self.photoButton.isEnabled = true
                        }
                    }
                    
                }
                
            }
            else {
                self.stillImageOutput!.isLensStabilizationDuringBracketedCaptureEnabled = false
                EZAlertController.alert("Info!", message: "Image Stabilization not supported by this phone")
            }
        }
        else {
            self.stillImageOutput!.isLensStabilizationDuringBracketedCaptureEnabled = false
        }
        
    }
    
    // MARK: - ------- SELECTORS --------
    
    @objc func donePicker(sender : UIBarButtonItem){
        if rifleValue1?.name == "" {
            rifleValue1 = rifleValue[0]
        }
        if let value1 = rifleValue1?.isoValue as? Float {
            isoSlider.value = (rifleValue1?.isoValue)!
            if selectedScope {
                sliderValueChanged(sender: isoSlider)
            }else{
                
            }
            
        }
        if let value2 = rifleValue1?.rotateValue as? Float {
            rotationSlider.value = (rifleValue1?.rotateValue)!
            if selectedScope {
                sliderValueChanged(sender: rotationSlider)
                
            }
        }
        if let value3 = rifleValue1?.focusValue as? Float {
            focusSlider.value = (rifleValue1?.focusValue)!
            if selectedScope{
                sliderValueChanged(sender: focusSlider)
            }
            
        }
        self.view.endEditing(true)
        
    }
    @objc func cancelPicker(sender : UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    
    
    // MARK: - ------- IBACTIONS --------
    
    
    
    @IBAction func buyLensAction(_ sender: Any) {
        if let url = URL(string: "https://www.phoneskope.com/store/product/2/530/"){
            UIApplication.shared.open(url, options: [:], completionHandler: { (status) in
                print(status)
            })
        }
    }
    
    @IBAction func lockRotation(_ sender: UIButton) {
        isRotationLock = !isRotationLock
        
        if isRotationLock {
            
            if UIDeviceOrientationIsPortrait(UIDevice.current.orientation) {
                AppUtility.lockOrientation(.portrait)
                
            } else {
                AppUtility.lockOrientation(.landscape)
            }
            
            sender.isSelected = true
            
        }else{
            AppUtility.lockOrientation(.all)
            sender.isSelected = false
            
        }
        
    }
    
    @IBAction func photoQualityButtonAction(_ sender: Any) {
        self.hideViews(view:photoQualityCollectionView)
    }
    
    @IBAction func videoQualityButtonAction(_ sender: Any) {
        if is4kVideo {
            let alert = UIAlertController(title: "", message: "4K Videos is selected. You can not change 4k video quality.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }else{
            self.hideViews(view:videoQualityCollectionView)
        }
        //  settingContainerView.isHidden = false
    }
    
    
    @IBAction func filterAction(_ sender: Any) {
        
        isFilterTriggered = true
        if currentFilter == nil {
            // removeVideoDataOutputFromSession()
            self.previewView.isHidden = false
        }
        
        self.hideViews(view: filterCollectionView)
        self.filterModeFunction(sender as? UIButton)
        
    }
    
    @IBAction func menuAction(_ sender: Any) {
        
        if captureModeTag == AVCamManualCaptureMode.photo.rawValue {
            if photoMenuSV.isHidden {
                mirrorScrollView.isHidden=true
                videoMenu.isHidden=true
                hideViews(view: photoMenuSV)
                self.view.bringSubview(toFront:photoMenuSV )
                
            }
            else
            {
                // self.view.sendSubview(toBack: purchaseButtonOutlet)
                
                hideViews(view: bottomContainerView)
                photoMenuSV.isHidden=true
            }
            
        }
        else{
            if videoMenu.isHidden
            {
                self.view.bringSubview(toFront:videoMenu )
                self.view.bringSubview(toFront:mirrorScrollView )
                hideViews(view: mirrorScrollView)
                hideViews(view: videoMenu)
                
            }
            else
            {
                // self.view.sendSubview(toBack: purchaseButtonOutlet)
                
                hideViews(view: bottomContainerView)
                mirrorScrollView.isHidden=true
                videoMenu.isHidden=true
                
            }
        }
    }
    
    
    @IBAction func mirrorAction(_ sender: Any) {
        
        if mirrorView.isHidden {
            mirrorView.isHidden = false
        }else{
            mirrorView.isHidden = true
        }
    }
    
    
    @IBAction func teleButtonAction(_ sender: Any) {
        
        setTelephoto(sender: sender as! UIButton)
    }
    
    
    @IBAction func stabilizationOnnAction(_ sender: Any) {
        stabilizationOnOffString = "on"
        
        stabilizationOnnButton.setTitleColor(UIColor.yellow, for: UIControlState.normal)
        stabilizationOffButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        
    }
    
    @IBAction func stabilizationOffAction(_ sender: Any) {
        
        stabilizationOnOffString = "off"
        stabilizationOnnButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        stabilizationOffButton.setTitleColor(UIColor.yellow, for: UIControlState.normal)
        
    }
    
    @IBAction func geoTagOnnAction(_ sender: UIButton) {
        AppDelegate.IS_GEO_TAG_ON = true
        if status == .notDetermined{
            locationMgr.requestWhenInUseAuthorization()
        }
        if CLLocationManager.locationServicesEnabled() {
            locationMgr.startUpdatingLocation()
        }
        geoTagOnnButton.setTitleColor(UIColor.yellow, for: UIControlState.normal)
        geoTagOffButton.setTitleColor(UIColor.white, for: UIControlState.normal)
    }
    
    @IBAction func geoTagOffAction(_ sender: Any) {
        AppDelegate.IS_GEO_TAG_ON = false
        stopButtonTapped()
        geoTagOnnButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        geoTagOffButton.setTitleColor(UIColor.yellow, for: UIControlState.normal)
    }
    
    @IBAction func stabilizationButtonAction(_ sender: Any) {
        
        self.hideViews(view: stabilizationView)
        
        
    }
    
    
    @IBAction func waterMarkDoneButtonAction(_ sender: Any) {
        
        if self.waterMarkField.text == "" {
            UserDefaults.standard.setValue(nil, forKey: UserDefaultKey.watermark.rawValue)
        }else {
            UserDefaults.standard.setValue(self.waterMarkField.text, forKey: UserDefaultKey.watermark.rawValue)
        }
        
        UserDefaults.standard.synchronize()
        
        waterMarkView.isHidden = true
        self.view.sendSubview(toBack: waterMarkView)
        
        self.view.endEditing(true)
        
    }
    
    
    @IBAction func changeScreenBrightness(_ sender: UIButton) {
        if sender.isSelected {
            UIScreen.main.brightness = CGFloat(0.75)
            screenBrightnessOutlet.isSelected = false
        }else{
            screenBrightnessOutlet.isSelected = true
            UIScreen.main.brightness = CGFloat(0.0)
            
        }
    }
    
    @IBAction func selectRifleSettingAction(_ sender: Any) {
        if rifleValue.count != 0{
            rifleSettingsTextField.becomeFirstResponder()
        }else{
            
            let alert = UIAlertController(title: "", message: "No Preset available.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func backToMenuAction(_ sender: Any) {
        selectScopeView.isHidden = false
        self.view.sendSubview(toBack: waterMarkView)
        self.view.bringSubview(toFront: selectScopeView)
        
    }
    
    @IBAction func rifleScopeAction(_ sender: Any) {
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { success in
            if success {
                DispatchQueue.main.async {
                    let videos = self.dataHandler.getSaveVideoPath()
                    if videos.count > 0 {
                        self.activityIndicationView.color = #colorLiteral(red: 0.8250325322, green: 0.8026339412, blue: 0.2692880332, alpha: 1)
                        self.activityIndicationView.type = .ballRotateChase
                        self.queueVideosListBtnOutlet.setTitle("\(videos.count)", for: .normal)
                        self.queueVideosListBtnOutlet.isHidden = false
                        self.activityIndicationView.startAnimating()
                    }else{
                        self.queueVideosListBtnOutlet.isHidden = true
                        self.activityIndicationView.stopAnimating()
                    }
                    self.selectedScope = true
                    self.timeLapsContentWidth.constant = 0
                    self.mirrorOffOutlet.setTitleColor(UIColor.white, for: UIControlState.normal)
                    self.mirrorOnOutlet.setTitleColor(UIColor.yellow, for: UIControlState.normal)
                    self.previewView.transform = CGAffineTransform(scaleX: -1, y: 1)
                    
                    self.queueVideosListBtnOutlet.isHidden = false
                    self.ISOSlider.isHidden = true
                    self.lensPositionSlider.isHidden = true
                    self.focusImageView.image = UIImage(named : "FocusSlider")
                    self.photFlashView.isHidden = true
                    self.scopeButtonMenu.isHidden = false
                    
                    self.selectScopeView.isHidden = true
                    self.isoSlider.isHidden = false
                    self.focusSlider.isHidden = false
                    self.rotationSlider.isHidden = false
                    
                    self.saveRifleOutlet.isHidden = false
                    self.selectRifleOulet.isHidden = false
                    self.switchCameraPhotoTop.isHidden = true
                    self.photoModeButton.isHidden = true
                    
                    self.videoModeButton.isHidden = false
                    self.rotationImageView.isHidden = false
                    self.focusImageView.isHidden = false
                    self.brightImageView.isHidden = false
                    
                    self.upButtonOutlet.isHidden = false
                    self.downButtonOulet.isHidden = false
                    self.chooseNewCamera()
                    self.isoLeftViewButton.isHidden = true
                    self.focusLeftViewButton.isHidden = true
                    if isTimeLapsReady{
                        self.setTimeLapse()
                    }
                    self.changeCaptureMode(self.photoModeButton)
                    
                    self.selectedScopeIntroButtons()
                    
                }
            } else {
                self.messageAccessNotGiven(title: "Camera Permission Required", message: "In order you this application you need to give camera permission." )
                return
            }
        }
        
        
        
        
    }
    
    @IBAction func turnOffTrainingModeAction(_ sender: Any) {
        dataHandler.saveTrainingMode(false)
        let mybutton0 = self.view.viewWithTag(9970) as! UIButton
        mybutton0.isHidden = true
        let mybutton1 = self.view.viewWithTag(9971) as! UIButton
        mybutton1.isHidden = true
        let mybutton2 = self.view.viewWithTag(9972) as! UIButton
        mybutton2.isHidden = true
        let mybutton3 = self.view.viewWithTag(9973) as! UIButton
        mybutton3.isHidden = true
        let mybutton4 = self.view.viewWithTag(9974) as! UIButton
        mybutton4.isHidden = true
        let mybutton5 = self.view.viewWithTag(9975) as! UIButton
        mybutton5.isHidden = true
        let mybutton6 = self.view.viewWithTag(9976) as! UIButton
        mybutton6.isHidden = true
        let mybutton7 = self.view.viewWithTag(9977) as! UIButton
        mybutton7.isHidden = true
        let mybutton8 = self.view.viewWithTag(9978) as! UIButton
        mybutton8.isHidden = true
        let mybutton9 = self.view.viewWithTag(9979) as! UIButton
        mybutton9.isHidden = true
        let mybutton10 = self.view.viewWithTag(9980) as! UIButton
        mybutton10.isHidden = true
        let mybutton11 = self.view.viewWithTag(9981) as! UIButton
        mybutton11.isHidden = true
        let mybutton12 = self.view.viewWithTag(9982) as! UIButton
        mybutton12.isHidden = true
        let mybutton13 = self.view.viewWithTag(9983) as! UIButton
        mybutton13.isHidden = true
        let mybutton14 = self.view.viewWithTag(9984) as! UIButton
        mybutton14.isHidden = true
        let mybutton15 = self.view.viewWithTag(9985) as! UIButton
        mybutton15.isHidden = true
        let mybutton16 = self.view.viewWithTag(9986) as! UIButton
        mybutton16.isHidden = true
        let mybutton17 = self.view.viewWithTag(9987) as! UIButton
        mybutton17.isHidden = true
        let mybutton18 = self.view.viewWithTag(9988) as! UIButton
        mybutton18.isHidden = true
        let mybutton19 = self.view.viewWithTag(9989) as! UIButton
        mybutton19.isHidden = true
        let mybutton20 = self.view.viewWithTag(9990) as! UIButton
        mybutton20.isHidden = true
        let mybutton21 = self.view.viewWithTag(9991) as! UIButton
        mybutton21.isHidden = true
        let mybutton22 = self.view.viewWithTag(9992) as! UIButton
        mybutton22.isHidden = true
        let mybutton23 = self.view.viewWithTag(9993) as! UIButton
        mybutton23.isHidden = true
        let mybutton24 = self.view.viewWithTag(9994) as! UIButton
        mybutton24.isHidden = true
        let mybutton25 = self.view.viewWithTag(9995) as! UIButton
        mybutton25.isHidden = true
        let mybutton26 = self.view.viewWithTag(9996) as! UIButton
        mybutton26.isHidden = true
        let mybutton27 = self.view.viewWithTag(9997) as! UIButton
        mybutton27.isHidden = true
        let mybutton28 = self.view.viewWithTag(9998) as! UIButton
        mybutton28.isHidden = true
        turnOffTrainingModeView.isHidden = true
        
    }
    //MARK : - binocularScope
    @IBAction func binocularScopeAction(_ sender: Any) {
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { success in
            if success {
                DispatchQueue.main.async {
                    self.queueVideosListBtnOutlet.isHidden = true
                    self.activityIndicationView.stopAnimating()
                    self.selectedScope = false
                    self.previewView.transform = CGAffineTransform(scaleX: 1, y: 1)
                    self._glkView.transform = CGAffineTransform(scaleX: 1,y : 1)
                    
                    self.queueVideosListBtnOutlet.isHidden = true
                    self.photFlashView.isHidden = false
                    self.timeLapsContentWidth.constant = 130
                    self.upButtonOutlet.isHidden = true
                    self.downButtonOulet.isHidden = true
                    
                    self.mirrorOnOutlet.setTitleColor(UIColor.white, for: UIControlState.normal)
                    self.mirrorOffOutlet.setTitleColor(UIColor.yellow, for: UIControlState.normal)
                    self.selectScopeView.isHidden = true
                    self.isoSlider.isHidden = true
                    self.focusSlider.isHidden = true
                    self.rotationSlider.isHidden = true
                    
                    self.saveRifleOutlet.isHidden = true
                    self.selectRifleOulet.isHidden = true
                    self.switchCameraPhotoTop.isHidden = false
                    self.photoModeButton.isHidden = true
                    self.videoModeButton.isHidden = false
                    
                    self.rotationImageView.isHidden = true
                    self.focusImageView.isHidden = true
                    self.brightImageView.isHidden = true
                    self.isoLeftViewButton.isHidden = false
                    self.focusLeftViewButton.isHidden = false
                    
                    self.changeCaptureMode(self.photoModeButton)
                    self.selectedScopeIntroButtons()
                    
                }
            } else {
                self.messageAccessNotGiven(title: "Camera Permission Required", message: "In order you this application you need to give camera permission." )
                return
            }
        }
        
        
        
    }
    
    @IBAction func featureListActionButton(_ sender: Any) {
        let sB: UIStoryboard = UIStoryboard(name: "Main", bundle: nil) //name of your storyboard
        let newVC = sB.instantiateViewController(withIdentifier: "DeviceInformationViewController") as! DeviceInformationViewController //if you have the restoration ID, otherwise use 'ModalViewController'
        newVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        newVC.modalPresentationStyle =  UIModalPresentationStyle.overFullScreen
        self.present(newVC, animated: true, completion: nil)
        
    }
    
    
    @IBAction func saveSettings(_ sender: Any) {
        
        let alert = UIAlertController(title: "Name", message: "Please Enter a new name for setting.", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.placeholder = "Enter new.."
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            print("Text field: \(textField?.text)")
            
            if textField?.text == ""{
                
                return
            }
            
            let rifleArray = self.dataHandler.getSettingsForRifle()
            
            for var rifle in rifleArray{
                if rifle.name != textField?.text {
                    
                    
                    
                }else{
                    let alert = UIAlertController(title: "", message: "Preset name already exist! Please try with new name.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return
                }
            }
            self.dataHandler.saveSettingsForRifle(name: (textField?.text)!, iso: self.isoSlider.value, rotate: self.rotationSlider.value, focus: self.focusSlider.value)
            self.rifleValue.removeAll()
            self.rifleValue = self.dataHandler.getSettingsForRifle()
            
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //MARK: - ---------- IN-APP PURCHASE IBACTIONS ------------
    
    @IBAction func openPurchaseAction(_ sender: Any) {
        let sB: UIStoryboard = UIStoryboard(name: "Main", bundle: nil) //name of your storyboard
        let newVC = sB.instantiateViewController(withIdentifier: "BuyProductViewController") as! BuyProductViewController //if you have the restoration ID, otherwise use 'ModalViewController'
        newVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        newVC.modalPresentationStyle =  UIModalPresentationStyle.fullScreen
        self.present(newVC, animated: true, completion: nil)
    }
    
    //MARK: ------ PRIVATE FUNCTIONS -------
    
    func checkAudioDevicePermission ()  -> Bool {
        
        switch AVAudioSession.sharedInstance().recordPermission() {
        case AVAudioSessionRecordPermission.granted:
            return true
            
        case AVAudioSessionRecordPermission.denied:
            self.messageAccessNotGiven(title : "Microphone Permission required", message : "If you want to save rifle scope video you need to grant microphone permission.")
            return false
        case AVAudioSessionRecordPermission.undetermined:
            
            self.messageAccessNotGiven(title : "Microphone Permission required", message : "If you want to save rifle scope video you need to grant microphone permission.")
            return false
        default:
            return false
        }
    }
    
    func captureBurstShot(tap: UILongPressGestureRecognizer) {
        if !selectedScope {
            let queue = DispatchQueue(label: "VideoQueue")
            
            if tap.state == UIGestureRecognizerState.began {
                
                isLongPress = true
                self.videoDataOutput = AVCaptureVideoDataOutput()
                self.videoDataOutput!.videoSettings = [
                    String(kCVPixelBufferPixelFormatTypeKey) : Int(kCVPixelFormatType_420YpCbCr8BiPlanarFullRange)
                ]
                self.videoDataOutput!.alwaysDiscardsLateVideoFrames = true
                
                if self.session!.canAddOutput(videoDataOutput!) {
                    self.session!.addOutput(videoDataOutput!)
                }
                
                self.session!.commitConfiguration()
                self.videoDataOutput!.setSampleBufferDelegate(self, queue: queue)
                
            } else if tap.state == UIGestureRecognizerState.ended {
                saveArraysOfPhoto ()
            }
        }
    }
    
    func saveArraysOfPhoto () {
        counter = 0
        isLongPress = false
        
        if currentFilter != nil {
            burstShotimages.removeAll()
            let alert = UIAlertController(title: "", message: "Can not capture burst shot. Please remove filters.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
            
        } else {
            removeVideoDataOutputFromSession()
        }
        
        if burstShotimages.count > 0 {
            burstShotimages.removeFirst()
            
            for  image in burstShotimages {
                
                let imageData = setPhotoOrientation(image)
                
                self.savePicturesToLibrary(imageData: imageData) {
                    if self.burstShotimages.count != 0 {
                        self.burstShotimages.removeFirst()
                    }
                }
            }
        }
    }
    
    func setPhotoOrientation (_ image: UIImage) -> Data {
        print(image.imageOrientation)
        var changedImage = UIImage(cgImage:image.cgImage!, scale:image.scale, orientation:.left)
        if deviceOrientaion == UIDeviceOrientation.portrait {
            if isFilterTriggered {
                changedImage = UIImage(cgImage:image.cgImage!, scale:image.scale, orientation:.leftMirrored)
            } else {
                changedImage = UIImage(cgImage:image.cgImage!, scale:image.scale, orientation:.right)
            }
        } else if deviceOrientaion == UIDeviceOrientation.landscapeLeft {
            changedImage = UIImage(cgImage:image.cgImage!, scale:image.scale, orientation:.up)
            
        } else if deviceOrientaion == UIDeviceOrientation.landscapeRight {
            changedImage = UIImage(cgImage:image.cgImage!, scale:image.scale, orientation:.down)
            
        }
        let imageData = UIImageJPEGRepresentation(changedImage, 1.0)!
        return imageData
        
    }
    
    @objc func waterMarkViewAppear() {
        hideViews(view: waterMarkView)
        self.view.bringSubview(toFront: waterMarkView)
        
        if let watermark = UserDefaults.standard.value(forKey: UserDefaultKey.watermark.rawValue) as? String {
            self.waterMarkField.text = watermark
        } else {
            self.waterMarkField.text = ""
        }
    }
    
    @objc func  fileTypeViewAppear() {
        self.rawJpgButtonColorSetting()
        self.hideViews(view: fileTypeView)
        
    }
    
    func updateCurrentLocation() {
        locationMgr = CLLocationManager()
        locationMgr.delegate = self
        locationMgr.desiredAccuracy = kCLLocationAccuracyBest
        locationMgr.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationMgr.startUpdatingLocation()
        }
    }
    
    func messageAccessNotGiven(title :String, message : String){
        
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Alert button to open Settings"), style: .default) {action in
                self.messageAccessNotGiven(title: title, message: message)
            }
            
            // Provide quick access to Settings.
            let settingsAction = UIAlertAction(title: NSLocalizedString("Allow", comment: "Alert button to open Settings"), style: .default) {action in
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                } else {
                    UIApplication.shared.openURL(URL(string: UIApplicationOpenSettingsURLString)!)
                }
            }
            
            alertController.addAction(settingsAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showTrainingModeModal(){
        let sB: UIStoryboard = UIStoryboard(name: "Main", bundle: nil) //name of your storyboard
        let newVC = sB.instantiateViewController(withIdentifier: "TrainingModeViewController") as! TrainingModeViewController //if you have the restoration ID, otherwise use 'ModalViewController'
        newVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        newVC.modalPresentationStyle =  UIModalPresentationStyle.popover
        self.present(newVC, animated: true, completion: nil)
    }
    
    
    func getValuesForRifle (){
        // self.rifleValue.removeAll()
        rifleValue = dataHandler.getSettingsForRifle()
        
    }
    
    func showTrainingModeModel(){
        let sB: UIStoryboard = UIStoryboard(name: "Main", bundle: nil) //name of your storyboard
        let newVC = sB.instantiateViewController(withIdentifier: "TrainingModeViewController") as! TrainingModeViewController //if you have the restoration ID, otherwise use 'ModalViewController'
        newVC.modalPresentationStyle =  UIModalPresentationStyle.popover
        self.present(newVC, animated: true, completion: nil)
    }
    
    @objc func rotated() {
        
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            
            UIView.animate(withDuration: 0.5, animations: {
                self.scopeStackView.axis = .horizontal
                self.scopeStackView.distribution = .equalSpacing
                self.scopeStackView.alignment = .center
                self.stackViewTopConstraint.constant = -30
            })
            
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.scopeStackView.axis = .vertical
                self.scopeStackView.distribution = .fillEqually
                self.stackViewTopConstraint.constant = 10
            })
            
        }
    }
    
    func selectedScopeIntroButtons () {
        
        if selectedScope{
            
            if !dataHandler.getTrainingMode(){
                return
                
            } else {
                //intro button hiding according to skope
                let mybutton = self.view.viewWithTag(9971) as! UIButton
                mybutton.isHidden = true
                
                if dataHandlerButton.getIntroductionSettings(key: 9972).hasShown == false{
                    let mybutton1 = self.view.viewWithTag(9972) as! UIButton
                    mybutton1.isHidden = false
                }else {
                    let mybutton6 = self.view.viewWithTag(9972) as! UIButton
                    mybutton6.isHidden = true
                }
                //select rifle scope presets
                if dataHandlerButton.getIntroductionSettings(key: 9974).hasShown == false{
                    let mybutton2 = self.view.viewWithTag(9974) as! UIButton
                    mybutton2.isHidden = false
                }else {
                    let mybutton6 = self.view.viewWithTag(9974) as! UIButton
                    mybutton6.isHidden = true
                }
                
                //switch change camera  top
                let mybutton3 = self.view.viewWithTag(9975) as! UIButton
                mybutton3.isHidden = true
                // timelaps button hidden for rifle scope
                let button25 = self.view.viewWithTag(9995)
                button25?.isHidden = true
                
                
                //iso left view
                let mybutton4 = self.view.viewWithTag(9976) as! UIButton
                mybutton4.isHidden = true
                
                //focus left view
                let mybutton5 = self.view.viewWithTag(9977) as! UIButton
                mybutton5.isHidden = true
                
                // rotatio  image view
                if dataHandlerButton.getIntroductionSettings(key: 9978).hasShown == false{
                    let mybutton6 = self.view.viewWithTag(9978) as! UIButton
                    mybutton6.isHidden = false
                }else {
                    let mybutton6 = self.view.viewWithTag(9978) as! UIButton
                    mybutton6.isHidden = true
                }
                
                // iso image view
                if dataHandlerButton.getIntroductionSettings(key: 9979).hasShown == false{
                    let mybutton7 = self.view.viewWithTag(9979) as! UIButton
                    mybutton7.isHidden = false
                }else {
                    let mybutton6 = self.view.viewWithTag(9979) as! UIButton
                    mybutton6.isHidden = true
                }
                // focus image view
                if dataHandlerButton.getIntroductionSettings(key: 9980).hasShown == false{
                    let mybutton8 = self.view.viewWithTag(9980) as! UIButton
                    mybutton8.isHidden = false
                }else {
                    let mybutton6 = self.view.viewWithTag(9980) as! UIButton
                    mybutton6.isHidden = true
                }
                //up slider button outlet
                if dataHandlerButton.getIntroductionSettings(key: 9981).hasShown == false{
                    let mybutton9 = self.view.viewWithTag(9981) as! UIButton
                    mybutton9.isHidden = false
                }else {
                    let mybutton6 = self.view.viewWithTag(9981) as! UIButton
                    mybutton6.isHidden = true
                }
                //timelaps hidden in rifle scope
                let button21 = self.view.viewWithTag(9995)
                button21?.isHidden = true
                //down slider button outlet
                if dataHandlerButton.getIntroductionSettings(key: 9982).hasShown == false{
                    let mybutton10 = self.view.viewWithTag(9982) as! UIButton
                    mybutton10.isHidden = false
                }else {
                    let mybutton6 = self.view.viewWithTag(9982) as! UIButton
                    mybutton6.isHidden = true
                }
            }
        }else{
            
            if !dataHandler.getTrainingMode(){
                return
            }else{
                
                //intro button hiding according to skope
                if dataHandlerButton.getIntroductionSettings(key: 9971).hasShown == false{
                    let mybutton = self.view.viewWithTag(9971) as! UIButton
                    mybutton.isHidden = false
                }else {
                    let mybutton6 = self.view.viewWithTag(9971) as! UIButton
                    mybutton6.isHidden = true
                }
                let mybutton1 = self.view.viewWithTag(9972) as! UIButton
                mybutton1.isHidden = true
                
                
                //select rifle scope presets
                let mybutton2 = self.view.viewWithTag(9974) as! UIButton
                mybutton2.isHidden = true
                
                //switch change camera  top
                if dataHandlerButton.getIntroductionSettings(key: 9975).hasShown == false{
                    let mybutton3 = self.view.viewWithTag(9975) as! UIButton
                    mybutton3.isHidden = false
                }else {
                    let mybutton6 = self.view.viewWithTag(9975) as! UIButton
                    mybutton6.isHidden = true
                }
                
                //iso left view
                if dataHandlerButton.getIntroductionSettings(key: 9976).hasShown == false{
                    let mybutton4 = self.view.viewWithTag(9976) as! UIButton
                    mybutton4.isHidden = false
                }else {
                    let mybutton6 = self.view.viewWithTag(9976) as! UIButton
                    mybutton6.isHidden = true
                }
                //focus left view
                if dataHandlerButton.getIntroductionSettings(key: 9977).hasShown == false{
                    let mybutton5 = self.view.viewWithTag(9977) as! UIButton
                    mybutton5.isHidden = false
                }else {
                    let mybutton6 = self.view.viewWithTag(9977) as! UIButton
                    mybutton6.isHidden = true
                }
                // rotatio  image view
                let mybutton6 = self.view.viewWithTag(9978) as! UIButton
                mybutton6.isHidden = true
                
                // iso image view
                let mybutton7 = self.view.viewWithTag(9979) as! UIButton
                mybutton7.isHidden = true
                // focus image view
                let mybutton8 = self.view.viewWithTag(9980) as! UIButton
                mybutton8.isHidden = true
                
                //up slider button outlet
                let mybutton9 = self.view.viewWithTag(9981) as! UIButton
                mybutton9.isHidden = true
                
                //down slider button outlet
                let mybutton10 = self.view.viewWithTag(9982) as! UIButton
                mybutton10.isHidden = true
            }
            //ISOSlider.isHidden = false
            if dataHandlerButton.getIntroductionSettings(key: 9995).hasShown == false{
                let mybutton3 = self.view.viewWithTag(9995) as! UIButton
                mybutton3.isHidden = false
            }else {
                let mybutton6 = self.view.viewWithTag(9995) as! UIButton
                mybutton6.isHidden = true
            }
            
            if dataHandlerButton.getIntroductionSettings(key: 9995).hasShown == false{
                let button21 = self.view.viewWithTag(9995)
                button21?.isHidden = false
            }else {
                let button21 = self.view.viewWithTag(9995)
                button21?.isHidden = true
            }
            //timelaps hidden in rifle scope
            
            
        }
    }
    
    
    private func loadPlistFileOfSessionPreset() {
        
        if let fileUrl = Bundle.main.url(forResource: "SessionPressetList", withExtension: "plist"), let data = try? Data(contentsOf: fileUrl) {
            
            if let result = try? PropertyListSerialization.propertyList(from: data, options: [], format: nil) as? [[String: Any]] {
                
                if let validResult = result {
                    
                    for sessionData in validResult {
                        sessionQualityArray.append(SessionPresetModel(data: sessionData as NSDictionary))
                    }
                    
                    videoQualityCollectionView.reloadData()
                }
            }
        }
        
        if let fileUrl = Bundle.main.url(forResource: "SessionPhotoPressetList", withExtension: "plist"),
            let data = try? Data(contentsOf: fileUrl) {
            if let result = try? PropertyListSerialization.propertyList(from: data, options: [], format: nil) as? [[String: Any]] {
                if let validResult = result {
                    for sessionData in validResult {
                        sessionPhotoQualityArray.append(SessionPresetModel(data: sessionData as NSDictionary))
                    }
                    
                    videoQualityCollectionView.reloadData()
                }
            }
        }
    }
    
    
    
    func stopIndicatorFunction (){
        SVProgressHUD.dismiss()
        coverView.isHidden = true
        // checkVolume = false
        self.snapShotTimer1.invalidate()
    }
    
    func volumeChanged(_ notification : Notification) {
        
        if let userInfo = notification.userInfo {
            
            if let volumeChangeType = userInfo["AVSystemController_AudioVolumeChangeReasonNotificationParameter"] as? String {
                if SVProgressHUD.isVisible(){
                    return
                }
                
                if volumeChangeType == "ExplicitVolumeChange" {
                    if captureModeTag == AVCamManualCaptureMode.photo.rawValue {
                        
                        if snapShotTimerLimit > 0 {
                            if snapShotToggleProperty {
                                startCameraSnapShotTimer()
                                snapShotToggleProperty = !snapShotToggleProperty
                            }else {
                                stopCameraSnapShotTimer()
                                snapShotToggleProperty = !snapShotToggleProperty
                            }
                        } else {
                            if #available(iOS 10.0, *) {
                                
                                self.capturePhoto(nil)
                                
                                
                            } else {
                                // toggleMovieRecording("Record")
                                //  self.snapStillImage()
                            }
                        }
                    }else{
                        if #available(iOS 10.0, *) {
                            if checkVolume == true{
                                if isTimeLapsReady {
                                    start_OR_Stop_TimeLapsVideo()
                                }else{
                                    toggleMovieRecording("Record")
                                    checkVolume = false
                                }
                                
                            }else{
                                if isTimeLapsReady {
                                    start_OR_Stop_TimeLapsVideo()
                                }else{
                                    toggleMovieRecording("Record")
                                    checkVolume = true
                                }
                            }
                            
                        } else {
                            checkVolume = false
                            toggleMovieRecording("Record")
                        }
                    }
                }else{
                    
                    print("volume working ... ")
                    
                }
            }
        }
        
    }
    
}

//MARK: Utilities

extension AVCaptureDevice.FocusMode: CustomStringConvertible {
    public var description: String {
        var string: String
        
        switch self {
        case .locked:
            string = "Locked"
        case .autoFocus:
            string = "Auto"
        case .continuousAutoFocus:
            string = "ContinuousAuto"
        }
        
        return string
    }
}

extension AVCaptureDevice.ExposureMode: CustomStringConvertible {
    public var description: String {
        var string: String
        
        switch self {
        case .locked:
            string = "Locked"
        case .autoExpose:
            string = "Auto"
        case .continuousAutoExposure:
            string = "ContinuousAuto"
        case .custom:
            string = "Custom"
        }
        
        return string
    }
}

extension AVCaptureDevice.WhiteBalanceMode: CustomStringConvertible {
    public var description: String {
        var string: String
        
        switch self {
        case .locked:
            string = "Locked"
        case .autoWhiteBalance:
            string = "Auto"
        case .continuousAutoWhiteBalance:
            string = "ContinuousAuto"
        }
        
        return string
    }
}

extension AVCamManualCameraViewController {
    func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
}








extension AVCamManualCameraViewController : UITextFieldDelegate {
    
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.startDateInterval = Date().timeIntervalSince1970
        let touch:UITouch = touches.first!
        
        if touch.view != filterCollectionView
        {
            filterCollectionView.isHidden=true
        }
        
        if touch.view != topContainerSubTimerView
        {
            topContainerSubTimerView.isHidden=true
        }
        
        
        if touch.view != topContainerSubFlushView
        {
            topContainerSubFlushView.isHidden=true
        }
        if touch.view != settingContainerView
        {
            
        }
        
        if touch.view != manualHUDFocusView
        {
            manualHUDFocusView.isHidden=true
        }
        
        if touch.view != manualHUDExposureView
        {
            manualHUDExposureView.isHidden=true
        }
        
        if touch.view != geoTagView
        {
            geoTagView.isHidden=true
        }
        
        if touch.view != stabilizationView
        {
            stabilizationView.isHidden=true
        }
        
        if touch.view != fileTypeView
        {
            fileTypeView.isHidden=true
        }
        if touch.view != photoQualityCollectionView
        {
            photoQualityCollectionView.isHidden=true
        }
        if touch.view != videoQualityCollectionView
        {
            videoQualityCollectionView.isHidden=true
        }
        let circleCenter = touch.location(in: view)
        
        if lastRectangle != nil{
            removeRectangle()
        }
        
        if lastBigRectangle != nil{
            removeBigRectangle()
        }
        
        if touch.view == previewView || touch.view == _glkView
        {
            if selectedScope == true {
                
            }else{
                showRectangle(centerCircle: circleCenter, width: 50, height: 50)
                
            }
        }
        
        
        if videoDevice?.position == .front{
        }else{
            try? videoDevice?.lockForConfiguration()
            videoDevice?.focusPointOfInterest = touch.location(in: view)
            videoDevice?.unlockForConfiguration()
        }
    }
    
    func showRectangle (centerCircle: CGPoint, width: CGFloat, height: CGFloat) {
        let circleView = CustomCircle(frame: CGRect(x: centerCircle.x-25, y: centerCircle.y-25, width: width , height: height ))
        circleView.backgroundColor = UIColor.clear
        view.addSubview(circleView)
        lastRectangle = circleView
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            self.removeRectangle()
            
        })
    }
    
    func showRectangleBig (centerCircle: CGPoint, width: CGFloat, height: CGFloat) {
        let rectangleView = CustomCircle(frame: CGRect(x: centerCircle.x-25, y: centerCircle.y-25, width: width , height: height ))
        rectangleView.backgroundColor = UIColor.clear
        view.addSubview(rectangleView)
        lastBigRectangle = rectangleView
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            self.removeBigRectangle()
            
        })
    }
    
    func removeRectangle()
    {
        lastRectangle?.removeFromSuperview()
        lastRectangle = nil
    }
    
    func removeBigRectangle()
    {
        lastBigRectangle?.removeFromSuperview()
        lastBigRectangle = nil
    }
    
    @objc func hideZoomLabel()
    {
        zoomLabel.isHidden = true
    }
    
    @objc func hideBurstShotCounter()
    {
        burstShotView.isHidden = true
    }
    
    func sessionPressetConfiguration() {
        
        if captureModeTag == AVCamManualCaptureMode.photo.rawValue {
            self.session.beginConfiguration()
            if self.session.canSetSessionPreset(AVCaptureSession.Preset(rawValue: self.photoQuality)) {
                self.session.sessionPreset = AVCaptureSession.Preset(rawValue: self.photoQuality)
            }else {
                self.photoQuality = AVCaptureSession.Preset.photo.rawValue
                self.session.sessionPreset = AVCaptureSession.Preset(rawValue: self.photoQuality)
                
            }
            self.session.commitConfiguration()
        }else if captureModeTag == AVCamManualCaptureMode.movie.rawValue {
            self.session.beginConfiguration()
            
            if self.session.canSetSessionPreset(AVCaptureSession.Preset(rawValue: self.photoQuality)) {
                self.session.sessionPreset = AVCaptureSession.Preset(rawValue: self.photoQuality)
            }else {
                self.photoQuality = AVCaptureSession.Preset.photo.rawValue
                self.session.sessionPreset = AVCaptureSession.Preset(rawValue: self.photoQuality)
            }
            self.session.commitConfiguration()
        }
    }
}


//MARK: - GEO TAGGING SECTION
extension AVCamManualCameraViewController : LocationServiceDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let currentLocation = locations.last!
        print("Current location: \(currentLocation)")
        let lat = NSNumber(value: currentLocation.coordinate.latitude)
        let lon = NSNumber(value: currentLocation.coordinate.longitude)
        let userLocation: NSDictionary = ["lat": lat, "long": lon]
        UserDefaults.standard.setValue(userLocation, forKey: UserDefaultKey.location.rawValue)
        UserDefaults.standard.synchronize()
        AppDelegate.IS_GEO_TAG_ON = true
    }
    
    // 2
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
}
extension AVCamManualCameraViewController  {
    
    @IBAction  func settingViewAppearFunction(_ sender : UIButton) {
        
        self.photoMenuSV.isHidden = true
        self.videoMenu.isHidden = true
        
        print(self.videoQuality)
        for obj in sessionQualityArray {
            print(obj._pressetName)
            if obj._pressetName == self.videoQuality {
                // highlight selected video quality here
            }
        }
        
        for objPhoto in sessionPhotoQualityArray {
            
            if objPhoto._pressetName == self.photoQuality {
                
                // high light selected photo quality here
            }
        }
    }
    
    @IBAction  func settingViewDisAppearFunction(_ sender : UIButton) {
        UIView.animate(withDuration: 0.7) { [weak self] in
            
        }
    }
    
    public func stopButtonTapped() {
        locationMgr.stopUpdatingLocation()
        UserDefaults.standard.setValue(nil, forKey: UserDefaultKey.location.rawValue)
        UserDefaults.standard.synchronize()
        AppDelegate.IS_GEO_TAG_ON = false
        
    }
    
    // MARK: LocationService Delegate
    func tracingLocation(_ currentLocation: CLLocation) {
        
        print("got new location")
        
        let lat = NSNumber(value: currentLocation.coordinate.latitude)
        let lon = NSNumber(value: currentLocation.coordinate.longitude)
        let userLocation: NSDictionary = ["lat": lat, "long": lon]
        UserDefaults.standard.setValue(userLocation, forKey: UserDefaultKey.location.rawValue)
        UserDefaults.standard.synchronize()
        AppDelegate.IS_GEO_TAG_ON = true
        
    }
    
    func tracingLocationDidFailWithError(_ error: NSError) {
        EZAlertController.alert(error.localizedDescription)
        print("tracing Location Error : \(error.description)")
    }
}

//MARK:" - Slow Motion Recording Section
extension AVCamManualCameraViewController {
    
    func removeMovieFileOutput() {
        
        self.sessionQueue.async {
            if self.movieFileOutput != nil {
                self.session.beginConfiguration()
                self.session.removeOutput(self.movieFileOutput!)
                self.session.commitConfiguration()
            }
        }
    }
    
    func switchFormat(withDesiredFPS desiredFPS: Float64) {
        let isRunning: Bool = self.session.isRunning
        
        if isRunning {
            self.session.stopRunning()
        }
        
        let videoDevice = AVCaptureDevice.default(for: AVMediaType.video)
        var selectedFormat: AVCaptureDevice.Format? = nil
        let maxHeight: Int32 = 720
        
        for format in (videoDevice?.formats)! {
            for range in (format as! AVCaptureDevice.Format).videoSupportedFrameRateRanges {
                let desc = (format as AnyObject).formatDescription!
                let dimensions: CMVideoDimensions = CMVideoFormatDescriptionGetDimensions(desc)
                let height: Int32 = dimensions.height
                
                print("width of pixle :::::: \(dimensions.width) && hight of pixle ::::: \(dimensions.height)")
                
                if (range as AnyObject).minFrameRate <= desiredFPS && desiredFPS <= (range as AnyObject).maxFrameRate && height == maxHeight {
                    selectedFormat = format as? AVCaptureDevice.Format
                }
            }
        }
        
        if (selectedFormat != nil) {
            if ((try? videoDevice?.lockForConfiguration()) != nil) {
                print("selected format:\(String(describing: selectedFormat))")
                videoDevice?.activeFormat = selectedFormat!
                videoDevice?.activeVideoMinFrameDuration = CMTimeMake(1, Int32(desiredFPS))
                videoDevice?.activeVideoMaxFrameDuration = CMTimeMake(1, Int32(desiredFPS))
                videoDevice?.unlockForConfiguration()
            }
        }
        if isRunning {
            self.session.startRunning()
        }
        
        print(session.sessionPreset)
        
        self.ISOSlider.minimumValue = self.videoDevice?.activeFormat.minISO ?? 0.0
        self.ISOSlider.maximumValue = self.videoDevice?.activeFormat.maxISO ?? 0.0
        
        self.isoSlider.minimumValue = self.videoDevice?.activeFormat.minISO ?? 0.0
        self.isoSlider.maximumValue = self.videoDevice?.activeFormat.maxISO ?? 0.0
        self.isoSlider.value = self.videoDevice?.iso ?? 0.0
        self.ISOSlider.value = self.videoDevice?.iso ?? 0.0
        self.ISOValueLabel.text = String(Int((self.ISOSlider.value)))
    }
    
    func resetFormat() {
        
        
        let isRunning: Bool = self.session.isRunning
        if isRunning {
            
            // self.session.stopRunning()
        }else{
            self.session.stopRunning()
        }
        self.session.beginConfiguration()
        //  try? videoDevice?.lockForConfiguration()
        //  videoDevice?.activeFormat = self.defaultFormat
        // videoDevice?.activeVideoMaxFrameDuration = self.videoDevice?.activeVideoMaxFrameDuration
        //  videoDevice?.unlockForConfiguration()
        self.session.commitConfiguration()
    }
}


class Logger{
    
    class func log(_ message: String,
                   function: String = #function,
                   file: String = #file,
                   line: Int = #line) {
        var filename = file
        if let match = filename.range(of: "[^/]*$", options: .regularExpression) {
            filename = filename.substring(with: match)
        }
        print("\(Date().timeIntervalSince1970):\(filename):L\(line):\(function) \"\(message)\"")
    }
}

