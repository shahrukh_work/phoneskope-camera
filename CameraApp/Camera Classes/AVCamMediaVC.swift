//
//  AVCamMediaVC.swift
//  CameraApplication
//
//  Created by Ali Apple on 8/18/17.
//  Copyright © 2017 softech. All rights reserved.
//

import Photos
import AVKit
import AVFoundation
import CoreLocation

import UIKit

@objcMembers class AVCamMediaVC: UIViewController {
    
    @IBOutlet fileprivate weak var phAssetCollectionView : UICollectionView!
    
    @IBOutlet fileprivate weak var topBarView : UIView!
    
    @IBOutlet fileprivate weak var bottomBarView : UIView!

    //fileprivate let dateFormatter = DateFormatter()

    var cellsPerRow:CGFloat = 1
    let cellPadding:CGFloat = 5
    @IBOutlet weak var titleDayLbl: UILabel!
    
    @IBOutlet weak var titleTimeLble: UILabel!
    
    //MARK :- Properties
    fileprivate var results = PHFetchResult<PHAsset>()
    fileprivate let imageManager = PHImageManager.default()
    fileprivate let imageCaheManager = PHCachingImageManager.init()
    
    fileprivate var toggleProperty = false
    var player : AVPlayer?
    var playerLayer : AVPlayerLayer?
    var currentPItem : AVPlayerItem?
    let dataHandler = DataHandler()
    
    var currentCell : UICollectionViewCell?
    var playButton : UIButton?
    var url1 : URL!
    let geoCoder = CLGeocoder()
    fileprivate let dateFormatter = DateFormatter()
    var tagretSize : CGSize!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(rotated), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
        
        let screen = UIScreen.main.bounds.size
        
        print("width :: \(screen.width) height :: \(screen.height)")
        
        tagretSize = CGSize(width: screen.width, height: screen.height)
        
        dateFormatter.dateFormat = "hh:mm a"
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.playerDidFinishPlaying(note:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
        
        getPhotosFromAlbum()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        let item = self.collectionView(self.phAssetCollectionView!, numberOfItemsInSection: 0) - 1
        
        if item > -1 {
            let lastItemIndex = IndexPath(item: item, section: 0)
            self.phAssetCollectionView?.scrollToItem(at: lastItemIndex, at: UICollectionViewScrollPosition.right, animated: false)
        }
        
    }

    @IBAction func dismissCurrentVC(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func allPhotoFunction(_ sender: UIButton) {
       
        let appURLScheme = "Photos://"
        
        guard let appURL = URL(string: appURLScheme) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(appURL) {
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(appURL)
            }
            else {
                UIApplication.shared.openURL(appURL)
            }
            url1 = appURL
        }
    }
    
    
//    override var shouldAutorotate: Bool {
//        return false
//    }
//    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
//        if UIDeviceOrientation.portrait == .portrait {
//        return .portrait
//        }else{
//            return .landscape
//
//        }
//    }
    
    fileprivate func cellLayOutSetup() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let screenSize = UIScreen.main.bounds.size
        layout.itemSize = CGSize(width: screenSize.width, height: screenSize.height)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        phAssetCollectionView?.collectionViewLayout = layout
    }
    

    fileprivate func getPhotosFromAlbum() {
        //cellLayOutSetup()
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
        fetchOptions.predicate = NSPredicate(format: "mediaType == %d || mediaType == %d",
                                             PHAssetMediaType.image.rawValue,
                                             PHAssetMediaType.video.rawValue)
       
        
        results = PHAsset.fetchAssets(with: fetchOptions)
        
        self.phAssetCollectionView.reloadData()
    }
    
    func rotated() {
        
//        let screen = UIScreen.main.bounds.size
//        tagretSize = CGSize(width: screen.width, height: screen.height)
//        phAssetCollectionView!.collectionViewLayout.invalidateLayout()
//        if playerLayer != nil {
//            playerLayer?.frame = CGRect(x: 0, y: 0, width: screen.width, height: screen.height)
//        }
//        phAssetCollectionView.reloadData()
    }
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
   
    
    deinit {
        print("De_Init called AVCamMediaVC")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
        
        NotificationCenter.default.removeObserver(self)
    }
}

//MARK: UICOLLECTION VIEW SECTION
extension AVCamMediaVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return results.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! AVCameCustomCell
        
        let asset = results[indexPath.row]
        
        cell.identifier = asset.localIdentifier

        if asset.mediaType == .video {
            cell.playBtn?.isHidden = false
            cell.playBtn?.addTarget(self, action: #selector(prepareVideoForGivenAsset(_:)), for: .touchUpInside)
        }else {
            cell.playBtn?.isHidden = true
        }
        
        
        if let title = asset.creationDate?.dayOfTheWeek() {
            self.titleDayLbl.text = title
        }
        
        imageCaheManager.requestImage(for: asset, targetSize: tagretSize, contentMode: .aspectFit, options: nil) {(result, info) in
            if cell.identifier == asset.localIdentifier {
                cell.imgView.image = result
            }
        }
        
        
        return cell
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator)
        let offset = self.phAssetCollectionView?.contentOffset;
        let width  = self.phAssetCollectionView?.bounds.size.width;
        
        let index     = round(offset!.x / width!);
        let newOffset = CGPoint(x: index * size.width, y: offset!.y)
        
        self.phAssetCollectionView?.setContentOffset(newOffset, animated: false)
        
        
        coordinator.animate(alongsideTransition: { (context) in
            self.phAssetCollectionView?.reloadData()
            self.phAssetCollectionView?.setContentOffset(newOffset, animated: false)
        }, completion: nil)
    }

    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events

        topBottomBarHiddenToggleFunction()
    }
   
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        cellsPerRow = (traitCollection.verticalSizeClass == .compact) ? 1 : 1
        phAssetCollectionView.reloadData()
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        
        return CGSize(width: phAssetCollectionView.bounds.width,
                      height: phAssetCollectionView.bounds.height)
    }
    
    fileprivate func topBottomBarHiddenToggleFunction() {
        toggleProperty = !toggleProperty
        if toggleProperty {
            UIView.animate(withDuration: 0.3, animations: {[weak self] in
                self?.topBarView.isHidden = true
                self?.bottomBarView.isHidden = true
            })
        }else {
            UIView.animate(withDuration: 0.3, animations: {[weak self] in
                self?.topBarView.isHidden = false
                self?.bottomBarView.isHidden = false
            })
        }
    }
    
    @objc fileprivate func prepareVideoForGivenAsset(_ sender : UIButton) {
        
        if let cell = sender.superview?.superview as? UICollectionViewCell {
            if let index = phAssetCollectionView.indexPath(for: cell) {
                let selectedAsset = results[index.row]
                
                if selectedAsset.mediaType == .video {
                    imageManager.requestPlayerItem(forVideo: selectedAsset, options: nil, resultHandler: { (playerItem, nil) in
                        DispatchQueue.main.async {[weak self] in
                            self?.playButton = sender
                        
                            self?.playVideo(playerItem, cell: cell)
                        }
                    })
                }
            }
        }
    }
    
    fileprivate func playVideo(_ pItem : AVPlayerItem?, cell : UICollectionViewCell) {
        
        if pItem != nil {
            currentPItem = pItem
            player = AVPlayer(playerItem: pItem)
            playerLayer = AVPlayerLayer(player: player)
            playerLayer?.frame = cell.bounds
            currentCell = cell
            cell.layer.addSublayer(playerLayer!)
            player?.play()
      
            
        }
    }
    
    func playerDidFinishPlaying(note: NSNotification){
        print("Video Finished")
        if playerLayer != nil {
            playerLayer?.removeFromSuperlayer()
            playerLayer = nil
        }
    }
}

extension Date {
    
    func dayOfTheWeek() -> String? {
        let weekdays = [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Satudrday,"
        ]
        
        let calendar: NSCalendar = NSCalendar.current as NSCalendar
        let components: NSDateComponents = calendar.components(.weekday, from: self as Date) as NSDateComponents
        return weekdays[components.weekday - 1]
    }
}


