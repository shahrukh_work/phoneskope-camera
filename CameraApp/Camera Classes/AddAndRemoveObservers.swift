//
//  AddAndRemoveObservers.swift
//  CameraApp
//
//  Created by iMac on 07/09/2018.
//  Copyright © 2018 softech. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Photos
import AssetsLibrary
import CoreLocation
import MediaPlayer ////Only for hidding  Volume view
import GLKit
import CoreImage
import SVProgressHUD
import LLVideoEditor
import NVActivityIndicatorView

extension AVCamManualCameraViewController{
    //MARK: KVO and Notifications
    
     func addObservers() {
        self.addObserver(self, forKeyPath: "session.running", options: .new, context: &SessionRunningContext)
        self.addObserver(self, forKeyPath: "videoDevice.focusMode", options: [.old, .new], context: &FocusModeContext)
        self.addObserver(self, forKeyPath: "videoDevice.lensPosition", options: .new, context: &LensPositionContext)
        self.addObserver(self, forKeyPath: "videoDevice.exposureMode", options: [.old, .new], context: &ExposureModeContext)
        self.addObserver(self, forKeyPath: "videoDevice.exposureDuration", options: .new, context: &ExposureDurationContext)
        self.addObserver(self, forKeyPath: "videoDevice.ISO", options: .new, context: &ISOContext)
        self.addObserver(self, forKeyPath: "videoDevice.exposureTargetBias", options: .new, context: &ExposureTargetBiasContext)
        self.addObserver(self, forKeyPath: "videoDevice.exposureTargetOffset", options: .new, context: &ExposureTargetOffsetContext)
        self.addObserver(self, forKeyPath: "videoDevice.whiteBalanceMode", options: [.old, .new], context: &WhiteBalanceModeContext)
        self.addObserver(self, forKeyPath: "videoDevice.deviceWhiteBalanceGains", options: .new, context: &DeviceWhiteBalanceGainsContext)
        
        if #available(iOS 10.0, *) {
        } else {
            self.addObserver(self, forKeyPath: "stillImageOutput.capturingStillImage", options: .new, context: &CapturingStillImageContext)
            self.addObserver(self, forKeyPath: "stillImageOutput.lensStabilizationDuringBracketedCaptureEnabled", options: [.old, .new], context: &LensStabilizationContext)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(subjectAreaDidChange), name: .AVCaptureDeviceSubjectAreaDidChange, object: self.videoDevice!)
        NotificationCenter.default.addObserver(self, selector: #selector(sessionRuntimeError), name: .AVCaptureSessionRuntimeError, object: self.session)
        // A session can only run when the app is full screen. It will be interrupted in a multi-app layout, introduced in iOS 9,
        // see also the documentation of AVCaptureSessionInterruptionReason. Add observers to handle these session interruptions
        // and show a preview is paused message. See the documentation of AVCaptureSessionWasInterruptedNotification for other
        // interruption reasons.
        if #available(iOS 9.0, *) {
            NotificationCenter.default.addObserver(self, selector: #selector(sessionWasInterrupted(_:)), name: .AVCaptureSessionWasInterrupted, object: self.session)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(sessionInterruptionEnded(_:)), name: .AVCaptureSessionInterruptionEnded, object: self.session)
    }
    
     func removeObservers() {
        NotificationCenter.default.removeObserver(self)
        self.removeObserver(self, forKeyPath: "session.running", context: &SessionRunningContext)
        self.removeObserver(self, forKeyPath: "videoDevice.focusMode", context: &FocusModeContext)
        self.removeObserver(self, forKeyPath: "videoDevice.lensPosition", context: &LensPositionContext)
        self.removeObserver(self, forKeyPath: "videoDevice.exposureMode", context: &ExposureModeContext)
        self.removeObserver(self, forKeyPath: "videoDevice.exposureDuration", context: &ExposureDurationContext)
        
        self.removeObserver(self, forKeyPath: "videoDevice.ISO", context: &ISOContext)
        self.removeObserver(self, forKeyPath: "videoDevice.exposureTargetBias", context: &ExposureTargetBiasContext)
        self.removeObserver(self, forKeyPath: "videoDevice.exposureTargetOffset", context: &ExposureTargetOffsetContext)
        self.removeObserver(self, forKeyPath: "videoDevice.whiteBalanceMode", context: &WhiteBalanceModeContext)
        self.removeObserver(self, forKeyPath: "videoDevice.deviceWhiteBalanceGains", context: &DeviceWhiteBalanceGainsContext)
        
        if #available(iOS 10.0, *) {
        } else {
            self.removeObserver(self, forKeyPath: "stillImageOutput.capturingStillImage", context: &CapturingStillImageContext)
            self.removeObserver(self, forKeyPath: "stillImageOutput.lensStabilizationDuringBracketedCaptureEnabled", context: &LensStabilizationContext)
            
            self.removeObserver(self, forKeyPath: KeyPath.volum.rawValue, context: &VolumModeContext)
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        let oldValue = change![.oldKey]
        let newValue = change![.newKey]
        
        guard let context = context else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: nil)
            return
        }
        switch context {
        case &FocusModeContext:
            if let value = newValue as? Int {
                let newMode = AVCaptureDevice.FocusMode(rawValue: value)!
                DispatchQueue.main.async {
                    self.focusModeControl.selectedSegmentIndex = self.focusModes.index(of: newMode)!
                    //   self.lensPositionSlider.isEnabled = (newMode == .locked)
                    
                    if let old = oldValue as? Int {
                        let oldMode = AVCaptureDevice.FocusMode(rawValue: old)!
                        NSLog("focus mode: \(oldMode) -> \(newMode)")
                    } else {
                        NSLog("focus mode: \(newMode)")
                    }
                }
            }
        case &LensPositionContext:
            if let value = newValue as? Float {
                let focusMode = self.videoDevice!.focusMode
                let newLensPosition = value
                
                DispatchQueue.main.async {
                    if focusMode != .locked {
                        
                    }
                    
                   // self.lensPositionValueLabel.text = String(format: "%.1f", Double(newLensPosition))
                }
            }
        case &ExposureModeContext:
            if let value = newValue as? Int {
                let newMode = AVCaptureDevice.ExposureMode(rawValue: value)!
                if let old = oldValue as? Int {
                    let oldMode = AVCaptureDevice.ExposureMode(rawValue: old)!
                    /*
                     It’s important to understand the relationship between exposureDuration and the minimum frame rate as represented by activeVideoMaxFrameDuration.
                     In manual mode, if exposureDuration is set to a value that's greater than activeVideoMaxFrameDuration, then activeVideoMaxFrameDuration will
                     increase to match it, thus lowering the minimum frame rate. If exposureMode is then changed to automatic mode, the minimum frame rate will
                     remain lower than its default. If this is not the desired behavior, the min and max frameRates can be reset to their default values for the
                     current activeFormat by setting activeVideoMaxFrameDuration and activeVideoMinFrameDuration to kCMTimeInvalid.
                     */
                    if oldMode != newMode && oldMode == .custom {
                        do {
                            try self.videoDevice!.lockForConfiguration()
                            defer {self.videoDevice!.unlockForConfiguration()}
                            self.videoDevice!.activeVideoMaxFrameDuration = kCMTimeInvalid
                            self.videoDevice!.activeVideoMinFrameDuration = kCMTimeInvalid
                        } catch let error {
                            NSLog("Could not lock device for configuration: \(error)")
                        }
                    }
                }
                DispatchQueue.main.async {
                    
                  //  self.exposureModeControl.selectedSegmentIndex = self.exposureModes.index(of: newMode)!
                  //  self.exposureDurationSlider.isEnabled = (newMode == .custom)
                    
                    if let old = oldValue as? Int {
                        let oldMode = AVCaptureDevice.ExposureMode(rawValue: old)!
                        NSLog("exposure mode: \(oldMode) -> \(newMode)")
                    } else {
                        NSLog("exposure mode: \(newMode)")
                    }
                }
            }
        case &ExposureDurationContext:
            // Map from duration to non-linear UI range 0-1
            
            if let value = newValue as? CMTime {
                let newDurationSeconds = CMTimeGetSeconds(value)
                let exposureMode = self.videoDevice!.exposureMode
                
                let minDurationSeconds = max(CMTimeGetSeconds(self.videoDevice!.activeFormat.minExposureDuration), kExposureMinimumDuration)
                let maxDurationSeconds = CMTimeGetSeconds(self.videoDevice!.activeFormat.maxExposureDuration)
                // Map from duration to non-linear UI range 0-1
                let p = (newDurationSeconds - minDurationSeconds) / (maxDurationSeconds - minDurationSeconds) // Scale to 0-1
                DispatchQueue.main.async {
                    if exposureMode != .custom {
                        self.exposureDurationSlider.value = Float(pow(p, 1 / self.kExposureDurationPower)) // Apply inverse power
                    }
                    if newDurationSeconds < 1 {
                        let digits = max(0, 2 + Int(floor(log10(newDurationSeconds))))
                        self.exposureDurationValueLabel.text = String(format: "1/%.*f", digits, 1/newDurationSeconds)
                    } else {
                        self.exposureDurationValueLabel.text = String(format: "%.2f", newDurationSeconds)
                    }
                }
            }
        case &ISOContext:
            if let value = newValue as? Float {
                let newISO = value
                let exposureMode = self.videoDevice!.exposureMode
                
                DispatchQueue.main.async {
                    if exposureMode != .custom {
                        //changehere
                        self.ISOSlider.value = newISO
                        self.isoSlider.value = newISO
                    }
                    self.ISOValueLabel.text = String(Int(newISO))
                }
            }
        case &ExposureTargetBiasContext:
            if let value = newValue as? Float {
                let newExposureTargetBias = value
                DispatchQueue.main.async {
                    self.exposureTargetBiasValueLabel.text = String(format: "%.1f", Double(newExposureTargetBias))
                }
            }
        case &ExposureTargetOffsetContext:
            print("")
            
        case &WhiteBalanceModeContext:
            break;
        case &DeviceWhiteBalanceGainsContext:
            break;
        case &SessionRunningContext:
            var isRunning = false
            if let value = newValue as? Bool {
                isRunning = value
            }
            
        case &CapturingStillImageContext:
            if #available(iOS 10.0, *) {
            } else {
                var isCapturingStillImage = false
                if let value = newValue as? Bool {
                    isCapturingStillImage = value
                }
                
                if isCapturingStillImage {
                    DispatchQueue.main.async {
                        self.previewView.layer.opacity = 0.0
                        UIView.animate(withDuration: 0.25, animations: {
                            self.previewView.layer.opacity = 1.0
                        })
                    }
                }
            }
        case &LensStabilizationContext:
            print("")
            //            if #available(iOS 10.0, *) {
            //            } else {
            //                if let value = newValue as? Bool {
            //                    let newMode = value
            //                    self.lensStabilizationControl.selectedSegmentIndex = (newMode ? 1 : 0)
            //                    if let old = oldValue as? Bool {
            //                        let oldMode = old
            //                        NSLog("Lens stabilization: %@ -> %@", (oldMode ? "YES" : "NO"), (newMode ? "YES" : "NO"))
            //                    }
            //                }
        //            }
        case &VolumModeContext:
            DispatchQueue.main.async {[weak self] in
                self?.capturePhoto(nil)
            }
            
        default:
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
}
