//
//  AVCameCustomCell.swift
//  CameraApplication
//
//  Created by Ali Apple on 8/24/17.
//  Copyright © 2017 softech. All rights reserved.
//

import UIKit

class AVCameCustomCell: UICollectionViewCell {
    @IBOutlet var imgView : UIImageView!
    @IBOutlet var identifier : String!
    @IBOutlet var playBtn : UIButton!
}
