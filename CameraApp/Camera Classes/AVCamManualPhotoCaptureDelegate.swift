//
//  AVCamManualPhotoCaptureDelegate.swift
//  AVCamManual
//
//  Created by 開発 on 2016/12/27.
//
//
/*
	Copyright (C) 2016 Apple Inc. All Rights Reserved.
	See LICENSE.txt for this sample’s licensing information

	Abstract:
	Photo capture delegate.
*/

import  CoreLocation
import AVFoundation
import Photos
import UIKit

protocol AVCamManualPhotoCaptureDelegateType: class {}
@available(iOS 10.0, *)
@objc(AVCamManualPhotoCaptureDelegate)
class AVCamManualPhotoCaptureDelegate: NSObject, AVCapturePhotoCaptureDelegate, AVCamManualPhotoCaptureDelegateType {
    
    private(set) var requestedPhotoSettings: AVCapturePhotoSettings
    
    var willCapturePhotoAnimation: ()->Void
    
    var completed: (AVCamManualPhotoCaptureDelegate)->Void
    
    var jpegPhotoData: Data?
    
    var dngPhotoData: Data?
    
    var isFilterMode : CIFilter?
    
    var videoOrientation : AVCaptureVideoOrientation!
    
    var videoDevice : AVCaptureDevice!
    
    var SampleBuffer: CMSampleBuffer?
    
    
    
    init(requestedPhotoSettings: AVCapturePhotoSettings, isFilter : CIFilter?, videoOrientation : AVCaptureVideoOrientation, videoDevice : AVCaptureDevice, willCapturePhotoAnimation: @escaping ()->Void, completed: @escaping (AVCamManualPhotoCaptureDelegate)->Void) {
        self.requestedPhotoSettings = requestedPhotoSettings
        self.willCapturePhotoAnimation = willCapturePhotoAnimation
        self.isFilterMode = isFilter
        self.videoOrientation = videoOrientation
        self.videoDevice = videoDevice
        self.completed = completed        
    }
    
    func didFinish() {
        self.completed(self)
    }
    
    func photoOutput(_ captureOutput: AVCapturePhotoOutput, willCapturePhotoFor resolvedSettings: AVCaptureResolvedPhotoSettings) {
        self.willCapturePhotoAnimation()
    }
    

    func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        if let error = error {
            NSLog("Error capturing photo: \(error)")
            return
        }

        self.jpegPhotoData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: photoSampleBuffer!, previewPhotoSampleBuffer: previewPhotoSampleBuffer!)
    }
    
    func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingRawPhoto rawSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        if let error = error {
            NSLog("Error capturing RAW photo: \(error)")
        }
        
        self.dngPhotoData = AVCapturePhotoOutput.dngPhotoDataRepresentation(forRawSampleBuffer: rawSampleBuffer!, previewPhotoSampleBuffer: previewPhotoSampleBuffer)
    }
    
    
    func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishCaptureFor resolvedSettings: AVCaptureResolvedPhotoSettings, error: Error?) {
        if let error = error {
            NSLog("Error capturing photo: \(error)")
            self.didFinish()
            return
        }
        
        if self.jpegPhotoData == nil && self.dngPhotoData == nil {
            NSLog("No photo data resource")
            self.didFinish()
            return
        }
        
        
        if (isFilterMode != nil) {
            if let validData = applyFiterOnImageData(self.jpegPhotoData!) {
                self.jpegPhotoData = validData
            }
        }
        
        self.savePhotoToLibrary(resolvedSettings: resolvedSettings)

    }
    
    public func waterMarkingOfPhoto(imgData : Data, watermark : String) -> Data? {
        
        var returnData : Data!
        
        if let img = UIImage(data: imgData) {
            if let photoQuality = UserDefaults.standard.value(forKey: "photoQuality") as? String {
                
                var rightPadding : CGFloat = 25
                var bottomPadding : CGFloat = 120
                var fontSize : CGFloat = 175.0
                
                if photoQuality == "AVCaptureSessionPresetPhoto" {
                    fontSize = 175.0
                    rightPadding = 40
                    bottomPadding = -140
                }else if photoQuality == "AVCaptureSessionPresetMedium" {
                    fontSize = 24
                    rightPadding = 8
                    bottomPadding = -25
                }else {
                    fontSize = 9
                    rightPadding = 3
                    bottomPadding = -10
                }
                
                let elblWidth = img.size.width/2
                let elblHeight = img.size.height/5
                let eXPoz = img.size.width - (elblWidth + rightPadding)
                let eYPoz = img.size.height - (elblHeight + bottomPadding)
                
                let lblFrame = CGRect(x: eXPoz, y: eYPoz, width: elblWidth, height: elblHeight)
                let waterLbl = UILabel(frame: lblFrame)
                
                waterLbl.textAlignment = .right
                waterLbl.text = watermark
                waterLbl.textColor = UIColor.white
                waterLbl.adjustsFontSizeToFitWidth = true
                waterLbl.font = UIFont.systemFont(ofSize: fontSize)
                waterLbl.backgroundColor = UIColor.blue
                
                let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
                
                UIGraphicsBeginImageContextWithOptions(img.size, true, 0)
                let context = UIGraphicsGetCurrentContext()
                
                context!.setFillColor(UIColor.white.cgColor)
                context!.fill(rect)
                img.draw(in: rect, blendMode: .normal, alpha: 1)
                
                waterLbl.drawText(in: CGRect(x: eXPoz, y: eYPoz, width: elblWidth, height: elblHeight))
                
                if let result = UIGraphicsGetImageFromCurrentImageContext() {
                    returnData = UIImageJPEGRepresentation(result, 1.0)
                }
                UIGraphicsEndImageContext()
            }
        }
        
        return returnData
    }
    
    fileprivate func applyFiterOnImageData(_ filterData: Data) -> Data? {
        
        let dataProvider = CGDataProvider(data: filterData as CFData)
        
        let cgImageRef = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.absoluteColorimetric)
        
        
        // Create a Core Image image from the pixel buffer and apply a filter.
        let orientationMap: [AVCaptureVideoOrientation : CGImagePropertyOrientation] = [
            .portrait           : .right,
            .portraitUpsideDown : .left,
            .landscapeLeft      : .down,
            .landscapeRight     : .up,
            ]
        
        let imageOrientation = Int32(orientationMap[videoOrientation]!.rawValue)
        
        let ciImage = CIImage(cgImage: cgImageRef!).oriented(forExifOrientation: imageOrientation)
        
            
        let filteredCIImage = ciImage.applyingFilter(self.isFilterMode!.name, parameters: [:])
        
        
        // Get a JPEG data representation of the filter output.
        let colorSpaceMap: [AVCaptureColorSpace: CFString] = [
            .sRGB   : CGColorSpace.sRGB,
            .P3_D65 : CGColorSpace.displayP3,
            ]
        let colorSpace = CGColorSpace(name: colorSpaceMap[(self.videoDevice?.activeColorSpace)!]!)!
        guard let jpegData = CIContext().jpegRepresentation(of: filteredCIImage, colorSpace: colorSpace) else {
            print("Unable to create filtered JPEG.")
            return nil
        }
        
        return jpegData
    }
    
    fileprivate func savePhotoToLibrary(resolvedSettings: AVCaptureResolvedPhotoSettings) {
        PHPhotoLibrary.requestAuthorization {status in
            if status == .authorized {
                
                var temporaryDNGFileURL: URL!
                if let dngPhotoData = self.dngPhotoData {
                    temporaryDNGFileURL = FileManager.default.temporaryDirectory.appendingPathComponent("\(resolvedSettings.uniqueID).dng")
                    _ = try? dngPhotoData.write(to: temporaryDNGFileURL, options: .atomic)
                }
                
                PHPhotoLibrary.shared().performChanges({
                    let creationRequest = PHAssetCreationRequest.forAsset()
                    
                    if AppDelegate.IS_GEO_TAG_ON {
                        if let validDic = UserDefaults.standard.value(forKey: UserDefaultKey.location.rawValue) as? NSDictionary {
                            
                            if let lat = validDic["lat"] as? Double {
                                if let lng = validDic["long"] as? Double {
                                    let validLoc = CLLocation(latitude: lat, longitude: lng)
                                    creationRequest.location = validLoc
                                }
                            }
                        }
                    }
                    else {
                        creationRequest.location = nil
                    }
                    
                    if let jpegPhotoData = self.jpegPhotoData {
                        var jpgPhotoData = jpegPhotoData
                        if let waterMarkStr = UserDefaults.standard.value(forKey: UserDefaultKey.watermark.rawValue) as? String {
                            DispatchQueue.main.sync {
                                if let imgData = self.waterMarkingOfPhoto(imgData: jpegPhotoData, watermark: waterMarkStr) {
                                    jpgPhotoData = imgData
                                }
                            }
                        }
                        creationRequest.addResource(with: .photo, data: jpgPhotoData, options: nil)
                        if let temporaryDNGFileURL = temporaryDNGFileURL {
                            let companionDNGResourceOptions = PHAssetResourceCreationOptions()
                            companionDNGResourceOptions.shouldMoveFile = true
                            creationRequest.addResource(with: .alternatePhoto, fileURL: temporaryDNGFileURL, options: companionDNGResourceOptions)
                        }
                        
                        
                    } else {
                        let dngResourceOptions = PHAssetResourceCreationOptions()
                        dngResourceOptions.shouldMoveFile = true
                        creationRequest.addResource(with: .photo, fileURL: temporaryDNGFileURL, options: dngResourceOptions)
                    }
                    
                }) {success, error in
                    if !success {
                        NSLog("Error occurred while saving photo to photo library: \(error!)")
                    }
                    
                    if
                        let temporaryDNGFileURL = temporaryDNGFileURL,
                        FileManager.default.fileExists(atPath: temporaryDNGFileURL.path)
                    {
                        _ = try? FileManager.default.removeItem(at: temporaryDNGFileURL)
                    }
                    
                    self.didFinish()
                }
            } else {
                NSLog("Not authorized to save photo")
                self.didFinish()
            }
        }
    }
}
