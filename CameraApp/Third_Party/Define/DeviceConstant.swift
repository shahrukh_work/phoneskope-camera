//
//  DeviceConstant.swift
//  Qaswa
//
//  Created by Umer Sufyan on 07/03/2017.
//  Copyright © 2017 Umer Sufyan. All rights reserved.
//

import UIKit
import Foundation

struct Screen
{
    static let WIDTH        =  UIScreen.main.bounds.size.width
    static let HEIGHT       =  UIScreen.main.bounds.size.height
    static let MAX_LENGTH   =  max(Screen.WIDTH, Screen.HEIGHT)
    static let MIN_LENGTH   =  min(Screen.WIDTH, Screen.HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE         = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_IPHONE_4       = UIDevice.current.userInterfaceIdiom == .phone && Screen.MAX_LENGTH < 568.0
    static let IS_IPHONE_5       = UIDevice.current.userInterfaceIdiom == .phone && Screen.MAX_LENGTH == 568.0
    static let IS_IPHONE_6       = UIDevice.current.userInterfaceIdiom == .phone && Screen.MAX_LENGTH == 667.0
    static let IS_IPHONE_6P      = UIDevice.current.userInterfaceIdiom == .phone && Screen.MAX_LENGTH == 736.0
    static let IS_IPHONE_7       = IS_IPHONE_6
    static let IS_IPHONE_7P      = IS_IPHONE_6P
    static let IS_IPAD           = UIDevice.current.userInterfaceIdiom == .pad && Screen.MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO_9_7   = IS_IPAD
    static let IS_IPAD_PRO_12_9  = UIDevice.current.userInterfaceIdiom == .pad && Screen.MAX_LENGTH == 1366.0
}

struct Version
{
    static let SYS_VERSION_FLOAT  = (UIDevice.current.systemVersion as NSString).floatValue
    static let iOS7               = (Version.SYS_VERSION_FLOAT < 8.0 && Version.SYS_VERSION_FLOAT >= 7.0)
    static let iOS8               = (Version.SYS_VERSION_FLOAT >= 8.0 && Version.SYS_VERSION_FLOAT < 9.0)
    static let iOS9               = (Version.SYS_VERSION_FLOAT >= 9.0 && Version.SYS_VERSION_FLOAT < 10.0)
    static let iOS10              = (Version.SYS_VERSION_FLOAT >= 10.0 && Version.SYS_VERSION_FLOAT < 11.0)
}

struct KeyboardConstant {
    static let KEYBOARD_ANIMATION_DURATION : CGFloat = 0.3
    static let MINIMUM_SCROLL_FRACTION : CGFloat = 0.2;
    static let MAXIMUM_SCROLL_FRACTION : CGFloat = 0.8;
    static let PORTRAIT_KEYBOARD_HEIGHT : CGFloat = 216;
    static let LANDSCAPE_KEYBOARD_HEIGHT : CGFloat = 162;
}
