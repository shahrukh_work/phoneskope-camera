//
//  ScrollViewCornerRadius.swift
//  CameraApp
//
//  Created by iMac on 28/02/2019.
//  Copyright © 2019 softech. All rights reserved.
//

import Foundation
import UIKit

class RoundEdgesScrollView : UIScrollView{
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.cornerRadius = 7
        clipsToBounds = true
    }
}
