//
//  RoundedCornerViews.swift
//  Starter-Project-iOS
//
//  Created by iMac on 25/12/2018.
//  Copyright © 2018 Softak Solutions Pvt Ltd. All rights reserved.
//

import Foundation
import UIKit

class RViews : UIView{
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.cornerRadius = 7.0
        clipsToBounds = true
        dropShadow()
        
    }
    
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = #colorLiteral(red: 0.7472794652, green: 0.7472972274, blue: 0.7472876906, alpha: 0.85)
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 5
        }
    
}

