//
//  ViewCornerRadius.swift
//  CameraApp
//
//  Created by iMac on 29/01/2019.
//  Copyright © 2019 softech. All rights reserved.
//

import Foundation
import UIKit
class RoundEdgesView : UIView{
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.cornerRadius = 7
        clipsToBounds = true
        
    }
}

