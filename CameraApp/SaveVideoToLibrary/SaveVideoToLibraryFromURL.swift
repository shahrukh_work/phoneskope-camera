//
//  SaveVideoToLibraryFromURL.swift
//  CameraApp
//
//  Created by iMac on 02/04/2019.
//  Copyright © 2019 softech. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Photos
import AssetsLibrary


class VideoSave {
    class func saveVideoToPhotoLibrary(_ outputFileURL : URL, error : Error?, video:@escaping (Bool) -> Void) {
            
            let cleanup: ()->() = {
                if FileManager.default.fileExists(atPath: outputFileURL.path) {
                    do {
                        try FileManager.default.removeItem(at: outputFileURL)
                    } catch _ {}
                }
            }
            
            var success = true
            
            if error != nil {
                NSLog("Error occurred while capturing movie: \(error!)")
                success = (error! as NSError).userInfo[AVErrorRecordingSuccessfullyFinishedKey] as? Bool ?? false
            }
            if success {
                // Check authorization status.
                PHPhotoLibrary.requestAuthorization {status in
                    guard status == .authorized else {
                        cleanup()
                    //    self.messageAccessNotGiven(title : "Photo Library Permission required", message : "If you want to save video you need to grant Photo Library Permission.")
                        return
                    }
                    // Save the movie file to the photo library and cleanup.
                    PHPhotoLibrary.shared().performChanges({
                        // In iOS 9 and later, it's possible to move the file into the photo library without duplicating the file data.
                        // This avoids using double the disk space during save, which can make a difference on devices with limited free disk space.
                        if #available(iOS 9.0, *) {
                            let options = PHAssetResourceCreationOptions()
                            options.shouldMoveFile = true
                            let changeRequest = PHAssetCreationRequest.forAsset()
                            changeRequest.addResource(with: .video, fileURL: outputFileURL, options: options)
                        } else {
                            //### Error occurred while capturing movie
                            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: outputFileURL)
                        }
                    }, completionHandler: {success, error in
                        if !success {
                            NSLog("Could not save movie to photo library: \(error!)")
                                video(false)
                        }else {
                            DispatchQueue.main.async {
                                video(true)
                            }
                        }
                        cleanup()
                    })
                }
            } else {
                cleanup()
            }
    }

}
