//
//  ChangeCompostion.swift
//  CameraApp
//
//  Created by iMac on 03/04/2019.
//  Copyright © 2019 softech. All rights reserved.
//

import Foundation
import AVFoundation
import Photos
import AssetsLibrary
import CoreLocation
import MediaPlayer ////Only for hidding  Volume view
import GLKit
import CoreImage
import SVProgressHUD
import LLVideoEditor
import NVActivityIndicatorView


protocol PercentageCheckVideoDelegate {
    func sendPercentage(_ percent: String)
}

class ChangeVideoCompostion {
    
   static var percentDelegate: PercentageCheckVideoDelegate?
    
    class func changeComposition(_ outputFileURL : URL, angle: CGFloat, fileName: String, isSlowMotion: Bool, success:@escaping (Bool, String) -> Void) {
        // SVProgressHUD.show(withStatus:"Saving video for you may take a while.")
        let currentAsset = AVAsset( url: outputFileURL)
        let composition = AVMutableComposition.init()
        
        let videoComposition = AVMutableVideoComposition()
        videoComposition.frameDuration = CMTimeMake(1, 30)
        videoComposition.renderScale  = 1.0
        
        let compositionCommentaryTrack: AVMutableCompositionTrack? = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)
        let compositionVideoTrack: AVMutableCompositionTrack? = composition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        var videoScaleFactor: Double = 5.0
        var videoDuration: CMTime = currentAsset.duration
        let clipVideoTrack:AVAssetTrack =  currentAsset.tracks(withMediaType: AVMediaType.video)[0]
        try? compositionVideoTrack?.insertTimeRange(CMTimeRangeMake(kCMTimeZero, currentAsset.duration ), of: clipVideoTrack, at: kCMTimeZero)
        
        if !isTimeLapsReady {
            let audioTrack: AVAssetTrack = currentAsset.tracks(withMediaType: AVMediaType.audio)[0]
            try? compositionCommentaryTrack?.insertTimeRange(CMTimeRangeMake(kCMTimeZero, currentAsset.duration), of: audioTrack, at: kCMTimeZero)
            
        }
        
        if isSlowMotion{
            compositionCommentaryTrack?.scaleTimeRange(CMTimeRangeMake(kCMTimeZero, videoDuration), toDuration: CMTimeMake(Int64(Double(videoDuration.value) * videoScaleFactor), videoDuration.timescale))
            compositionVideoTrack?.scaleTimeRange(CMTimeRangeMake(kCMTimeZero, videoDuration), toDuration: CMTimeMake(Int64(Double(videoDuration.value) * videoScaleFactor), videoDuration.timescale))
            
        }else{
            
        }
        
        var isPortrait = false
        
        var naturalSize = clipVideoTrack.naturalSize
        
        if !isPortrait
        {
            naturalSize = CGSize.init(width: naturalSize.height, height: naturalSize.width)
        }
        
        videoComposition.renderSize = naturalSize
        let x =  naturalSize.width/2
        let y =  naturalSize.height/2
        
        let scale = CGFloat(1.0)
        
        var transform = CGAffineTransform.init(scaleX: CGFloat(-scale), y: CGFloat(scale))
        
        if UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft {
            transform =  transform.translatedBy(x: -x, y: y)
            transform =  transform.rotated(by:angle)
            transform = transform.translatedBy(x: -y, y:-x )
        } else if UIDevice.current.orientation == UIDeviceOrientation.landscapeRight {
            transform =  transform.translatedBy(x: -x, y: y)
            transform =  transform.rotated(by: angle + 3.1415)
            transform = transform.translatedBy(x: -y, y:-x )
        } else{
            transform =  transform.translatedBy(x: -x, y: y)
            transform =  transform.rotated(by: angle+1.57)
            transform = transform.translatedBy(x: -y, y:-x )
        }
        
        let frontLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: compositionVideoTrack!)
        frontLayerInstruction.setTransform(transform, at: kCMTimeZero)
        
        let MainInstruction = AVMutableVideoCompositionInstruction()
        print(composition.duration)
        MainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, composition.duration)
        MainInstruction.layerInstructions = [frontLayerInstruction]
        videoComposition.instructions = [MainInstruction]
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let videoPath = documentsPath+"/cropEditVideo.mov"
        let fileManager = FileManager.default
        
        if fileManager.fileExists(atPath: videoPath)
        {
            try! fileManager.removeItem(atPath: videoPath)
        }
        
        print("video path \(videoPath)")
        var exportSession = AVAssetExportSession.init(asset: composition, presetName: AVAssetExportPresetHighestQuality)
        
        exportSession?.videoComposition = videoComposition
        exportSession?.outputFileType = AVFileType.mov
        exportSession?.outputURL = URL.init(fileURLWithPath: videoPath)
        exportSession?.videoComposition = videoComposition
        var exportProgress: Float = 0
        let queue = DispatchQueue(label: "Export Progress Queue")
        queue.async(execute: {() -> Void in
            while exportSession != nil {
                exportProgress = (exportSession?.progress)!
                percentDelegate?.sendPercentage("\(Int(exportProgress*100))")
                print("current progress == \(exportProgress)")
                DispatchQueue.main.async{
                    var estimatedTime = videoDuration.value/1750
                    var timeInMinutes = 0
                    var timeInHours = 0
                    if estimatedTime >= 60 {
                        timeInMinutes = Int(estimatedTime/60)
                        estimatedTime = Int64(Int(estimatedTime) - Int(timeInMinutes)*60)
                    }
                    if estimatedTime >= 3600{
                        timeInHours = Int(estimatedTime/3600)
                        timeInMinutes = Int(estimatedTime) - Int(timeInHours)*60
                        estimatedTime = Int64(Int(estimatedTime) - Int(timeInMinutes)*60*60)
                    }
                }
                sleep(1)
            }
        })
        exportSession?.exportAsynchronously(completionHandler: {
            if exportSession?.status == AVAssetExportSessionStatus.failed {
                 exportSession = nil
                print("Failed \(exportSession?.error)")
            } else if exportSession?.status == AVAssetExportSessionStatus.completed {
                exportSession = nil
                DispatchQueue.main.async {
                    if Product.purchased{
                        SVProgressHUD.dismiss() // this will call when purchase
                        VideoSave.saveVideoToPhotoLibrary(URL.init(fileURLWithPath: videoPath), error: nil,video: { (status) in
                            if status{
                             success(true, "\(exportProgress*100)!")
                            }else {
                              //  success(false, "")
                            }
                        })
                        
                    }else{
                        //  self.addWaterMark(url: URL.init(fileURLWithPath: videoPath))
                    }
                }
            }
            
        })
    }
}
