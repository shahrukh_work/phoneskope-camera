//
//  IAPProduct.swift
//  NAASLapp
//
//  Created by Umar on 5/18/18.
//  Copyright © 2018 softak. All rights reserved.
//

import Foundation

enum IAPProduct: Int {

    case professionalEdition
    case individualEdition
    case singleSubmission
}


