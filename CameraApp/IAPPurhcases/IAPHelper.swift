//
//  IAPHelper.swift
//  NAASLapp
//
//  Created by Umar on 5/18/18.
//  Copyright © 2018 softak. All rights reserved.
//

import Foundation
import StoreKit
import Alamofire
import FirebaseDatabase
import KeychainSwift
import SVProgressHUD



extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}


typealias ProductsRequestCompletionHandler = (SKProduct?, String?) -> ()
typealias PaymentCompletionHandler = (String?, String?) -> ()

class IAPHelper: NSObject {
    
    var buyViewController = UIViewController()
    static let shared = IAPHelper()
    private var products = [SKProduct]()
    
    let paymentQueue = SKPaymentQueue.default()
    let sharedSecret = "9eecb29cb7bc4b68b89afa1d31d43c8a"
    var productsRequestCompletionHandler: ProductsRequestCompletionHandler?
    var paymentCompletionHandler: PaymentCompletionHandler?
    
    private override init() {}
    
    func getProduct(product: String, completionHandler: @escaping ProductsRequestCompletionHandler) {
        productsRequestCompletionHandler = completionHandler
        let productID : NSSet = NSSet(objects: product)
        let productRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>)
        productRequest.delegate = self
        productRequest.start()
        
        paymentQueue.add(self)
    }
    
    func purchase(product: SKProduct?,_ viewController : UIViewController, completionHandler: @escaping PaymentCompletionHandler) {
        buyViewController = viewController
        if let productToPurchase = product {
            paymentCompletionHandler = completionHandler
            let payment = SKPayment(product: productToPurchase)
            paymentQueue.add(payment)
        }
    }
    
    func restorePurchases() {
        print("restoring purchases")
        paymentQueue.restoreCompletedTransactions()
    }
    
    // MARK: - Helper Methods
    
    func getProductIdentifier(iapProduct: String) -> String {
        return "com.softake.CameraApp.Monthly.RemoveLogo"
    }
}

extension IAPHelper: SKProductsRequestDelegate {
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        productsRequestCompletionHandler?(response.products.first, .none)
        productsRequestCompletionHandler = .none
        
        for product in response.products {
            print(product.localizedTitle)
        }
    }
    
    func request(_ request: SKRequest, didFailWithError error: Error) {
        productsRequestCompletionHandler?(.none, error.localizedDescription)
      //  productsRequestCompletionHandler = .none
    }
}

extension IAPHelper: SKPaymentTransactionObserver {
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction in transactions {
            print(transaction.transactionState.state(), transaction.payment.productIdentifier)
            SVProgressHUD.dismiss()
            switch transaction.transactionState {
                
            case .purchasing:
                break
                
            case .purchased,.restored:
                
                if let receiptURL = Bundle.main.appStoreReceiptURL, let receipt = try? Data(contentsOf: receiptURL) {
                    let base64EncodedReceipt = receipt.base64EncodedString(options: .init(rawValue: 0))
                    print(base64EncodedReceipt)
                    
                    paymentCompletionHandler?(base64EncodedReceipt, sharedSecret)
                   // SVProgressHUD.show()
                    iTunesPaymentResponse.getPurchaseResponse(receipt,nil, completionHandler: { (status,message) in
                       SVProgressHUD.dismiss()
                        Product.purchased = status.paid
                        self.buyViewController.dismiss(animated: true, completion: nil)
                    
                        
                    })
                    
                queue.finishTransaction(transaction)
                
            
                }
            default:
                paymentCompletionHandler?(.none, .none)
                queue.finishTransaction(transaction)
            
            }
        }
    }
 
}

extension SKPaymentTransactionState {
    
    func state() -> String {
        
        switch self {
        case .deferred: return "deferred"
        case .failed: return "failed"
        case .purchased: return "purchased"
        case .purchasing: return "purchasing"
        case .restored: return "restored"
        }
    }
}

