//
//  CustomCircle.swift
//  CameraApp
//
//  Created by Ali Apple  on 10/19/17.
//  Copyright © 2017 softech. All rights reserved.
//

import UIKit

class CustomCircle: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func draw(_ frame: CGRect) {
        //let h = frame.height
       // let w = frame.width
        let color:UIColor = UIColor.yellow
        
      //  let drect = CGRect(x: (w * 0.25), y: (h * 0.25), width: w, height: h)
        let bpath:UIBezierPath = UIBezierPath(rect: frame)
        
        color.set()
        bpath.stroke()
        
        print("it ran")
        NSLog("drawRect has updated the view")
    }

}
