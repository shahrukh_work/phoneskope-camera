//
//  BuyProductViewController.swift
//  CameraApp
//
//  Created by iMac on 30/01/2019.
//  Copyright © 2019 softech. All rights reserved.
//

import UIKit
import StoreKit
import KeychainSwift
import FirebaseDatabase
import SVProgressHUD


enum PaidType : String{
   
    case IAP = "iap"
    case PROMO = "promo"
   
}
class BuyProductViewController: UIViewController {
    //Mark: - IBOutlets
    @IBOutlet weak var detailSubsriptionLabel: UILabel!
    @IBOutlet weak var descriptionScrollView: RoundEdgesScrollView!
    
    @IBOutlet weak var termsConditionLabel: UILabel!
    @IBOutlet weak var cancelButtonOutlet: RoundEdgesButton!
    @IBOutlet weak var detailLabel: UILabel!
   
    //Mark: - Properties
    let dateFormatterGet = DateFormatter()
    var currentDate = ""
    var isDescriptionViewHidden = true
    var subDescription = ""
    var listOfProducts = [SKProduct]()
    var skProduct = SKProduct()
    var isPurchaseable = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.alpha = 0.5
        self.view.backgroundColor = .black
        SVProgressHUD.show()
        descriptionScrollView.isHidden = true
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        currentDate = dateFormatterGet.string(from: Date())
        
        
         subDescription = "\u{2022} Subscription will remove PhoneSkope Logo from pictures and videos captured. \n\n\u{2022} Cost: $0.99 / month. \n\n\u{2022} Validity: 1 month. \n\n\u{2022} No trial period. \n\n\u{2022} Your payment will be charged to your iTunes Account once you confirm the purchase. \n\n\u{2022} Subscription will automatically renew unless cancelled. \n\n\u{2022} Your iTunes account will be charged again when your subscription automatically renews at the end you current subscription period unless auto-renew is turned off at least 24 hours prior to end of current period. \n\n\u{2022} You can manage or turn off auto-renew in your Apple ID Account settings anytime after purchase. \n\n\u{2022} Paid subscription of $0.99 for a 1-month reoccuring subscription starts on \(currentDate)."
      
        
        detailLabel.text = subDescription
        termsConditionLabel.text = "\u{2022} Also look at our"
        if Reachability.isConnectedToNetwork(){
            IAPHelper.shared.getProduct(product: "com.softake.CameraApp.Monthly.RemoveLogo") { [weak self] (product, message) in
                SVProgressHUD.dismiss()
                if product == nil {
                    self?.isPurchaseable = false
                    let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                    return
                } else {
                    self?.isPurchaseable = true
                    self?.skProduct = product!
                }
            }
            
        }else{
              SVProgressHUD.dismiss()
            let alert = UIAlertController(title: "", message: "Internet connection is available.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
       
        cancelButtonOutlet.layer.borderWidth = 1.0
        cancelButtonOutlet.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        // Do any additional setup after loading the view.
    }

@IBAction func purchaseProductAction(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
            purchasePhoneSkope()
            
        }else{
            let alert = UIAlertController(title: "", message: "Internet connection is available.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
       
    }
    
    @IBAction func termOfUse(_ sender: Any) {
        UIApplication.shared.open(URL(string: "https://www.phoneskope.com/terms-conditions/")!, options: [:], completionHandler: nil)
    }
    @IBAction func privacyAction(_ sender: Any) {
        UIApplication.shared.open(URL(string: "https://www.phoneskope.com/privacy-policy/")!, options: [:], completionHandler: nil)
    }
    @IBAction func cancelButtonAction(_ sender: Any) {
        isDescriptionViewHidden = true
        descriptionScrollView.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func purchasePhoneSkope(){
       // SVProgressHUD.show()
     //   descriptionScrollView.isHidden = true
        if !isPurchaseable {
            let alert = UIAlertController(title: "Error", message: "Can not connect to iTunes Store.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            return
        }else{
        IAPHelper.shared.purchase(product: self.skProduct,self) { [weak self] (receipt, sharedSecret) in
            SVProgressHUD.dismiss()
            if let base64EncodedReceipt = receipt, let sharedSecretKey = sharedSecret {
               //  self?.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    @IBAction func upgradeAction(_ sender: Any) {
        isDescriptionViewHidden = !isDescriptionViewHidden
        if isDescriptionViewHidden{
            descriptionScrollView.isHidden = true
        }else{
            descriptionScrollView.isHidden = false
        }
    }
    
    
    @IBAction func usePromoAction(_ sender :Any){
    
    }
    @IBAction func buySkopeAction(_ sender: Any) {
        if let url = URL(string: "https://www.phoneskope.com/store/"){
            UIApplication.shared.open(url, options: [:], completionHandler: { (status) in
                print(status)
            })
        }
//         SKPaymentQueue.default().add(self)
//        SKPaymentQueue.default().restoreCompletedTransactions()
//
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func purchaseAppAction(_ sender: Any) {
       
    }
//    func buyProduct(){
//        SVProgressHUD.show(withStatus: "Making Payment")
//        print("buy"+currentproduct.productIdentifier)
//        let pay = SKPayment(product: currentproduct)
//        SKPaymentQueue.default().add(self)
//        SKPaymentQueue.default().add(pay as SKPayment)
//    }
//
//    func canMakePayment (){
//        SVProgressHUD.show()
//        if (SKPaymentQueue.canMakePayments() ){
//            print("IAP enabled Loading..")
//            let productID : NSSet = NSSet(objects: "com.softake.CameraApp.removeskopelogo")
//            let request : SKProductsRequest = SKProductsRequest(productIdentifiers : productID as! Set<String>)
//            request.delegate = self
//            request.start()
//        }else{
//            print("please enable IAP ")
//        }
//    }
//
//    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
//
//        print("product request")
//        let myProduct = response.products
//        print(myProduct)
//        print(myProduct.count)
//        for product in myProduct{
//            print("product added")
//            print(product.productIdentifier)
//            print(product.localizedTitle)
//            print(product.localizedDescription)
//            print(product.price)
//            listOfProducts.append(product)
//
//        }
//
//        SVProgressHUD.dismiss()
//    }
//    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
//        for transaction: AnyObject in transactions{
//            SVProgressHUD.show(withStatus: "Making Payment..")
//            let trans = transaction as! SKPaymentTransaction
//            print(trans.error)
//            let keyChain = KeychainSwift()
//            switch  trans.transactionState{
//            case .purchased , .restored:
//                print("bought OK Unlock app Here")
//                let prodID = currentproduct.productIdentifier
//                switch prodID{
//                case "com.softake.CameraApp.removeskopelogo":
//                    print("Purchase is successful")
//                    if let uniqueID = keyChain.get("DeviceID"){
//                        let ref1 = Database.database().reference()
//                        ref1.child("PromoCodes").child("PaidUsers").child(uniqueID).
//                        keyChain.set(true, forKey: "PurchaseInfo")
//                        Product.purchased = true
//                    }
//
//                default :
//                    print("IAP not found")
//                }
//                SVProgressHUD.dismiss()
//                queue.finishTransaction(trans)
//
//                break;
//            case .failed:
//                print("bought error")
//                queue.finishTransaction(trans)
//                SVProgressHUD.dismiss()
//                break;
//
//            default:
//                SVProgressHUD.dismiss()
//                break;
//
//            }
//        }
//
//
//    }
//    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
//        print("transaction restored")
//        let keyChain = KeychainSwift()
//        for transaction in queue.transactions{
//            let t : SKPaymentTransaction = transaction
//            let prodID = t.payment.productIdentifier as! String
//            switch prodID{
//            case "com.softake.CameraApp.removeskopelogo":
//                print("Purchase is successful")
//                if let uniqueID = keyChain.get("DeviceID"){
//                    let ref1 = Database.database().reference()
//                    ref1.child("PromoCodes").child("PaidUsers").child(uniqueID).setValue(true)
//                    keyChain.set(true, forKey: "PurchaseInfo")
//                    Product.purchased = true
//                }
//                break;
//
//            default:
//                print("IAP not found")
//            }
//        }
//    }
//    func finishTransaction(_ transaction: SKPaymentTransaction)
//    {
//       print( transaction.transactionState)
//    }
//
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
