//
//  AddPromoViewController.swift
//  CameraApp
//
//  Created by iMac on 30/01/2019.
//  Copyright © 2019 softech. All rights reserved.
//

import UIKit
import KeychainSwift
import FirebaseDatabase
import SVProgressHUD

class AddPromoViewController: UIViewController {

    @IBOutlet weak var promoTextField: UITextField!
    
    @IBOutlet weak var cancelBtnOutlet: RoundEdgesButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        cancelBtnOutlet.layer.borderWidth = 1.0
        cancelBtnOutlet.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        // Do any additional setup after loading the view.
    }
    @IBAction func addCodeAction(_ sender: Any) {
        if promoTextField.text != "" {
            if Reachability.isConnectedToNetwork(){
                print("Internet Connection Available!")
                getPromoCodes(text: promoTextField.text!)
                
            }else{
                let alert = UIAlertController(title: "", message: "Internet connection is available.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
           
        }
    }
    func getPromoCodes(text : String){
        SVProgressHUD.show()
        let keychain = KeychainSwift()
        let ref = Database.database().reference(withPath: "PromoCodes").child("Codes")
        ref.observeSingleEvent(of: .value, with: { snapshot in
            
            SVProgressHUD.dismiss()
            if !snapshot.exists() { return }
            print(snapshot) // Its print all values including Snap (User)
            let payment = Payment()
            let keyChain = KeychainSwift()
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot]
            {
                for promo in snapshots {
                    if let value = promo.value as? String, let _ = promo.key as? String{
                        if value == text {
                            if let uniqueID = keychain.get("DeviceID"){
                                let ref1 = Database.database().reference()
                                ref1.child("UserPayments").child("PaidUsers").child(uniqueID).setValue(["expiry_date":0,"paid":true,"type":"promo","data":""])
                                payment.expiry_date = 0
                                payment.paid = true
                                payment.type = "promo"
                                payment.data = ""
                                keyChain.set(payment.toJsonString(), forKey: "paymentInfo")
                                Product.purchased = true
                                self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)

                            }else{
                                
                            }
                        }else{
                            let alert = UIAlertController(title: "", message: "Invalid Promo Code!", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                             print("sorry wrong code.")
                        }
                        
                        
                    }
                }
            }
        })
    }
    @IBAction func cancelCodeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
