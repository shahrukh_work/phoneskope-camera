//
//  PromoCodesExtension.swift
//  CameraApp
//
//  Created by iMac on 28/01/2019.
//  Copyright © 2019 softech. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Photos
import AssetsLibrary
import CoreLocation
import MediaPlayer ////Only for hidding  Volume view
import GLKit
import CoreImage
import SVProgressHUD
import LLVideoEditor
import NVActivityIndicatorView
import FirebaseDatabase
import KeychainSwift


extension AVCamManualCameraViewController{
    func getPromoCodes(){
        SVProgressHUD.show()
        let keychain = KeychainSwift()
        let ref = Database.database().reference(withPath: "UserPayments").child("PaidUsers")
        ref.observeSingleEvent(of: .value, with: { snapshot in
            
            SVProgressHUD.dismiss()
            if !snapshot.exists() { return }
            print(snapshot) // Its print all values including Snap (User)
            
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot]
            {
                for promo in snapshots {
                    if let value = promo.value as? Bool, let key = promo.key as? String{
                        if let uniqueID = keychain.get("DeviceID"){
                            if key == uniqueID {
                              //  Product.purchased = value
                            }else{
                          
                            }
                       
                        }
                    }
                }
            }
        })
    }
    
    func getPaymentInformation (){
        let keychain = KeychainSwift()
        if let info = keychain.get("paymentInfo"){
            let payment = Payment(json: info)
            if payment.type == "iap"{
                if payment.expiry_date > Date().millisecondsSince1970{
                    Product.purchased = true
                }else{
                    self.selectScopeView.isUserInteractionEnabled = false
                    let activityIndicator = ViewControllerUtils()
                    activityIndicator.showActivityIndicator(uiView: self.selectScopeView)
                    Product.purchased = false
                    iTunesPaymentResponse.getPurchaseResponse(nil,payment.data, completionHandler: { (payment,message) in
                        activityIndicator.hideActivityIndicator(uiView: self.selectScopeView)
                        Product.purchased = payment.paid
                        if Product.purchased {
                            self.purchaseButtonOutlet.isHidden = true
                        }else{
                            self.purchaseButtonOutlet.isHidden = false
                        }
                        self.selectScopeView.isUserInteractionEnabled = true
                        
                    })
                }
            }else if payment.type == "promo"{
                Product.purchased = payment.paid
                if payment.paid {
                    self.purchaseButtonOutlet.isHidden = true
                }else{
                    self.purchaseButtonOutlet.isHidden = false
                }
            }
        }
    }
}
