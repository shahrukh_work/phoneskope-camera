//
//  AppDelegate.swift
//  CameraApp
//
//  Create by Ali Apple on 9/1/17.
//  Copyright © 2017 softech. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import SVProgressHUD
import OneSignal
import UserNotifications
import IQKeyboardManagerSwift
import Firebase
import KeychainSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, OSPermissionObserver,OSSubscriptionObserver {
    var window: UIWindow?
    
    static var IS_GEO_TAG_ON = false
    var orientationLock = UIInterfaceOrientationMask.all
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
      
        let keychain = KeychainSwift()
        if let _ = keychain.get("DeviceID"){

        }else{
            keychain.set((UIDevice.current.identifierForVendor?.uuidString)!, forKey: "DeviceID")
        }
       // keychain.clear()
        Product.purchased = true
        FirebaseApp.configure()
       
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
   
        UIApplication.shared.isIdleTimerDisabled = true
        //save introduction status
        
        let value = DataHandler().getIntroductionSettingsAll()
         var j : Int16 = 0
        if !(value.count != 0){
            for _ in 70..<99 {
                DataHandler().saveIntroductionSettings(key:  Int(Int16(9970) + j), hasShown: false)
                j += 1
            }
        }
      //  DataHandler().deleteAllRecords()
        
        let isRegisteredForRemoteNotifications = UIApplication.shared.isRegisteredForRemoteNotifications
        if isRegisteredForRemoteNotifications {
            // User is registered for notification
        } else {
            // Show alert user is not registered for notification
        }
        
        let notificationReceivedBlock: OSHandleNotificationReceivedBlock = { notification in
            
            print("Received Notification: \(notification!.payload.notificationID)")
            print("launchURL = \(notification?.payload.launchURL ?? "None")")
            print("content_available = \(notification?.payload.contentAvailable ?? false)")
        }
        
        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
            // This block gets called when the user reacts to a notification received
            let payload: OSNotificationPayload? = result?.notification.payload
            
            print("Message = \(payload!.body)")
            print("badge number = \(payload?.badge ?? 0)")
            print("notification sound = \(payload?.sound ?? "None")")
            
            if let additionalData = result!.notification.payload!.additionalData {
                print("additionalData = \(additionalData)")
                
                
                if let actionSelected = payload?.actionButtons {
                    print("actionSelected = \(actionSelected)")
                }
                
                // DEEP LINK from action buttons
                if let actionID = result?.action.actionID {
                    
                    // For presenting a ViewController from push notification action button
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let instantiateRedViewController : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "RedViewControllerID") as UIViewController
                    let instantiatedGreenViewController: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "GreenViewControllerID") as UIViewController
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    
                    print("actionID = \(actionID)")
                    
                    if actionID == "id2" {
                        print("do something when button 2 is pressed")
                        self.window?.rootViewController = instantiateRedViewController
                        self.window?.makeKeyAndVisible()
                        
                        
                    } else if actionID == "id1" {
                        print("do something when button 1 is pressed")
                        self.window?.rootViewController = instantiatedGreenViewController
                        self.window?.makeKeyAndVisible()
                        
                    }
                }
            }
        }
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: true, ]
        
        OneSignal.initWithLaunchOptions(launchOptions, appId: "a9bd98a2-9d9a-4244-bbef-088934e8331f", handleNotificationReceived: notificationReceivedBlock, handleNotificationAction: notificationOpenedBlock, settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification
        
        //        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        //        //8fcb3bfd-d97c-4e5f-afb8-f1772f64a316
        //        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        //       //original app id //a9bd98a2-9d9a-4244-bbef-088934e8331f
        //        OneSignal.initWithLaunchOptions(launchOptions,
        //                                        appId: "8fcb3bfd-d97c-4e5f-afb8-f1772f64a316",
        //                                        handleNotificationAction: nil,
        //                                        settings: onesignalInitSettings)
        //
        //        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        //
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        
        //
        // Add your AppDelegate as an obsserver
        OneSignal.add(self as! OSPermissionObserver)
        OneSignal.add(self as! OSSubscriptionObserver)
        
        Fabric.with([Crashlytics.self])
        SVProgressHUD.setForegroundColor(UIColor.yellow)
        SVProgressHUD.setBackgroundColor(.black)
        
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
           // SVProgressHUD.show()
           //  getPayment()
        }else{
            
            print("Internet Connection not Available!")
        }
        
        
       
        return true
    }
    // Add this new method
    func onOSPermissionChanged(_ stateChanges: OSPermissionStateChanges!) {
        
        // Example of detecting answering the permission prompt
        if stateChanges.from.status == OSNotificationPermission.notDetermined {
            if stateChanges.to.status == OSNotificationPermission.authorized {
                print("Thanks for accepting notifications!")
            } else if stateChanges.to.status == OSNotificationPermission.denied {
                print("Notifications not accepted. You can turn them on later under your iOS settings.")
            }
        }
        // prints out all properties
        print("PermissionStateChanges: \n\(stateChanges)")
    }
    
    // Output:
    /*
     Thanks for accepting notifications!
     PermissionStateChanges:
     Optional(<OSSubscriptionStateChanges:
     from: <OSPermissionState: hasPrompted: 0, status: NotDetermined>,
     to:   <OSPermissionState: hasPrompted: 1, status: Authorized>
     >
     */
    
    // TODO: update docs to change method name
    // Add this new method
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        print("SubscriptionStateChange: \n\(stateChanges)")
    }
    
//    func getPayment(){
//     
//            let keychain = KeychainSwift()
//            let ref = Database.database().reference(withPath: "UserPayments").child("PaidUsers")
//            ref.observeSingleEvent(of: .value, with: { snapshot in
//                if !snapshot.exists() { return }
//                print(snapshot) // Its print all values including Snap (User)
//                SVProgressHUD.dismiss()
//                if let snapshots = snapshot.children.allObjects as? [DataSnapshot]
//                {
//                    for promo in snapshots {
//                        if let value = promo.value as? [String:Any]{
//                            if let uniqueID = keychain.get("DeviceID"){
//                                if let paidType = value["type"] as? String{
//                                    if promo.key == uniqueID {
//                                    if paidType == PaidType.IAP.rawValue{
//                                        if let expireDate = value["expiry_date"] as? String{
//                                            if Int(expireDate)! > Date().millisecondsSince1970{
//                                                 Product.purchased = true
//                                            }else{
//                                                Product.purchased = false
//                                            }
//                                        }
//                                    }else{
//                                        if let paid = value["paid"] as? Bool{
//                                            Product.purchased = paid
//                                        }else{
//                                            Product.purchased = false
//                                        }
//                                    }
//                                }
//                                   
//
//                                }
//                                
//                            }
//                        }
//                    }
//                }
//            })
//        
//    }
    
    
    //
    //    func applicationWillResignActive(_ application: UIApplication) {
    //        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    //        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    //    }
    //
    //    func applicationDidEnterBackground(_ application: UIApplication) {
    //        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    //        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    //    }
    //
        func applicationWillEnterForeground(_ application: UIApplication) {
            // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
            let dataHandler = DataHandler()
            CoreDataVideos.shared.rifleVideos = dataHandler.getSaveVideoPath()
        }
    //
    //    func applicationDidBecomeActive(_ application: UIApplication) {
    //        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //    }
    //
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        let dataHandler = DataHandler()
        dataHandler.deleteAllData()
        dataHandler.resaveValues()
        
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }
    
    
}

